/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Require the Slick Slider initialiser
 */
require('./init/slick');

/**
 * Require the Isotope initializer
 */
require('./init/isotope');

/**
 * Require Moment JS
 */
moment = require('moment');

/**
 * Initialize Bootstrap Tooltip plugin
 */
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

/**
 * Enable changing of icons on open and close
 */
function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}

$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);

/**
 * AJAX usage
 */
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// Like function
$(".like").click(function (e) {
    var thisLikeButton = this;
    e.preventDefault(); // you dont want your anchor to redirect so prevent it
    $.ajax({
        type: "POST",
        // blade.php already loaded with contents we need, so we just need to
        // select the anchor attribute href with js.
        url: $(this).attr('href'),
        error: function (xhr, status, error) {
            var err = JSON.parse(xhr.responseText);
            alert(error);
        },
        success: function () {
            if ($(thisLikeButton).hasClass('liked')) {
                $(thisLikeButton).removeClass("liked");
                $(thisLikeButton).addClass("unliked");
                $(thisLikeButton).attr('title', 'Like this')
                    .tooltip('fixTitle').tooltip('hide');
            } else {
                $(thisLikeButton).removeClass("unliked");
                $(thisLikeButton).addClass("liked");
                $(thisLikeButton).attr('title', 'Unlike this')
                    .tooltip('fixTitle').tooltip('hide');
            }
        },
        complete: function () {

        }
    });
});

// Watch
$(".watch").click(function (e) {
    e.preventDefault(); // you dont want your anchor to redirect so prevent it
    $.ajax({
        type: "POST",
        // blade.php already loaded with contents we need, so we just need to
        // select the anchor attribute href with js.
        url: $(this).attr('href'),
        error: function (xhr, status, error) {
            var err = JSON.parse(xhr.responseText);
            alert(error);
        },
        success: function () {
            if ($('.watch').hasClass('watched')) {
                $(".watch").removeClass("watched");
                $(".watch").addClass("unwatched");
                $('.watch').attr('title', 'Watch this');
            } else {
                $(".watch").removeClass("unwatched");
                $(".watch").addClass("watched");
                $('.watch').attr('title', 'Stop watching this');
            }
        },
        complete: function () {

        }
    });
});