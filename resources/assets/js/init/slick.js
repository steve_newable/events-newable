$('.carousel-container').slick({
    accessibility: true,
    adaptiveHeight: true,
    arrows: false,
    autoplay: false,
    autoplaySpeed: 1000,
    centerMode: false,
    dots: true,
    mobileFirst: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    infinite: false,
    variableWidth: true,
    responsive: [{
        breakpoint: 2048,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 2,
            infinite: false,
            dots: true,
            centerMode: true,
            speed: 500
        }
    }, {
        breakpoint: 1024,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 2,
            infinite: false,
            dots: true,
            centerMode: false,
            speed: 500
        }
    }, {
        breakpoint: 600,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2
        }
    }, {
        breakpoint: 480,
        settings: {
            centerMode: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
        }
    }, {
        breakpoint: 320,
        settings: {
            centerMode: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
        }
    }]
});

$('#next').click(function () {
    $('.carousel-container').slick('slickNext');
});

$('#prev').click(function () {
    $('.carousel-container').slick('slickPrev');
});