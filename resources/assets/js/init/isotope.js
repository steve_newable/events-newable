/**
 * Isotope initialization script
 * 
 * var $grid is used for the Widget Grid
 * var $container is used for the news grid 
 */

/**
 * Initialize Isotope and Packery on page load
 */
$(window).on('load', function () {

    // quick search regex
    var qsRegex;

    // Initialise Isotope for News & Updates Grid
    var $grid = $('.grid').isotope({
        itemSelector: '.grid-item',
        percentPosition: true,
        masonry: {
            columnWidth: '.grid-sizer',
            gutter: 12
        },
        filter: function() {
            return qsRegex ? $(this).text().match( qsRegex ) : true;
        }
    });

    // use value of search field to filter
    var $quicksearch = $('.quicksearch').keyup( debounce( function() {
        qsRegex = new RegExp( $quicksearch.val(), 'gi' );
        $grid.isotope();
    }, 200 ) );
    
    // debounce so filtering doesn't happen every millisecond
    function debounce( fn, threshold ) {
        var timeout;
        threshold = threshold || 100;
        return function debounced() {
        clearTimeout( timeout );
        var args = arguments;
        var _this = this;
        function delayed() {
            fn.apply( _this, args );
        }
        timeout = setTimeout( delayed, threshold );
        };
    }

    /**
     * Initialize Packery on the Home Grid on the welcome page
     */
    var $gridHome = $('#gridHome').packery({
        itemSelector: '.grid-item',
        percentPosition: true,
        columnWidth: '.grid-sizer',
        gutter: 12,
        initLayout: false // disable initial layout


    });

    /**
     * Get positions of items in a JSON friendly format
     */
    Packery.prototype.getShiftPositions = function (attrName) {
        attrName = attrName || 'id';
        var _this = this;
        return this.items.map(function (item) {
            return {
                attr: item.element.getAttribute(attrName),
                x: item.rect.x / _this.packer.width
            }
        });
    };

    /**
     * Initialize a layout shift by either running the layout or grabbing positions from the JSON array
     */
    Packery.prototype.initShiftLayout = function (positions, attr) {
        if (!positions) {
            // if no initial positions, run packery layout
            this.layout();
            return;
        }
        // parse string to JSON
        if (typeof positions == 'string') {
            try {
                positions = JSON.parse(positions);
            } catch (error) {
                console.error('JSON parse error: ' + error);
                this.layout();
                return;
            }
        }

        attr = attr || 'id'; // default to id attribute
        this._resetLayout();

        // set item order and horizontal position from saved positions
        this.items = positions.map(function (itemPosition) {
            var selector = '[' + attr + '="' + itemPosition.attr + '"]';
            var itemElem = this.element.querySelector(selector);
            var item = this.getItem(itemElem);
            if (item) {
                item.rect.x = itemPosition.x * this.packer.width;
                return item;
            }
        }, this);

        /************** sort out items which no longer exist and add new items **************/

        // filter out any items that no longer exist
        this.items = this.items.filter(function (item) {
            return item !== undefined;
        });
        // add new items
        var newitems = [];
        var p = this;

        $(this.$element[0]).find(this.options.itemSelector).each(function (i, e) {
            if (!p.getItem(e)) {
                newitems.push(e);
            }
        });

        this.addItems(newitems);

        this.shiftLayout();
    };

    /**
     * Enable Draggability functionality on the grid items with an each statement
     */

    //If the screen width is larger than 768px
    if ($(window).width() > 768) {

    $gridHome.find('.grid-item').each(function (i, gridItem) {

        var draggie = new Draggabilly(gridItem);

        // bind drag events to Packery
        $gridHome.packery('bindDraggabillyEvents', draggie);

    });

    /**
     * Save dragged to positions in local storage
     */
    $gridHome.on('dragItemPositioned', function () {

        // save drag positions
        var positions = $gridHome.packery('getShiftPositions', 'data-item-id');

        localStorage.setItem('dragPositions', JSON.stringify(positions));

    });


    /**
     * Retrieve saved positions and initialize the layout using the saved positions
     */
    var initPositions = localStorage.getItem('dragPositions');

    $gridHome.packery('initShiftLayout', initPositions, 'data-item-id');

    }


    /**
     * Filter functions for Isotope layout
     */
    $(function () {

        //Filters for the news and updates main page
        var $container = $('#news-grid .grid'),
            $checkboxes = $('#filters input');

        $container.isotope({
            itemSelector: '.item'
        });

        $checkboxes.change(function () {
            var filters = [];
            // get checked checkboxes values
            $checkboxes.filter(':checked').each(function () {
                filters.push(this.value);
            });
            // ['.red', '.blue'] -> '.red, .blue'
            filters = filters.join(', ');
            $container.isotope({
                filter: filters
            });
        });

        /**
         * Shuffle function for Isotope Grid
         */
        $('#shuffle').click(function () {
            $container.isotope('shuffle');
        });
    });

    /**
     * Predefined filter functions, if any
     */
    var filterFns = {

    };

    // bind filter on select change
    $('.filters-select').on('change', function () {
        var $container = $('.grid');

        // get filter value from option value
        var filterValue = this.value;

        // use filterFn if matches value
        filterValue = filterFns[filterValue] || filterValue;

        $container.isotope({
            filter: filterValue
        });

    });
});