<div class="grid-item grid-item-full" data-item-id="{{$widgetNo}}">
	<section class="widget" id="yammer">

		<div class="row header">

			<div class="col-xs-10">
				<div class="heading">

				</div>
			</div>

			<div class="AddWidget">

				@include('partials.widget-button')

			</div>


		</div>
		<div class="row">
			<div class="col-xs-12">
				<div id="values-feed" style="height:400px;width:100%;"></div>
			</div>
		</div>




		<script type="text/javascript" src="https://s0.assets-yammer.com/assets/platform_embed.js"></script>
		<script type="text/javascript">
			yam.connect.embedFeed({
				"config": {
					"use_sso": true,
					"header": true,
					"footer": false,
					"showOpenGraphPreview": false,
					"defaultToCanonical": false,
					"hideNetworkName": false,
					"theme": "dark"
				},
				"container": "#values-feed"
			});
		</script>
	</section>
</div>