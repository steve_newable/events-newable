<div class="grid-item element-item" data-item-id="{{$widgetNo}}">
        <section class="widget" id="expenses">
            <!-- Find an expert -->
    
            <div class="form-box favourites">
    
                <div class="row header">
    
                    <div class="col-xs-10">
                        <div class="heading">
                            <h2>Your favourite files</h2>
                        </div>
                    </div>
    
                    <div class="col-xs-2 widget">
    
                        @include('partials.widget-button')
        
                    </div>
    
                </div>
    
                <div class="body">
                    <table id="templates" class="table-striped table-vaccancies table-responsive" role="grid" aria-describedby="templates_info">
                        <tbody>

                            @foreach ($likedFiles as $file) 

                            <tr>
                                <td><a target="_blank" href="{{ Storage::url($file->filepath) }}">{{ $file->name }}</a></td>
                            </tr>

                            @endforeach

                        </tbody>
                    </table>

                </div>
    
            </div>
    
        </section>
    </div>