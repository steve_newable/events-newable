@role('Content Manager')
<div class="grid-item element-item" tabindex="4" data-item-id="{{$widgetNo}}">
	<section class="widget" id="teams-cms">

		<div class="form-box widget yellow">
			<div class="row header">

				<div class="col-xs-10">
					<div class="heading">
						<h2>Edit all teams</h2>
					</div>
				</div>

				<div class="col-xs-2 widget">


						@include('partials.widget-button')
		
					</div>

			</div>
			<div class="row ">

				<div class="form-group col-xs-12 col-md-12">
					<p class="info">Edit all department profiles</p>

					@foreach ($teams as $team )

					<a href="/editable/teams/{{$team['name']}}">{{$team['name']}}</a>
					<br/> @endforeach

				</div>

			</div>
		</div>
	</section>
</div>
@endrole