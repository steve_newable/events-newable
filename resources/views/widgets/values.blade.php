<div class="container">
	<section class="widget" id="values">


		<div class="row">
			<div class="col-xs-12">

				<!-- Carousel box -->
				<div class="carousel-box">
					<div class="header">
						<div class="heading">
							<h2>Our values</h2>
						</div>

						<!-- Backwards and forwards buttons -->
						<div class="controls">

							<button id="prev">
								<i class="fas fa-angle-left"></i>
							</button>
							<button id="next">
								<i class="fas fa-angle-right"></i>
							</button>
						</div>

					</div>

					<!-- Main container for the teams carousel -->
					<div class="carousel-container">

						<!-- Carousel item block -->

						<div class="carousel-item">

							<div class="picto">

								<div class="visible">
									<img src="https://www.newable.co.uk/assets/img/picto/originals/png/Rocket/Newable_Pictogram_Blue_ROCKET.png" alt="">
								</div>

						


							</div>

							<div class="details">
								<h3 class="title">Dream Big</h3>
								<p class="sub-text">We believe in ambition, imagination and hope. We see potential where others see problems.</p>
							</div>


						</div>

						<!-- Carousel item block -->

						<!-- Carousel item block -->

						<div class="carousel-item">

							<div class="picto">

								<div class="visible">
									<img src="https://www.newable.co.uk/assets/img/picto/originals/png/Arrow/Newable_Pictogram_Navy_ARROW.png" alt="">
								</div>



							</div>

							<div class="details">
								<h3 class="title">Get Going</h3>
								<p class="sub-text">We believe in action and experimentation. We are driven by an urgency to make things happen.</p>
							</div>

						</div>

						<!-- Carousel item block -->



						<!-- Carousel item block -->

						<div class="carousel-item">

							<div class="picto">

								<div class="visible">
									<img src="https://www.newable.co.uk/assets/img/picto/originals/png/Leaves/Newable_Pictogram_CoolGrey_LEAVES.png" alt="">
								</div>

							


							</div>

							<div class="details">
								<h3 class="title">Grow Together</h3>
								<p class="sub-text">We believe in collaboration and diversity. We use the breadth of our experience to accelerate thinking

								</p>
							</div>

						</div>

						<!-- Carousel item block -->

						<!-- Carousel item block -->

						<div class="carousel-item">

							<div class="picto">

								<div class="visible">
									<img src="https://www.newable.co.uk/assets/img/picto/originals/png/Improve/Newable_Pictogram_Blue_IMPROVE.png" alt="">
								</div>

						

							</div>

							<div class="details">
								<h3 class="title">Always Improve</h3>
								<p class="sub-text">We believe in innovation, agility and resilience. We are quick to adapt, continuously looking for better ways to
									do things.</p>
							</div>

						</div>

						<!-- Carousel item block -->


						<!-- Carousel item block -->

						<div class="carousel-item">

							<div class="picto">

								<div class="visible">
									<img src="https://www.newable.co.uk/assets/img/picto/originals/png/Speech Bubble/Newable_Pictogram_Navy_SPEECHBUBBLE.png"
									    alt="">
								</div>

						


							</div>

							<div class="details">
								<h3 class="title">Pass It On</h3>
								<p class="sub-text">We believe in empowerment and leadership.</p>
							</div>

						</div>

						<!-- Carousel item block -->

					</div>
					<!-- Main container for the generic carousel -->

				</div>
			</div>
		</div>
</div>
<!-- Values -->


</section>
</div>