
<section class="widget" id="case-studies">
	
  	<div class="row">
		
			<div class="col-xs-12 col-md-4">
				
				<!-- Story panel with image (1 col) -->
				<div class="case-study-container">
					
						<div class="case-study-header">

							<div class="case-study-image-box">
								
								<div class="embed-responsive embed-responsive-16by9">
									
									<video class="embed-responsive-item video-player" controls>
										<source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
										<source src="https://www.w3schools.com/html/mov_bbb.ogg" type="video/ogg">
										Your browser does not support HTML5 video.
									</video>
									
								</div>

							</div>
				
							<div class="play-button small">
							</div>

						</div>

						<div class="case-study-body">

							<!-- Title -->
							<div class="case-study-title">
								Case Study: About Something
							</div>

							<div class="case-study-blurb">
								Donec rutrum congue leo eget malesuada. Sed porttitor lectus nibh. Nulla quis lorem ut libero malesuada feugiat.
							</div>

						</div>
					
				</div>
				<!-- Story panel with image (1 col) -->
				
			</div>	
			
			<div class="col-xs-12 col-md-4">
				
				<!-- Story panel with image (1 col) -->
				<div class="case-study-container">
					
						<div class="case-study-header">

							<div class="case-study-image-box">
								
								<div class="embed-responsive embed-responsive-16by9">
									
									<video class="embed-responsive-item video-player">
										<source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
										<source src="https://www.w3schools.com/html/mov_bbb.ogg" type="video/ogg">
										Your browser does not support HTML5 video.
									</video>
									
								</div>

							</div>
				
							<div class="play-button small">
							</div>

						</div>

						<div class="case-study-body">

							<!-- Title -->
							<div class="case-study-title">
								Case Study: About Something
							</div>

							<div class="case-study-blurb">
								Donec rutrum congue leo eget malesuada. Sed porttitor lectus nibh. Nulla quis lorem ut libero malesuada feugiat.
							</div>

						</div>
					
				</div>
				<!-- Story panel with image (1 col) -->
				
			</div>
			
			<div class="col-xs-12 col-md-4">
				
				<!-- Story panel with image (1 col) -->
				<div class="case-study-container">
					
						<div class="case-study-header">

							<div class="case-study-image-box">

								<img class="case-study-image" src="http://via.placeholder.com/368x287" alt="Image alt">

							</div>

							<div class="sticker yellow">
								<span class="icon icon-download"></span>
							</div>

						</div>

						<div class="case-study-body">

							<!-- Title -->
							<div class="case-study-title">
								Case Study: About Something
							</div>

							<div class="case-study-blurb">
								Donec rutrum congue leo eget malesuada. Sed porttitor lectus nibh. Nulla quis lorem ut libero malesuada feugiat.
							</div>

						</div>
					
				</div>
				<!-- Story panel with image (1 col) -->
				
			</div>	
		
		</div>
	
</section>
