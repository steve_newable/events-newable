<!-- Modal -->
<div id="ConfirmationModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        
        <div class="modal-content">
            <div class="modal-body">
                    
                    <div class="header">
                        <div class="heading">
                          	<h2>Please validate your email to proceed.<br/>Thank you.</h2>
                        </div>
                    </div>

                    <form method="POST" action="{{ action( 'ConfirmationController@storeToken' ) }}">
                    @csrf
                        <div class="form-group">
                         
                            <input type="email" class="form-control" name="SLF_email" id="SLF_email" placeholder="example@example.com">
                            <input type="hidden" name="event" value="{{ $event->id }}">
                          
                        
                        </div>

                        <button type="submit" class="btn btn-filled-blue btn-block no-margin">Send</button>
                    </form>


                    @if (session('error'))
                    <div class="alert alert-danger">{{ session('error') }}</div>
                    @endif

                    @if (session('success'))
                    <div class="alert alert-success">{{ session('success') }}</div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    
                </div>
        </div>
    </div>
</div>
    
 

