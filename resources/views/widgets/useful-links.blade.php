
<div class="grid-item element-item" tabindex="4" data-item-id="{{$widgetNo}}">
	<section class="widget" id="teams-cms">

		<div class="form-box widget yellow">
			
            <div class="row header">

				<div class="col-xs-10">
					<div class="heading">
						<h2>Useful links</h2>
					</div>
				</div>

				<div class="col-xs-2 widget">
                  @include('partials.widget-button')
		       </div>

			</div>
			
            <div class="row">
                <a href="https://rigel.newable.io/" target="_blank" >Book A holiday</a><br/>
                <a href="https://mynewable.co.uk/storage/library/Finance/Newable%20Expenses%20Template.xlsm" target="_blank" >Expenses Form</a><br/>
                <a href="https://voyager.newable.io/master.aspx" target="_blank" >Book a meeting room</a>
            </div>

			
		</div>

	</section>
</div>
