

<div class="container">
  <section class="widget" id="expenses">
    	
<!-- Invite a friend -->

		<div class="row">
			<div class="col-xs-12 col-md-8">

				<div class="form-box dark_grey">
					<div class="row">
						<div class="col-xs-12">
							<div class="header">
								<div class="heading">
									<h2>Invite a friend to this event</h2>
								</div>
							</div>
						</div>
					</div>

					<div class="form-container">

						<form method="get" action="/search.php">
							<div class="row">

								<div class="form-group col-xs-12 col-md-8">
									<label for="email-address" class="sr-only">Enter your friend's email address</label>
									
									 <input type="text" class="form-control filled" id="search-box" name="email-address" placeholder="Email address">
									
								</div>

								<div class="form-group col-xs-12 col-md-4">
									<button type="submit" id="find-contact" name="find-contact" class="btn btn-filled-blue btn-block no-margin">Send</button>
								</div>
								
							</div>
						</form>

					</div>

				</div>
			</div>
		</div>


	<!-- Invite a friend -->

  </section>
</div>

