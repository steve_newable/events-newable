@role('Admin')
@php

    $access_token = 'EAABmN3ehT2QBAPwtX8y3Dgrctwb3ZCylx7Eyq7KiXE8hYVtPSt3PrdHeJ0YsNrWQ9knh5PUIx982lHPsZCvajMMdZB5MsZAm4vI84d6PzPtF6jS9fB0DqiomB4ZCjbpMGoj17Ig7g7F3BZCDJ6LMqhkD2ZAJTmjnbk3EI7KjV8UQgZDZD';
    
    // Request the public posts.
    $json_str = file_get_contents('https://graph.facebook.com/v3.0/NewableGroup/feed?access_token='.$access_token);
    
    // decode json string into array
    $facebookData = json_decode($json_str);

@endphp

<div class="grid-item element-item" {{ isset($widgetNo) ? 'data-item-id=' . $widgetNo . '' : '' }}>

	<section class="widget" id="social-widget">

		<div class="form-box facebook-widget">
			<div class="row header">
				<div class="col-xs-10 AddWidget">

					<div class="heading">
						<h2>Newable Facebook</h2>
					</div>

				</div>

				<div class="col-xs-2 AddWidget">

					@include('partials.widget-button')

				</div>

            </div>
            
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="social-icon">
                        <i class="fab fa-facebook"></i>
                    </div>
                    
                    <div class="social-slider">  
                        
                        @if(isset($facebookData))

                        @foreach($facebookData->data as $facebookPost)

                            @if(isset($facebookPost->message))

                                <div>
                                        
                                    <p class="facebook-text">

                                        <a target="_blank" href="https://www.facebook.com/NewableGroup/posts/{{substr($facebookPost->id, strpos($facebookPost->id, "_") + 1)}}">

                                        {{$facebookPost->message}}

                                        </a>
                                    
                                    </p>


                                <p class="facebook-date">
                                    {{date("d/m/Y", strtotime($facebookPost->created_time))}}
                                </p>

                                
                            </div>

                            @endif

                        @endforeach

                        @endif

                        @if(!isset($facebookData))

                            <p style="color: #fff;">No data received</p>

                        @endif
                        

                    </div>
                </div>
            </div>

	    </section>
    </div>
    
    @section('inline-script')
    <script>
        $(document).ready(function(){
            $('.social-slider').slick({
                infinite: true,
                speed: 300,
                slidesToShow: 1,
                adaptiveHeight: true,
                nextArrow: '<i class="fas fa-arrow-right"></i>',
                prevArrow: '<i class="fas fa-arrow-left"></i>',
                fade: true
            });
        });
    </script>
    @append

@endrole