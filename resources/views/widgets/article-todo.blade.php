@role('News Approver')
<div class="grid-item element-item" data-item-id="{{$widgetNo}}">

	<section class="widget" id="articles-todo">

		<div id="news_approval" class="form-box widget dark_blue">
			<div class="row header">

				<div class="col-xs-10">
					<div class="heading">
						<h2>Approve articles</h2>
					</div>
				</div>

				<div class="col-xs-2 widget">

						@include('partials.widget-button')

				</div>

			</div>

			<ul>
				@foreach ( $article_pending as $article)

				<li>
					<a href="{{action('Editable\ArticleController@edit', $article['id'])}}">{{$article['title']}}</a>
				</li>

				@endforeach

			</ul>
			<a href="/editable/news-and-updates" class="btn btn-filled-blue btn-block no-margin">View all articles</a>
		</div>
	</section>
</div>
@endrole