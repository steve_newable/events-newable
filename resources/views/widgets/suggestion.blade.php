<div class="grid-item element-item" data-item-id="{{$widgetNo}}">
	<section class="widget" id="suggestion">

		<div class="form-box">

			<div class="row header">

				<div class="col-xs-10">
					<div class="heading">
						<h2>Have a suggestion?</h2>
					</div>
				</div>

				<div class="col-xs-2 widget">

						@include('partials.widget-button')


				</div>


			</div>


			<div class="form-container">

				<form method="post" action="{{ action( 'MessageController@storeSuggestion' ) }}">
					{{ csrf_field() }}
					<div class="row">

						<div class="form-group col-xs-12 col-md-12">
							<label for="suggestion-type">Select type</label>

							<select class="form-control transparant" id="suggestion-type" name="suggestion-type">
								<option value="">Select</option>
								<option value="News">News &amp; Updates</option>
								<option value="Events">Events</option>
								<option value="Profiles">Profiles</option>
								<option value="Search">Search</option>
								<option value="Other">Other</option>
							</select>

						</div>

						<div class="form-group col-xs-12 col-md-12">

							<textarea class="form-control filled" id="suggestion-text" name="suggestion-text" rows="6" placeholder="Lorem ipsum dolor sit amet, consectet adipiscing elit. Mauris ut viverra tellus, imperd consectetur nibh. Donec arcu tort"
							    required></textarea>

						</div>

						<div class="form-group col-xs-12 col-md-12">
							<button type="submit" id="make-suggestion" name="make-suggestion" class="btn btn-filled-blue pull-right no-margin">Submit</button>
						</div>
					</div>
				</form>

				@if(session()->has('suggestion-success'))
				<div class="alert alert-success">
					{{ session()->get('suggestion-success') }}
				</div>
				@endif @if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

			</div>


	</section>
	</div>