 @role('Admin')
 <div class="grid-item element-item" data-item-id="{{$widgetNo}}">
	<section class="widget" id="expenses">

		<div class="form-box light_blue">



			<div class="row header">

				<div class="col-xs-10">
					<div class="heading">
						<h2>Holiday counter</h2>
					</div>
				</div>

				<div class="col-xs-2 widget">
						@include('partials.widget-button')
					</div>

			</div>

			<div class="form-container">

				<form method="get" action="/search.php">
					<div class="row">

						<div class="col-xs-12 text-center">
							<p class="info">Days left in this period:</p>

							<p class="info">
								<span class="key-info big">18</span>

							</p>

							<p class="info">Booking deadline:
								<span class="key-info">30 Oct 2017</span>
							</p>
						</div>

						<div class="form-group col-xs-12 col-md-12">
							<a href="" class="btn btn-filled-navy btn-full-width no-margin">Book holiday now</a>
						</div>
					</div>
				</form>

			</div>

		</div>

	</section>
</div>
@endrole