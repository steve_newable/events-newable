<div class="grid-item grid-item-full element-item" {{ isset($widgetNo) ? 'data-item-id=' . $widgetNo . '' : '' }}>
	<section class="widget" id="downloads">

		<div class="header addWidget">
			@include('partials.widget-button')
		</div>

		<div class="row">
			<div class="col-xs-12 col-md-4">

				<div class="blub-box">

					<div class="blurb-box-header">
						<div class="blurb-box-heading">
							<h2 class="blurb-title">Reports</h2>
							<hr>
						</div>
					</div>

					<div class="blurb-box-body">

						<p>Major positioning pieces and reference docs that are unlikely to change anytime soon or need versions thereof.</p>



					</div>

					<div class="blurb-box-footer">
						<p class="bolder"><a href="/reports">View all reports</a></p>
					</div>

				</div>

			</div>

			<div class="col-xs-12 col-md-8">

				<div class="row">

					@foreach($latestReports as $report)

						<div class="col-md-4">

							<!-- Downloadable item picture box -->
							<div class="downloadable-item-wrapper">

								<div class="downloadable-item-image-container">

									<img class="downloadable-item-image" src="{{$report->image_path}}" alt="{{$report->title}}">

								</div>

								<div class="downloadable-item-overlay">
									<a class="overlay-link" href="">

										<div class="downloadable-item-text">
											{{$report->title}}
										</div>

										<div class="downloadable-item-link">
											<a class="download-link" href="{{$report->file_path}}">Download</a>
											<a class="download-icon" href="{{$report->file_path}}"><i class="fas fa-download"></i></a>
										</div>
									</a>
								</div>

							</div>
							<!-- Downloadable item picture box -->

						</div>

					@endforeach

				</div>
			</div>
		</div>

	</section>
</div>