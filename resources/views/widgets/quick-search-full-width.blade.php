<div class="grid-item grid-item-full" data-item-id="{{$widgetNo}}">
    <section class="form-box dark_blue widget">

        <div class="row header">

            <div class="col-xs-9">
                <h2>Quick search a contact</h2>

            </div>

            <div class="col-xs-2 hidden-xs hidden-sm">

                <a class="reset" href="">
                    <i class="fas fa-sync-alt"></i> Reset filter</a>
            </div>

            <div class="AddWidget">

                    @include('partials.widget-button')

            </div>


        </div>

        <div class="form-container">

            <form method="get" action="{{ action('SearchController@quickSearch') }}">

                <div class="row">

                    <div class="form-group col-md-3">
                        <label for="department">Department</label>

                        <select class="form-control transparant" id="department" name="department" title="Department">
                            <option value="">Select</option>
                            @foreach($departments as $department)
                            <option value="{{ $department }}">{{ $department }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="role">Role</label>

                        <select class="form-control transparant" id="role" name="role" title="Role">
                            <option value="">Select</option>
                            @foreach($roles as $role)
                            <option value="{{ $role }}">{{ $role }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-3">
                        <label for="location">Location</label>

                        <select class="form-control transparant" id="location" name="location" title="Location">
                            <option value="">Select</option>
                            @foreach($locations as $location)
                            <option value="{{ $location }}">{{ $location }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-xs-12 visible-xs visible-sm">
                        <button class="btn btn-transparant-link btn-block reset" title="Reset search filter">
                            <i class="fas fa-sync-alt"></i> Reset filter
                        </button>
                    </div>

                    <div class="form-group col-md-3">
                        <button type="submit" id="find-contact" name="find-contact" class="btn btn-filled-blue btn-block">Find contact</button>
                    </div>

                </div>
            </form>

        </div>

    </section>
</div>