@hasanyrole('Events Contributor|Events Approver')
<div class="grid-item element-item" data-item-id="{{$widgetNo}}" tabindex="9">

    <section class="widget" id="events-cms">

        <div class="form-box widget light_blue">

            <div class="row header">

                <div class="col-xs-10">
                    <div class="heading">
                        <h2>Events</h2>
                    </div>
                </div>

                <div class="col-xs-2 widget">
        
                        @include('partials.widget-button')
        
                </div>

            </div>
            <div class="row">

                <div class="form-group col-xs-12 col-md-12">
                    <p class="info">Create and edit events</p>
                    <a href="/editable/events" class="btn btn-filled-navy btn-full-width no-margin">Create or Edit</a>
                </div>

            </div>
        </div>
    </section>
</div>
@endrole