
<section class="widget" id="picto-banner">
	
 	
		<div class="row">
			<div class="col-xs-12">
				
				<div class="colour-block ">
					
					<div class="colour-block-header">
						<div class="colour-block-heading">
							Newable Awards Title Here (Or any event)
						</div>
					</div>
				
					<div class="colour-block-body">

						<div class="row">
							
							<div class="col-md-4">
								
								<div class="picto-holder">
									<img class="picto-holder-picto" src="https://www.newable.co.uk/assets/img/picto/originals/png/Rocket/Newable_Pictogram_White_ROCKET.png" alt="">
								</div>
								
							</div>
							
							<div class="col-md-8">
								
								<div class="colour-block-subheading">
									“Subheader to get people excited about the event”
								</div>

								<p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine.</p>

								<a class="btn btn-filled-navy btn-default-width no-margin" href="">Call To Action</a>
								
							</div>

							

							</div>

						</div>

					</div>

				</div>
				
			</div>
		</div>
	
</section>
	