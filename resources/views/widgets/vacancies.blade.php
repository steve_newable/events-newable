<div class="grid-item grid-item-full" {{ isset($widgetNo) ? 'data-item-id=' . $widgetNo . '' : '' }}>
	<section class="widget" id="case-vacancies">

		<div class="row">

			<div class="col-xs-12 ">

				<table class="table table-striped table-vaccancies header">


					<caption>Newable Vacancies

					<div class="AddWidget">
							@include('partials.widget-button')
					</div>




					<thead>
						<tr>
							<th scope="col">Vaccancy Title</th>
							<th scope="col">Dept</th>
							<th scope="col">Salary/Pay Level</th>
							<th colspan="2" scope="col">Deadline</th>
						</tr>
					</thead>

					<tbody>

						<tr>
							<td data-label="Vaccancy Title">Marketing Executive</td>
							<td data-label="Department">Marketing</td>
							<td data-label="Salary">Salary(£/Competitive)</td>
							<td data-label="Deadline">29/05/2018</td>
							<td data-label="More information">
								<a href="https://www.newable.co.uk/careers.php" target="_blank">
									<i class="fas fa-info-circle "></i>
								</a>
							</td>
						</tr>

					</tbody>
				</table>
			</div>

		</div>

	</section>
</div>