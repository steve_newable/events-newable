@role('Admin')
<div class="grid-item element-item" data-item-id="{{$widgetNo}}">
	<section class="widget" id="expenses">

		<div class="form-box light_blue">

			<div class="row header">

				<div class="col-xs-10">
					<div class="heading">
						<h2>Finance</h2>
					</div>
				</div>

				<div class="col-xs-2 widget">
		
						@include('partials.widget-button')
		
					</div>

			</div>

			<div class="form-container">

				<form method="get" action="/search.php">
					<div class="row">

						<div class="col-xs-12 text-center">
							<p class="info">Your next payday:</p>

							<p class="info">
								<span class="key-info big">29</span>
								<br />
								<span class="key-info small">Sep 2017</span>
							</p>

							<p class="info">Expenses deadline:
								<span class="key-info">30 Oct 2017</span>
							</p>
						</div>

						<div class="form-group col-xs-12 col-md-12">
							<button type="submit" id="make-suggestion" name="make-suggestion" class="btn btn-filled-navy btn-full-width no-margin">Submit expenses now</button>
						</div>
					</div>
				</form>

			</div>

		</div>

	</section>
</div>
@endrole