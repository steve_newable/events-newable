@role('Team Manager')
<div class="grid-item element-item" data-item-id="{{$widgetNo}}">
	<section class="widget" id="team-cms">

		<div class="form-box widget light_blue">

			<div class="row header">

				<div class="col-xs-10">
					<div class="heading">
						<h2>Department/Team</h2>
					</div>
				</div>

				<div class="col-xs-2 widget">
						@include('partials.widget-button')
				</div>

			</div>

			<div class="row">
				<div class="form-group col-xs-12 col-md-12">
					<p class="info">Edit your team page and profiles</p>
					<a href="/editable/teams/{{ $user->department }}" class="btn btn-filled-navy btn-full-width no-margin">Click Here</a>
				</div>
			</div>
		</div>

	</section>
</div>
@endrole