<div class="grid-item element-item" {{ isset($widgetNo) ? 'data-item-id=' . $widgetNo . '' : '' }}>

	<section class="widget" id="social-widget">

		<div class="form-box">
			<div class="row header">
				<div class="col-xs-10 AddWidget">

					<div class="heading">
						<h2>Newable Tweets</h2>
					</div>

				</div>

				<div class="col-xs-2 AddWidget">

					@include('partials.widget-button')

				</div>

            </div>
            
            <div class="row">
                <div class="col-md-12">
                    
                    <div class="social-icon">
                        <i class="fab fa-twitter"></i>
                    </div>
                    
                    @if(!empty($twitterData))
                    <div class="social-slider">  

                      
         
                        @foreach($twitterData as $key => $value)
                            <div>
                                <p class="tweet-text">
                                

                                    <a target="_blank" href="https://twitter.com/Newable/status/{{ $value['id'] }}">

                                    {{preg_replace("/ https:\/\/t.co.*/", "", $value['full_text'])}}

                                    </a>

                                    @if(!empty($value['extended_entities']['media']))
                                    @foreach($value['extended_entities']['media'] as $v)
                                    <a target="_blank" href="https://twitter.com/Newable/status/{{ $value['id'] }}">
                                        <img class="center-block social-img img-responsive" src="{{ $v['media_url_https'] }}">
                                    </a>
                                    @endforeach
                                @endif

                                </p>

                                <p class="tweet-stats">
                                    Favourites {{ $value['favorite_count'] }}
                                    | Retweets {{ $value['retweet_count'] }}
                                </p>
                                
                            </div>
                        @endforeach
                        
                    </div>
                    @endif
                </div>
            </div>

	    </section>
    </div>
    
    @section('inline-script')
    <script>
    $(document).ready(function(){
        $('.social-slider').slick({
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            adaptiveHeight: true,
            nextArrow: '<i class="fas fa-arrow-right"></i>',
            prevArrow: '<i class="fas fa-arrow-left"></i>',
            fade: true
        });

        $('.social-slider').on('setPosition', function(event, slick, currentSlider, nextSlide){
            @if(\Route::current()->getName() == 'widget-library')

                var $container = $('.grid');
                $container.isotope('layout');

                @elseif(\Route::current()->getName() == 'welcome-page')

                var $grid = $('.grid');
                var initPositions = localStorage.getItem('dragPositions');
                // init layout with saved positions
                $grid.packery('initShiftLayout', initPositions, 'data-item-id');

            @endif
        });
    });
</script>
@append