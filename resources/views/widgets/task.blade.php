
<div class="grid-item element-item" {{ isset($widgetNo) ? 'data-item-id=' . $widgetNo . '' : '' }}>
    <section class="widget" id="suggestion">

        <div class="form-box">

            <div class="row header">

                <div class="col-xs-10">
                    <div class="heading">
                        <h2>Got a task?</h2>
                    </div>
                </div>

                <div class="col-xs-2 widget">
                        @include('partials.widget-button')
                </div>


            </div>


            <div class="form-container">

                <form method="post" id="create_task_form" action="{{ action( 'TaskController@store' ) }}">
                    {{ csrf_field() }}
                    <div class="row">

                        <div class="form-group task-recipient-form-group col-xs-12 col-md-12" id="task-form-group-recipient_1">
                            <label for="task-recipient">To</label>
                            <div class="form-recipient-name-container">
                            <input type="text" class="form-control filled" name="task_recipient[]" id="task_recipient_1" placeholder="Who is this task for?"
                                required>
                                <button type="button" class="btn btn-recipient btn-add" title="Add another recipient" >+</button>
                                <button type="button" class="btn btn-recipient btn-remove" title="Remove recipient" >-</button>
                            </div>
                        </div>

                        <div class="form-group col-xs-12 col-md-12">
                            <label for="task_title">Task Title</label>
                            <input type="text" class="form-control filled" name="task_title" id="task_title" placeholder="Enter the task title" required>
                        </div>

                        <div class="form-group col-xs-12 col-md-12">
                            <label for="task_text">Task Description</label>
                            <textarea class="form-control filled" id="task_text" name="task_text" rows="6" placeholder="Enter a task description"
                                required></textarea>
                        </div>

                        <div class="form-group col-xs-12 col-md-12">
                            <button type="submit" id="make-task" name="make-task" class="btn btn-filled-blue pull-right no-margin">Submit</button>
                        </div>
                    </div>
                </form>

                @if(session()->has('task-success'))
                <div class="alert alert-success">
                    {{ session()->get('task-success') }}
                </div>
                @endif @if(session()->has('task-error'))
                <div class="alert alert-danger">
                    {{ session()->get('task-error') }}
                </div>
                @endif @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

            </div>


    </section>
    </div>

    @section('inline-script') 
    @append

    <script>

        document.addEventListener("DOMContentLoaded", function(event) {

            var taskAutoSuggestOptions = {
                url: "/api/users-list",
                list: {
                    match: {
                        enabled: true
                    }
                }
            };
            $("#task_recipient_1").easyAutocomplete(taskAutoSuggestOptions);
    
            $('#create_task_form').submit(function(e){
                $(this).find('button[type=submit]').attr('disabled', true);
            });

        if (typeof checkRemainingRecipients != 'function') { 

            function checkRemainingRecipients() {
                recipientsAmount = $('.task-recipient-form-group').length;
                $(".task-recipient-form-group").each(function (index) {
                    //If remaining fields left are 1, disable the delete btn for that field
                    if (index == 0 && recipientsAmount == 1) {
                        $(this).find('.btn-recipient.btn-remove').attr("disabled", true);
                    }
                    //Otherwise, reenable it
                    else if (recipientsAmount > 1) {
                        $(this).find('.btn-recipient.btn-remove').attr("disabled", false);
                    }
                });
            }
        }

        // Add new element
        $("#create_task_form").on('click', '.btn-recipient.btn-add', function() {

            // Finding total number of elements added
            var total_element = $(".task-recipient-form-group").length;


            // last <div> with element class id
            var lastid = $(".task-recipient-form-group").last().attr("id");
            var split_id = lastid.split("_");
            var nextindex = Number(split_id[1]) + 1;

            var maxAnswers = 25;
            // Check total number elements
            if (total_element < maxAnswers) {
                // Adding new div container after last occurance of element class
                $(".task-recipient-form-group:last").after("<div class='form-group task-recipient-form-group col-xs-12 col-md-12' id='task-form-group-recipient_" + nextindex +
                    "'></div>");

                // Adding element to <div>
                $("#task-form-group-recipient_" + nextindex).append(
                    `<div class='form-recipient-name-container'>
                        <input type='text' name='task_recipient[]' class='form-control form-recipient-name' placeholder='Insert Full Name' id='task_recipient${nextindex}' required>
                        <button type='button' title="Add another recipient" class='btn btn-recipient btn-add'>+</button>
                        <button type='button' id='remove_${nextindex}' class='btn btn-recipient btn-remove'>-</button>
                    </div>
                `);

                //Check how many remaining answer fields are left
                checkRemainingRecipients();

                $("#task_recipient" + nextindex).easyAutocomplete(taskAutoSuggestOptions);

                updateLayout();

            } else {
                alert('You have reached the maximum amount of recipients you can have (25)');
            }

        });

        // Remove element
        $('#create_task_form').on('click', '.btn-remove', function () {

            var id = this.id;
            var split_id = id.split("_");
            var deleteindex = split_id[1];

            // Remove <div> with id
            $("#task-form-group-recipient_" + deleteindex).remove();

            //Check how many remaining answer fields are left
            checkRemainingRecipients();

            updateLayout();

        }); 

        if (typeof updateLayout != 'function') { 

            function updateLayout() {
                //Update isotope layout
                @if(\Route::current()->getName() == 'widget-library')

                var $container = $('.grid');
                $container.isotope('layout');

                @elseif(\Route::current()->getName() == 'welcome-page')

                var $grid = $('.grid');
                var initPositions = localStorage.getItem('dragPositions');
                // init layout with saved positions
                $grid.packery('initShiftLayout', initPositions, 'data-item-id');

                @endif
            }

        }

        $('#create_task_form').submit(function(e){
            $(this).find('button[type=submit]').attr('disabled', true);
        });


    });
    </script>