<div class="grid-item element-item grid-item-create-poll" data-item-id="{{$widgetNo}}">

	<section class="widget" id="snap-pole">

		<div class="form-box">
			<div class="row header">
				<div class="col-xs-10 AddWidget">

					<div class="heading">
						<h2>Snap Poll</h2>
					</div>

				</div>

				<div class="col-xs-2 AddWidget">

					@include('partials.widget-button')

				</div>

			</div>

			<div class="form-container">

				@if(session()->has('poll-success'))
				<div class="alert alert-success">
					{{ session()->get('poll-success') }}
				</div>
				@endif @if(session()->has('poll-error'))
				<div class="alert alert-danger">
					{{ session()->get('poll-error') }}
				</div>
				@endif @include('layouts.errors')

				<p class="info">Create a new Snap Poll <span class="pull-right"><i class="far fa-question-circle" data-toggle="tooltip" data-placement="right" title="Create a new public snap poll."></i></span></p>


				<form action="{{url('poll')}}" enctype="multipart/form-data" method="POST">
					@csrf
					<div class="form-group">
						<label for="question">Poll Question</label>
						<textarea name="question" class="form-control" id="question" required>{{ old('question') }}</textarea>
					</div>
					<div id="answer-fields">
						<div class="form-group answer-form-group" id="divanswer_1">
							<label for="answer1">Answer #1</label>
							<input type="text" name="answer[]" class="form-control answer-input" id="answer1" required>
							<button id="remove_1" class="btn btn-primary remove-btn">
								<i class='fa fa-times'></i>
							</button>
						</div>
						<div class="form-group answer-form-group" id="divanswer_2">
							<label for="answer2">Answer #2</label>
							<input type="text" name="answer[]" class="form-control answer-input" id="answer2" required>
							<button id="remove_2" class="btn btn-primary remove-btn">
								<i class='fa fa-times'></i>
							</button>
						</div>
					</div>
					<div class="form-group">
						<button type="button" id="addQuestion" class="btn btn-filled-navy btn-full-width no-margin add-answer">Add Another Answer</button>
					</div>
					<div class="checkbox">
						<label id="anonymous-chkbox"><input type="checkbox" name="anonymous" value="anonymous">Create Anonymously </label>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-filled-navy btn-full-width no-margin">Submit Poll</button>
					</div>
				</form>

			</div>

	</section>
	</div>

	@section('inline-script')
	<script>
		$(document).ready(function () {

			function calcAnswerOrder() {
				$(".answer-form-group").each(function (index) {
					var answerNum = index + 1;
					$(this).find('label').text("Answer #" + answerNum);
				});
			}

			function checkRemainingAnswer() {
				answersAmount = $('.answer-form-group').length;
				$(".answer-form-group").each(function (index) {
					//If remaining fields left are 1, disable the delete btn for that field
					if (index == 0 && answersAmount == 1) {
						$(this).find('.remove-btn').attr("disabled", true);
					}
					//Otherwise, reenable it
					else if (answersAmount > 1) {
						$(this).find('.remove-btn').attr("disabled", false);
					}
				});
			}

			// Add new element
			$(".add-answer").click(function () {

				// Finding total number of elements added
				var total_element = $(".answer-form-group").length;


				// last <div> with element class id
				var lastid = $(".answer-form-group:last").attr("id");
				var split_id = lastid.split("_");
				var nextindex = Number(split_id[1]) + 1;

				var maxAnswers = 8;
				// Check total number elements
				if (total_element < maxAnswers) {
					// Adding new div container after last occurance of element class
					$(".answer-form-group:last").after("<div class='form-group answer-form-group' id='divanswer_" + nextindex +
						"'></div>");

					// Adding element to <div>
					$("#divanswer_" + nextindex).append("<label for='answer" + nextindex +
						"'>Answer</label><input type='text' name='answer[]' class='form-control answer-input' id='answer" +
						nextindex + "' required><button id='remove_" + nextindex +
						"' class='btn btn-primary remove-btn'><i class='fa fa-times'></i></button>");

					//Recalculate order of answers
					calcAnswerOrder();
					//Check how many remaining answer fields are left
					checkRemainingAnswer();

				} else {
					alert('You have reached the maximum amount of answers you can have (8)');
				}

				//Update isotope layout

				@if(\Route::current()->getName() == 'widget-library')

					var $container = $('.grid');
					$container.isotope('layout');

				@elseif(\Route::current()->getName() == 'welcome-page')

					var $grid = $('.grid');
					var initPositions = localStorage.getItem('dragPositions');
					// init layout with saved positions
					$grid.packery('initShiftLayout', initPositions, 'data-item-id');

				@endif

			});

			// Remove element
			$('#snap-pole').on('click', '.remove-btn', function () {

				var id = this.id;
				var split_id = id.split("_");
				var deleteindex = split_id[1];

				// Remove <div> with id
				$("#divanswer_" + deleteindex).remove();

				//Recalculate order of answers
				calcAnswerOrder();
				//Check how many remaining answer fields are left
				checkRemainingAnswer();

				//Update isotope layout

				@if(\Route::current()->getName() == 'widget-library')

					var $container = $('.grid');
					$container.isotope('layout');

				@elseif(\Route::current()->getName() == 'welcome-page')

					var $grid = $('.grid');
					var initPositions = localStorage.getItem('dragPositions');
					// init layout with saved positions
					$grid.packery('initShiftLayout', initPositions, 'data-item-id');

				@endif

			}); 

			$('#snap-pole form').submit(function(e){
            	$(this).find('button[type=submit]').attr('disabled', true);
        	});

		});
	</script>
	@append