@php 
    $widgetNo = 6; 
    $exist = false;
 @endphp 

<div class="grid-item element-item" data-item-id="{{$widgetNo}}">
    <section class="widget" id="messages-widget">

        <div class="form-box widget dark_grey">

            <div class="row header">

                <div class="col-xs-10">
                    <div class="heading">
                        <h2>Send a Message</h2>
                    </div>
                </div>


                <div class="col-xs-2 widget">

                        @include('partials.widget-button')
                </div>

            </div>

            <form class="messages-widget-form" method="POST" action="{{ action( 'MessageController@sendMessageByWidget' ) }}">
                @csrf
                {{-- If the validation didn't fail previously, display single recipient input --}}
                @if(empty(old('recipient_name')))
                <div class="recipient-form-group form-group" id="form-group-recipient_1">
                    <label for="recipient_name">To</label>
                    <div class="form-recipient-name-container">
                        <input type="text" name="recipient_name[]" class="form-control form-recipient-name" id="recipient_name1" placeholder="Insert Full Name" required>
                        <button type="button" class="btn btn-recipient btn-add" title="Add another recipient" >+</button>
                        <button type="button" class="btn btn-recipient btn-remove" title="Remove recipient" disabled>-</button>
                    </div>
                </div>
                {{-- If validation failed previously, output all recipient inputs entered previously --}}
                @else
                    @foreach(old('recipient_name') as $key => $recipient_name)
                            <div class="recipient-form-group form-group" id="form-group-recipient_{{$key}}">
                            @if($key == 0)
                                <label for="recipient_name">To</label>
                            @endif
                            <div class="form-recipient-name-container">
                                <input type="text" value="{{ old('recipient_name.' . $key) }}" name="recipient_name[]" class="form-control form-recipient-name" id="recipient_name{{$key}}" placeholder="Insert Full Name" required>
                                <button type="button" class="btn btn-recipient btn-add" title="Add another recipient" >+</button>
                                <button type="button" id="remove_{{$key}}" class="btn btn-recipient btn-remove" title="Remove recipient" @if($key == 0) @endif>-</button>
                            </div>
                        </div>
                    @endforeach
                @endif
                <div class="form-group">
                    <label for="body">Message</label>
                    <textarea class="form-control" name="message_body" id="message_body" cols="30" rows="4" required>{{ old('message_body') }}</textarea>
                </div>
                <button type="submit" class="btn btn-filled-blue btn-block no-margin">Send</button>
            </form>

            @if (session('message-error'))
            <div class="alert alert-danger">{{ session('message-error') }}</div>
            @endif @if (session('message-success'))
            <div class="alert alert-success">{{ session('message-success') }}</div>
            @endif @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

        </div>
    </section>
</div>

@section('inline-script')

<script>
    function checkRemainingRecipients() {
        recipientsAmount = $('.recipient-form-group').length;
        $(".recipient-form-group").each(function (index) {
            //If remaining fields left are 1, disable the delete btn for that field
            if (index == 0 && recipientsAmount == 1) {
                $(this).find('.btn-recipient.btn-remove').attr("disabled", true);
            }
            //Otherwise, reenable it
            else if (recipientsAmount > 1) {
                $(this).find('.btn-recipient.btn-remove').attr("disabled", false);
            }
        });
    }

    // Add new element
    $(".messages-widget-form").on('click', '.btn-recipient.btn-add', function() {

        // Finding total number of elements added
        var total_element = $(".recipient-form-group").length;


        // last <div> with element class id
        var lastid = $(".recipient-form-group").last().attr("id");
        var split_id = lastid.split("_");
        var nextindex = Number(split_id[1]) + 1;

        var maxAnswers = 25;
        // Check total number elements
        if (total_element < maxAnswers) {
            // Adding new div container after last occurance of element class
            $(".recipient-form-group:last").after("<div class='form-group recipient-form-group' id='form-group-recipient_" + nextindex +
                "'></div>");

            // Adding element to <div>
            $("#form-group-recipient_" + nextindex).append(
                `<div class='form-recipient-name-container'>
                    <input type='text' name='recipient_name[]' class='form-control form-recipient-name' placeholder='Insert Full Name' id='recipient_name${nextindex}' required>
                    <button type='button' title="Add another recipient" class='btn btn-recipient btn-add'>+</button>
                    <button type='button' id='remove_${nextindex}' class='btn btn-recipient btn-remove'>-</button>
                </div>
            `);

            //Check how many remaining answer fields are left
            checkRemainingRecipients();

            $("#recipient_name" + nextindex).easyAutocomplete(messageRecipientSuggestOptions);

            updateLayout();

        } else {
            alert('You have reached the maximum amount of recipients you can have (25)');
        }

    });

    // Remove element
    $('.messages-widget-form').on('click', '.btn-remove', function () {

        var id = this.id;
        var split_id = id.split("_");
        var deleteindex = split_id[1];

        // Remove <div> with id
        $("#form-group-recipient_" + deleteindex).remove();

        //Check how many remaining answer fields are left
        checkRemainingRecipients();

        updateLayout();

    }); 

    function updateLayout() {
        //Update isotope layout
        @if(\Route::current()->getName() == 'widget-library')

        var $container = $('.grid');
        $container.isotope('layout');

        @elseif(\Route::current()->getName() == 'welcome-page')

        var $grid = $('.grid');
        var initPositions = localStorage.getItem('dragPositions');
        // init layout with saved positions
        $grid.packery('initShiftLayout', initPositions, 'data-item-id');

        @endif
    }

    var messageRecipientSuggestOptions = {
        url: "/api/users-list",
        list: {
            match: {
                enabled: true
            }
        }
    };

    $("#recipient_name1").easyAutocomplete(messageRecipientSuggestOptions);

    $('.messages-widget-form').submit(function(e){
        $(this).find('button[type=submit]').attr('disabled', true);
    });

</script>

@append