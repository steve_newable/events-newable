@hasanyrole('News Contributor|News Approver')
<div class="grid-item element-item" data-item-id="{{$widgetNo}}">
	<section class="widget" id="articles-cms">

		<div class="form-box widget light_blue">

			<div class="row header">

				<div class="col-xs-10">
					<div class="heading">
						<h2>News articles</h2>
					</div>
				</div>

				<div class="col-xs-2 AddWidget">
						@include('partials.widget-button')
				</div>


			</div>

			<div class="row">
				<div class="form-group col-xs-12 col-md-12">
					<p class="info">Create and edit news and updates</p>
					<a href="/editable/news-and-updates" class="btn btn-filled-navy btn-full-width no-margin">Create or Edit</a>
				</div>
			</div>
		</div>
	</section>
</div>
@endrole