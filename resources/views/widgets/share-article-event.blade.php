<!-- Modal -->
<div id="shareModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        
        <div class="modal-content">
            <div class="modal-body">
                    <div class="header">
                        <div class="heading">
                            <h2>Share This {{ isset($article) ? 'Article' : 'Event' }}</h2>
                        </div>
                    </div>
                    <form method="POST" action="{{ action( 'MessageController@storeShare' ) }}">
                    @csrf
                        <div class="form-group">
                            <label for="recipient_name">To</label>
                            <input type="text" name="share_recipient_name" class="form-control" id="share_recipient_name" placeholder="Insert Full Name">
                            <input type="hidden" name="title" value="{{ isset($article) ? $article->title : $event->title }}">
                            <input type="hidden" name="url" value="{{ url()->full() }}">
                            <input type="hidden" name="type" value="{{ isset($article) ? 'article' : 'event' }}">
                        </div>
                        <button type="submit" class="btn btn-filled-blue btn-block no-margin">Send</button>
                    </form>

                    @if (session('error'))
                    <div class="alert alert-danger">{{ session('error') }}</div>
                    @endif

                    @if (session('success'))
                    <div class="alert alert-success">{{ session('success') }}</div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
        </div>
    </div>
</div>
    
    @section('inline-script')
    @parent
    
        <script>
            var shareOptions = {
                url: "/api/users-list",
                list: {
                    match: {
                        enabled: true
                    }
                }
            };
    
            $("#share_recipient_name").easyAutocomplete(shareOptions);
            $("#shareModal .easy-autocomplete").removeAttr("style");
        </script>

        @if(!empty(Session::get('success')) || !empty(Session::get('error')))
        <script>
        $(function() {
            $('#shareModal').modal('show');
        });
        </script>
        @endif

        @stop
