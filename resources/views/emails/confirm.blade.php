@component('mail::message')
# Dear {{ $data['person'] }}

Thank you for registering your details.  Please click on the button below to verify your email address.




@component('mail::button', ['url' => 'http://127.0.0.1:8000/confirm/'.$data['token'] ])
Verify your email address
@endcomponent
 
If the button above does not work please copy the following link into your browser's address bar: 
http://events.newable.co.uk/events/confirm_email.php?token=aebd99d216aa85a0ab00755a2474759488da0a05b2b6828a0cf4da41b869



Thanks,<br>
{{ config('app.name') }}
@endcomponent
