@component('mail::message')
# Hi {{ $data['person'] }}

You have been assigned a task by {{ $data['sender'] }}


@component('mail::panel')
# {{ $data['title'] }}
  {{ $data['message'] }}  

@endcomponent

&nbsp;

@component('mail::button', ['url' => 'http://mynewable.co.uk/tasks/show/'.$data['id'] ])
Go to Task
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
