@extends ('layouts.app') 

@section('title', $event->title . ' | Events') 

@section ('content')

<!-- Breadcrumbs -->
<div class="container">
    <div class="row">

        <div class="col-xs-12">

            <div class="breadcrumb-bar">

                <ul>
                    <li>
                        <a href="/" class="">Home</a>
                    </li>
                    <li>
                        <a href="/events">Events</a>
                    </li>
                    <li>{{ $event->title }}</li>
                </ul>

            </div>
        </div>

    </div>

</div>
<!-- Breadcrumbs end -->

<div class="container">
    <div class="row">

        <!-- Main article -->
        <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-0">

            <!-- Article container -->
            <div class="article">

                <div class="article-featured-image-box">

                    <img class="featured-image" src="{{ $event->image }}" alt="{{ $event->title }}">

                </div>

                <div class="article-content">

                    <div class="article-header">

                        <div class="article-title">
                            {{ $event->title }}
                        </div>

                        @if(count($event->tags) !== 0)

                        <div class="tag-row">
                            <span class="hidden-xs">Tags:</span>

                            <div class="tag-words-list">

                                @foreach($event->tags as $tag)

                                <a class="tag-word" href="{{ URL::action('TagController@show', $tag->slug) }}">{{ $tag->name }}</a>

                                @endforeach

                            </div>
                        </div>

                        @endif

                        <div class="interactions-panel">

                            <!-- A row for icons to sit in -->
                            <div class="interaction-row">

                                <!-- The individual items -->
                                {{-- <div class="interaction-item">

                                    @if($event->isWatched)
                                    <a href="{{ action('WatchController@watchEvent', $event->id) }}" class="interactor watch watched" role="button" tabindex="0"
                                        title="Stop watching this">
                                        @else
                                        <a href="{{ action('WatchController@watchEvent', $event->id) }}" class="interactor watch unwatched" role="button" tabindex="0"
                                            title="Watch this">
                                            @endif
                                            <div class="icon-block">
                                                <i class="fas fa-eye"></i>
                                            </div>
                                        </a>
                                </div> --}}

                                <!-- The individual items -->
                                <div class="interaction-item">

                                  
                                    <a href="" class="interactor like liked" role="button" tabindex="0" title="Unlike this" data-toggle="tooltip">
                                  
                                    <a href="" class="interactor like unliked" role="button" tabindex="0"
                                        title="Like this" data-toggle="tooltip">
                             
                                        <div class="icon-block">
                                            <i class="fas fa-fw fa-heart"></i>
                                        </div>
                                    </a>

                                </div>

                                <!-- The individual items -->
                                {{-- <div class="interaction-item">
                                    <button class="interactor share" role="button" tabindex="0">
                                        <div class="icon-block">
                                            <i class="fas fa-fw fa-download"></i>
                                        </div>
                                    </button>
                                </div> --}}

                                <!-- The individual items -->
                                <div class="interaction-item">
                                    <button class="interactor share" data-toggle="modal" data-target="#shareModal" role="button" tabindex="0">
                                        <div class="icon-block">
                                            <i class="fas fa-fw fa-paper-plane"></i>
                                        </div>
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>

                    <!-- Body of article -->
                    <div class="article-body">

                        <h2 class="article-subheading">Description</h2>

                        {!! $event->description !!} 

                        @if( $event->details )
                            <h2 class="article-subheading">What to expect</h2>
                            {!! $event->details !!} 
                        @endif

                    </div>
                    <!-- Article body ends -->

                    <!-- Article buttons -->
                    <div class="row">

                        <div class="button-row">

                            @if(!empty($event->externalPageUrl))

                            <div class="col-xs-12 col-sm-6 col-md-6">

                                <a class="btn btn-filled-blue btn-block no-margin" data-toggle="modal" data-target="#ConfirmationModal">
                                Book your place</a>

                            </div>

                            @endif

                            <div class="col-xs-12 col-sm-6 col-md-6">

                                <button class="btn btn-transparant-link btn-block no-margin" data-toggle="modal" data-target="#shareModal">
                                    <span class="button-text icon-right">Share this event</span>
                                    <i class="fas fa-paper-plane icon-small"></i>
                                </button>

                            </div>

                        </div>

                    </div>
                    <!-- Article buttons -->

                    <div class="article-footer">

                        @if( $event->hostedBy )
                        <h2 class="article-subheading">Host department:</h2>
                        <p>{{ $event->hostedBy }}</p>
                        @endif

                    </div>

                </div>

            </div>
            <!-- End main event container -->

            @include('widgets.share-article-event')
             @include('widgets.confirm-email-event')

            @if(count($relatedEvents) > 0)
            <!-- Related Events to this event -->
            <div class="row">
                <div class="col-xs-12">

                    <div class="event-list-container">

                        <div class="header">
                            <div class="heading">
                                Similar upcoming events
                            </div>
                        </div>

                        <!-- Event box -->
                        @foreach($relatedEvents as $relatedEvent)

                        <!-- Event box -->
                        <div class="event-box">

                            <div class="row">

                                <!-- Date -->
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-1">

                                    <div class="date">
                                        <p class="day">{{ $relatedEvent->startDate->format('d') }}</p>
                                        <p class="month">{{ $relatedEvent->startDate->format('F') }}</p>
                                    </div>

                                </div>

                                <!-- Image  -->
                                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">

                                    <div class="event-image">
                                        <a href="{{action('EventController@show',[$event->id, str_slug($event->title)])}}">
                                            <img class="img-responsive" alt="{{ $event->title }}" src="{{ $event->image }}">
                                        </a>
                                    </div>

                                </div>

                                <!-- Details -->
                                <div class="col-xs-12 col-sm-6 col-md-7 col-lg-7">

                                    <div class="event-details">

                                        <h2 class="title">
                                            <a href="{{action('EventController@show',[$relatedEvent->id, str_slug($relatedEvent->title)])}}">{{ $relatedEvent->title }}</a>
                                        </h2>
                                        <p class="when">
                                            <span class="inline-title">When:</span> {{ $relatedEvent->startDate->format('d M Y') }} | {{$relatedEvent->startTime}}-{{$relatedEvent->finishTime}}</p>
                                        <p class="where">
                                            <span class="inline-title">Where:</span>{{ $relatedEvent->location }}</p>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <!-- End -->


                        @endforeach
                        <!-- End -->

                        <div class="event-list-footer">
                            <div class="row">
                                <div class="col-xs-12">

                                    <a href="/events">View all events</a>

                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>
            @endif

        </div>

        <!-- End related events -->


        <!-- Sidebar -->
        <div class="col-xs-12 col-sm-12 col-md-4">

            <div class="event-sidebar-details-box">

                <h2 class="event-sidebar-title">Event Details</h2>

                <h3 class="event-sidebar-subtitle">When:</h3>

                <p>{{ $event->startDate->format('d M Y') }} | {{ $event->startTime }}-{{ $event->finishTime }}</p>

                <h3 class="event-sidebar-subtitle">Where:</h3>

                <p>{{ $event->location }}</p>

                <h3 class="event-sidebar-subtitle">Enquiries:</h3>

                <p>t: 020 7403 0300</p>

                <p>e:
                    <a href="mailto:example@newable.com">example@newable.com</a>
                </p>

                <a class="btn btn-filled-navy btn-book-your-place" href="{{ $outlookEvent }}">Add to Calendar</a>

                @if(!empty($event->externalPageUrl))

                <a class="btn btn-filled-navy btn-book-your-place" href="{{ $event->externalPageUrl }}" target="_blank">Reserve your place</a>

                @endif

            </div>

            <div class="event-sidebar-map">
                {!! $location['html'] !!}
            </div>

            @section('inline-script') 
                @parent 
                {!! $location['js'] !!} 
            @endsection 
            
            @include('layouts.calendar')

        </div>
        <!-- Sidebar ends -->

    </div>
</div>

    @endsection
