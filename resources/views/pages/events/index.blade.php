@extends ('layouts.app') @section('title', 'Events') @section ('content')

<!-- Breadcrumbs -->
<div class="container">
    <div class="row">

        <div class="col-xs-12">

            <div class="breadcrumb-bar">

                <ul>
                    <li>
                        <a href="/">Home</a>
                    </li>
                    <li>
                        <a href="/events">Events</a>
                    </li>
                    @if(isset($date))
                    <li>Events for: {{ date("d/m/Y", strtotime($date))}}</li> @endif
                </ul>

            </div>
        </div>

    </div>

</div>

<!-- Header -->
<div class="container">
    <div class="row">

        <div class="col-xs-12">

            <div class="heading-underline-wrapper">
                <h1>Events</h1>
                <span class="underline"></span>
            </div>

            <div class="introduction">
                <p>From client events to Town Hall presentations to networking and social get-togethers - find out what's going
                    on and how to get involved. If you'd like to add an event, please speak to XXXX.</p>
            </div>

        </div>
    </div>
</div>

<div class="container">
    <div class="row">

        <div class="col-xs-12 col-md-4">

            <div class="row">

                <div class="col-xs-12">

                    @include('layouts.calendar')

                </div>


            </div>

            <div class="row">

                <div class="col-xs-12">

                    <div class="event-search">

                        <div class="hidden-xs form-box dark_blue">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="header">
                                        <div class="heading">
                                            <h2>Refine your view</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-container">

                                <div id="eventSearch">

                                    <div class="row">

                                        <div class="form-group col-xs-12">
                                            <input id="refine-event-txtbox" type="text" name="q" class="search form-control" placeholder="Search events">
                                        </div>

                                        <div class="form-group col-xs-12">
                                            <button type="button" id="refine-event-search" class="btn btn-filled-blue btn-full-width">Search</button>
                                        </div>
                                        <div class="form-group col-xs-12">
                                            <button type="button" id="refine-event-reset" class="btn btn-filled-navy btn-full-width">Reset View</button>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

        <!-- Events block -->
        <div class="col-xs-12 col-md-8">

            <div class="event-list-container">


                @foreach($events as $event)

                <!-- Event box -->
                <div class="event-box">

                    <div class="row">

                        <!-- Date -->
                        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-1">

                            <div class="date">
                                <p class="day">{{ $event->startDate->format('d') }}</p>
                                <p class="month">{{ $event->startDate->format('M') }}</p>
                            </div>

                        </div>

                        <!-- Picture -->
                        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">

                            <div class="event-image">
                                <a href="{{action('EventController@show',[$event->id, str_slug($event->title)])}}">
                                    <img class="img-responsive" alt="{{ $event->title }}" src="{{ $event->image }}">
                                </a>
                            </div>

                        </div>

                        <!-- Details -->
                        <div class="col-xs-12 col-sm-6 col-md-7 col-lg-7">

                            <div class="event-details">

                                <h2 class="title">
                                    <a href="{{action('EventController@show',[$event->id, str_slug($event->title)])}}">{{ $event->title }}</a>
                                </h2>
                                <p class="when">
                                    <span class="inline-title">When:</span> {{ $event->startDate->format('d M Y') }} | {{$event->startTime}}-{{$event->finishTime}}</p>
                                <p class="where">
                                    <span class="inline-title">Where:</span>{{ $event->location }}</p>

                            </div>

                        </div>

                    </div>

                </div>
                <!-- End -->


                @endforeach

                <div class="event-list-footer">
                    <div class="row">
                        <div class="col-xs-12">

                            {{ $events->links() }}

                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>

@endsection

@section('inline-script')
<script>                   
$(document).ready(function () {

    var currentEvents = $(".event-list-container").contents();
    var currentRequest = null;  
    var searchField;
    var expression; 

    $( "#refine-event-reset" ).click(function() {
        $('.event-list-container').empty();
        $('.event-list-container').append(currentEvents);
    });

        $.ajaxSetup({
        cache: false
    });

    $( "#refine-event-search" ).click(function() {
        searchField = $('#refine-event-txtbox').val();
        expression = new RegExp(searchField, "i"); 
        searchEvents(searchField, expression);
    });

    $('#refine-event-txtbox').on('keyup touchend', function () {
        searchField = $('#refine-event-txtbox').val();
        expression = new RegExp(searchField, "i"); 

        if(searchField.length < 2)
        {
            if(currentRequest != null) {
                currentRequest.abort();
            }

            $('.event-list-container').empty();
            $('.event-list-container').append(currentEvents);
        }
        else
        {
            searchEvents(searchField, expression);
        }
    });

    function searchEvents(searchField, expression)
    {
        currentRequest = jQuery.ajax({
            type: 'GET',
            url: '/api/events',
            beforeSend : function()    { 
                if(currentRequest != null) {
                    currentRequest.abort();
                }
            },
            success: function(data) {
                $('.event-list-container').empty();  
                $.each(data, function (key, value) {
                if (value.title.search(expression) != -1 ||
                    value.location.search(expression) !=
                    -1) {

                    var startDate = value.startDate;
                    var startDateDay = moment.utc(startDate).format('D');
                    var startDateMonth = moment.utc(startDate).format('MMM');
                    var fullDate = moment.utc(startDate).format('D MMM YYYY');
                    var eventURL = "/events/" + value.id + "/" + value.title;
                    eventURL = eventURL.replace(/\s+/g, '-').toLowerCase();
                    $('.event-list-container').append(
                        `<div class="event-box">
                            <div class="row">
                                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-1">
                                    <div class="date">
                                        <p class="day">${startDateDay}</p>
                                        <p class="month">${startDateMonth}</p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">
                                    <div class="event-image">
                                        <a href="${eventURL}">
                                            <img class="img-responsive" src="${value.image}">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-7 col-lg-7">
                                    <div class="event-details">
                                        <h2 class="title">
                                            <a href="${eventURL}">${value.title}</a>
                                        </h2>
                                        <p class="when"><span class="inline-title">When: </span>${fullDate} | ${value.startTime}-${value.finishTime}</p>
                                        <p class="where"><span class="inline-title">Where:</span>${value.location}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `);
                }
            });
            },
            error:function(e){
            // Error
            }
        });
    }
});
</script>
@append