@extends ('layouts.app')

@section ('content')

    <!-- Breadcrumbs -->
	<div class="container">
        <div class="row">
            
            <div class="col-xs-12">

                <div class="breadcrumb-bar">

                    <ul>
                    <li>
                        <a href="/" class="">Home</a>
                    </li>
                    <li>
                        <a href="/editable" class="">Editable Home</a>
                    </li>
                    <li>
                        <a href="/editable/events" class="">Editable Events</a>
                    </li>
                    </ul>

                </div>
            </div>
        
        </div>

    </div>

    <div class="container">
        <div class="row">

            <div class="col-md-4">
                <div class="create-event-sidebar-box">
                <button type="button" class="btn btn-filled-navy btn-full-width" data-toggle="modal" data-target="#createEventModal">Create New Event</button>
                </div>
                  <!--  include('layouts.calendar') -->
            </div>
            
            <div class="col-md-8">

                @if (\Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{ \Session::get('success') }}</p>
                    </div>
                @endif

                <div class="event-list-container">

                    @foreach($events as $event)

                    <!-- Event box -->
                    <div class="event-box">

                        <div class="row">

                            <!-- Date -->
                            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-1">

                                <div class="date">
                                    <p class="day">{{ $event->startDate->format('d') }}</p>
                                    <p class="month">{{ $event->startDate->format('M') }}</p>
                                </div>

                            </div>

                            <!-- Picture -->
                            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3">

                                <div class="event-image">
                                <a href="{{action('Editable\EventController@edit', $event['id'])}}">
                                    <img class="img-responsive" alt="{{ $event->title }}" src="{{ $event->image }}">
                                </a>
                                </div>

                            </div>

                            <!-- Details -->
                            <div class="col-xs-12 col-sm-6 col-md-7 col-lg-7">

                                <div class="event-details">

                                    <h2 class="title"><a href="{{action('Editable\EventController@edit', $event['id'])}}">{{ $event->title }}</a></h2>
                                    <p class="when"><span class="inline-title">When:</span> {{ $event->startDate->format('d F Y') }} | {{$event->startTime}}-{{$event->finishTime}}</p>
                                    <p class="where"><span class="inline-title">Where:</span>{{ $event->location }}</p>
                                    <form class="delete-event-form" action="{{action('Editable\EventController@destroy', $event['id'])}}" method="post">
                                        @csrf
                                        <input name="_method" type="hidden" value="DELETE">
                                        <a class="btn btn-primary" href="{{action('Editable\EventController@edit', $event['id'])}}">Edit</a>
                                        <button class="btn btn-danger" type="submit">Delete</button>
                                    </form>

                                </div>
                                
                            </div>

                        </div>

                    </div>
                    <!-- End -->

                    @endforeach

                </div>
                
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="createEventModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            @include('editable.events.create')
        </div>
    </div>

@endsection