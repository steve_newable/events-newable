@extends ('layouts.app') 

@section ('content')

<!-- Breadcrumbs -->
<div class="container">
    <div class="row">

        <div class="col-xs-12">

            <div class="breadcrumb-bar">

                <ul>
                    <li>
                        <a href="/" class="">Home</a>
                    </li>
                   
                    <li>
                        <a href="/editable/events/">Events Home</a>
                    </li>
                </ul>

            </div>
        </div>

    </div>

</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-8">

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif

            <div class="article">

                <div class="article-featured-image-box">



                    <img class="featured-image" style="width: 100%; height:auto;" src="{{$event['image']}}" alt="{{$event['title']}}">


                </div>

                <div class="article-content">
                    <div class="article-header">

                        <div class="article-meta-information">
                            <i class="far fa-calendar"></i> {{ $event->startDate->format('d F Y') }} |
                            <i class="far fa-clock"></i> {{ $event->startTime }}-{{ $event->finishTime }}

                        </div>

                        <div class="article-title">
                            {{ $event->title }}
                        </div>

                        @if(count($event->tags) != 0)

                        <div class="tag-row">
                            <span class="hidden-xs">Tags:</span>

                            <div class="tag-words-list">

                                @foreach($event->tags as $tag)

                                <a class="tag-word" href="{{ URL::action('TagController@show', $tag->slug) }}">{{ $tag->name }}</a>

                                @endforeach

                            </div>
                        </div>

                        @endif



                    </div>

                    <div class="article-body">

                        <h2 class="article-subheading">Description</h2>

                        <p>{!! $event->description !!}</p>

                        <h2 class="article-subheading">What to expect</h2>

                        <p>{{ $event->details }}</p>

                

                    </div>

                    <div class="article-footer">
                        <h2 class="article-subheading">Host department:</h2>
                        <p>{{ $event->hostedBy }}</p>
                    </div>

                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        Edit Event</button>

                    <form class="delete-event-form" action="{{action('Editable\EventController@destroy', $event['id'])}}" method="post">
                        @csrf
                        <input name="_method" type="hidden" value="DELETE">
                        <button class="btn btn-danger" type="submit">Delete</button>
                    </form>

                    <a href="/editable/events" class="btn btn-primary">Return to event edit home page</a>

                </div>

            </div>

        </div>

        <div class="col-md-4">
            <div class="event-sidebar-details-box">
                <h2 class="event-sidebar-title">Event Details</h2>
                <h3 class="event-sidebar-subtitle">When:</h3>
                <p>{{ $event->startDate->format('d M Y') }} | {{ $event->startTime }}-{{ $event->finishTime }}</p>
                <h3 class="event-sidebar-subtitle">Where:</h3>
                <p>{{ $event->location }}</p>
                <h3 class="event-sidebar-subtitle">Enquires:</h3>
                <p>t: 020 7403 0300</p>
                <p>e:
                    <a href="mailto:example@newable.com">example@newable.com</a>
                </p>
                <a class="btn btn-filled-navy btn-book-your-place" href="{{ $event->externalPageUrl }}">Book Your Place</a>
            </div>
              <div class="event-sidebar-map">
                {!! $location['html'] !!}
            </div>

            @section('inline-script') 
                @parent 
                {!! $location['js'] !!} 
            @endsection 
            
            <!--  include('layouts.calendar') -->

        </div>
        <!-- Sidebar ends -->


        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">

                    <div class="modal-body">

                        <form method="post" action="{{ action('Editable\EventController@update', $event->id) }}">


                            @csrf @method('patch')

                            <div class="form-group">

                                <label for="title">Title</label>
                                <input type="text" class="form-control" name="title" id="title" placeholder="Enter a descriptive name" value="{{$event->title}}"
                                    required>

                            </div>

                            <div class="form-group">

                                <label for="location">Location</label>
                                <input type="text" class="form-control" name="location" id="location" placeholder="Where is this event?" value="{{$event->location}}"
                                    required>

                            </div>

                            <div class="form-group">

                                <div class="row">

                                    <div class="col-md-6">

                                        <label for="location">Start</label>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="date" class="form-control" name="startDate" id="startDate" title="Event start date" value="{{$event->startDate->format('Y-m-d')}}"
                                                    required>
                                            </div>

                                            <div class="col-md-6">
                                                <input type="time" class="form-control" name="startTime" id="startTime" title="Event start time" value="{{$event->startTime}}"
                                                    required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">

                                        <label for="location">Finish</label>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="date" class="form-control" name="finishDate" id="finishDate" title="Event end date" value="{{$event->finishDate->format('Y-m-d')}}"
                                                    required>
                                            </div>

                                            <div class="col-md-6">
                                                <input type="time" class="form-control" name="finishTime" id="endTime" title="Event end time" value="{{$event->finishTime}}"
                                                    required>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="form-group">

                                <label for="image">Image</label>

                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                                            <i class="fa fa-picture-o"></i> Choose
                                        </a>
                                    </span>

                                    <input id="thumbnail" class="form-control" type="text" name="filepath" readonly="" value="{{ $event->image }}">

                                </div>

                                <small class="help-block">Pressing 'Choose' will allow you to select an image. Once selected, a preview will appear
                                    below.
                                </small>

                            </div>

                            <div class="form-group">

                                <div class="image-preview">

                                    <img src="{{ $event->image }}" class="preview" id="holder">

                                </div>

                            </div>


                            <div class="form-group">

                                <label for="description">Description
                                    <i class="far fa-question-circle" data-toggle="tooltip" data-placement="top" title="Add some relevant key information about this event"></i>
                                </label>
                                <textarea class="form-control editable" name="description" id="description" cols="30" rows="5" minlength="50" required>{{ $event->description }}</textarea>

                            </div>

                            <div class="form-group">

                                <label for="details">Key Details
                                    <i class="far fa-question-circle" data-toggle="tooltip" data-placement="top" title="What key things should attendees expect from this event?"></i>
                                </label>
                                <textarea class="form-control" name="details" id="details" cols="30" rows="5">{{ $event->details }}</textarea>

                            </div>

                            <div class="form-group">

                                <label for="externalPageUrl">External event link
                                    <i class="far fa-question-circle" data-toggle="tooltip" data-placement="top" title="Link to a third party page for this event e.g. EventBrite"></i>
                                </label>
                                <input type="url" class="form-control" name="externalPageUrl" id="externalPageUrl" placeholder="Link to external event page"
                                    value="{{ $event->externalPageUrl }}">

                            </div>

                            <div class="form-group">

                                <div class="row">

                                    <div class="col-md-6">

                                        <label for="location">Hosted By</label>

                                        <div class="row">

                                            <div class="col-md-12">
                                                <select name="hostedBy" class="form-control" id="hostedBy">
                                                    <option value="">Please select</option>
                                                    <option @if($event[ 'hostedBy']=="Events" ) selected @endif value="Events">Events</option>
                                                    <option @if($event[ 'hostedBy']=="Marketing" ) selected @endif value="Marketing">Marketing</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">

                                        <label for="location">Event Sector</label>

                                        <div class="row">

                                            <div class="col-md-12">
                                                <input type="text" class="form-control" name="sector" id="sector" title="Event sector" value="{{$event->sector}}" placeholder="Sector">
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="form-group">
                                <label for="featuredVideo">Tags:</label>
                                <input type="text" class="form-control" name="tags">
                            </div>

                            <div class="form-group">

                                <div class="row">

                                    <div class="col-md-4">

                                        <label for="location">Event Type</label>

                                        <div class="row">

                                            <div class="col-md-12">
                                                <select name="type" class="form-control" id="type">

                                                    <option value="">Please select</option>
                                                    <option @if($event[ 'type']=="Event" ) selected @endif value="Event">Event</option>
                                                    <option @if($event[ 'type']=="EEN Partner Event" ) selected @endif value="EEN Partner Event">EEN Partner Event</option>
                                                    <option @if($event[ 'type']=="Trade Mission" ) selected @endif value="Trade Mission">Trade Mission</option>
                                                    <option @if($event[ 'type']=="Newable Event" ) selected @endif value="Newable Event">Newable Event</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">

                                        <label for="location">Event Category</label>

                                        <div class="row">

                                            <div class="col-md-12">
                                                <select name="category" class="form-control" id="type">

                                                    <option value="">Please select</option>
                                                    <option @if($event[ 'category']=="Master Class" ) selected @endif value="Master Class">Master Class</option>
                                                    <option @if($event[ 'category']=="Brokerage Event" ) selected @endif value="Brokerage Event">Brokerage Event</option>
                                                    <option @if($event[ 'category']=="Meet the Buyer" ) selected @endif value="Meet the Buyer">Meet the Buyer</option>
                                                    <option @if($event[ 'category']=="Workshop" ) selected @endif value="Workshop">Workshop</option>
                                                    <option @if($event[ 'category']=="Seminar" ) selected @endif value="Seminar">Seminar</option>
                                                    <option @if($event[ 'category']=="Company Mission" ) selected @endif value="Company Mission">Company Mission</option>
                                                    <option @if($event[ 'category']=="Trade Mission" ) selected @endif value="Trade Mission">Trade Mission</option>
                                                    <option @if($event[ 'category']=="GBAP" ) selected @endif value="GBAP">GBAP</option>

                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label for="published">Status</label>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">

                                                    <select name="published" class="form-control">
                                                        <option value="pending" @if($event[ 'published']=="pending" ) selected @endif>Pending</option>
                                                        <option value="archived" @if($event[ 'published']=="archived" ) selected @endif>Archived</option>
                                                        <option value="published" @if($event[ 'published']=="published" ) selected @endif>Published</option>

                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="modal-footer ">
                                <div class="row">

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                    </div>

                                </div>
                            </div>


                        </form>


                    </div>


                </div>

            </div>

        </div>
    </div>
</div>

@endsection