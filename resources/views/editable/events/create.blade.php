<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Create New Event</h4>
    </div>
    <form method="POST" action="{{url('editable/events')}}" enctype="multipart/form-data">
        @csrf

        <div class="modal-body">

            @if ($errors->any())
                <div class="form-group">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif

            <!-- Creation form -->
            <div class="form-group">

                <label for="title">Title</label>
                <input type="text" class="form-control" name="title" id="title" placeholder="Enter a descriptive event title" value="{{ old('title') }}" required>

            </div>

            <div class="form-group">

                <label for="location">Location</label>
                <input type="text" class="form-control" name="location" id="location" placeholder="Where is this event?" value="{{ old('location') }}" required>

            </div>

            <div class="form-group">

                <div class="row">

                    <div class="col-md-6">

                        <label for="location">Start</label>

                        <div class="row">
                            <div class="col-md-6">
                                <input type="date" class="form-control" name="startDate" id="startDate" title="Event start date" placeholder="Where is this event?"
                                value="{{ old('startDate') }}" required>
                            </div>

                            <div class="col-md-6">
                                <input type="time" class="form-control" name="startTime" id="startTime" title="Event start time" placeholder="Where is this event?"
                                value="{{ old('startTime') }}" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">

                        <label for="location">Finish</label>

                        <div class="row">
                            <div class="col-md-6">
                                <input type="date" class="form-control" name="finishDate" id="finishDate" title="Event end date" placeholder="Where is this event?"
                                value="{{ old('finishDate') }}" required>
                            </div>

                            <div class="col-md-6">
                                <input type="time" class="form-control" name="finishTime" id="finishTime" title="Event end time" placeholder="Where is this event?"
                                 value="{{ old('finishTime') }}" required>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="form-group">

                <label for="image">Image</label>

                <div class="input-group">
                    <span class="input-group-btn">
                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                            <i class="fa fa-picture-o"></i> Choose
                        </a>
                    </span>

                    <input id="thumbnail" class="form-control" type="text" name="filepath" readonly="" value="{{ old('filepath') }}">

                </div>

                <small class="help-block">Pressing 'Choose' will allow you to select an image. Once selected, a preview will appear below.</small>

            </div>

            <div class="form-group">

                <div class="image-preview">

                    <img class="preview" id="holder">

                </div>

            </div>


            <div class="form-group">

                <label for="description">Description
                    <i class="far fa-question-circle" data-toggle="tooltip" data-placement="top" title="Add some relevant key information about this event"></i>
                </label>
                <textarea class="form-control editable" name="description" id="description" cols="30" rows="5">{{ old('description') }}</textarea>

            </div>

            <div class="form-group">

                <label for="details">Key Details
                    <i class="far fa-question-circle" data-toggle="tooltip" data-placement="top" title="What key things should attendees expect from this event?"></i>
                </label>
                <textarea class="form-control" name="details" id="details" cols="30" rows="5">{{ old('details') }}</textarea>

            </div>

            <div class="form-group">

                <label for="externalPageUrl">External event link
                    <i class="far fa-question-circle" data-toggle="tooltip" data-placement="top" title="Link to a third party page for this event e.g. EventBrite. Links must start with http://"></i>
                </label>
                <input type="url" class="form-control" name="externalPageUrl" id="externalPageUrl" value="{{ old('externalPageUrl') }}" placeholder="Link to external event page">

            </div>

            <div class="form-group">

                <label for="hostedBy">Hosted By</label>
                <select name="hostedBy" class="form-control" id="hostedBy">

                    <option value="">Please select</option>
                    <option {{ old('hostedBy') == "Events" ? 'selected' : '' }} value="Events">Events</option>
                    <option {{ old('hostedBy') == "Marketing" ? 'selected' : '' }} value="Marketing">Marketing</option>

                </select>

            </div>

            <div class="form-group">
                <label for="tags">Tags:</label>
                <input type="text"  value="{{ old('tags') }}" class="form-control" name="tags">
            </div>



            <div class="form-group">

                <div class="row">

                    <div class="col-md-4">

                        <label for="location">Event Type</label>

                        <div class="row">

                            <div class="col-md-12">
                                <select name="type" class="form-control" id="type">

                                    <option value="">Please select</option>
                                    <option {{ old('type') == "Event" ? 'selected' : '' }} value="Event">Event</option>
                                    <option {{ old('type') == "EEN Partner Event" ? 'selected' : '' }} value="EEN Partner Event">EEN Partner Event</option>
                                    <option {{ old('type') == "Trade Mission" ? 'selected' : '' }} value="Trade Mission">Trade Mission</option>
                                    <option {{ old('type') == "Newable Event" ? 'selected' : '' }} value="Newable Event">Newable Event</option>

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">

                        <label for="location">Event Category</label>

                        <div class="row">

                            <div class="col-md-12">
                                <select name="category" class="form-control" id="category">

                                    <option value="">Please select</option>
                                    <option {{ old('category') == "Master Class" ? 'selected' : '' }} value="Master Class">Master Class</option>
                                    <option {{ old('category') == "Brokerage Event" ? 'selected' : '' }} value="Brokerage Event">Brokerage Event</option>
                                    <option {{ old('category') == "Meet the Buyer" ? 'selected' : '' }} value="Meet the Buyer">Meet the Buyer</option>
                                    <option {{ old('category') == "Workshop" ? 'selected' : '' }} value="Workshop">Workshop</option>
                                    <option {{ old('category') == "Seminar" ? 'selected' : '' }} value="Seminar">Seminar</option>
                                    <option {{ old('category') == "Company Mission" ? 'selected' : '' }} value="Company Mission">Company Mission</option>
                                    <option {{ old('category') == "Trade Mission" ? 'selected' : '' }} value="Trade Mission">Trade Mission</option>
                                    <option {{ old('category') == "GBAP" ? 'selected' : '' }} value="GBAP">GBAP</option>

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">

                        <label for="sector">Event Sector</label>

                        <div class="row">

                            <div class="col-md-12">
                                <input type="text" class="form-control" name="sector" id="sector" title="Event sector" placeholder="Sector" value="{{ old('sector') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <div class="modal-footer">
        <div class="form-group col-md-offset-10 col-md-2">
            <button type="submit" class="btn btn-primary ">Submit</button>
        </div>
        </form>
    </div>
</div>

@section('inline-script')

<script>
    document.addEventListener("DOMContentLoaded", function (event) {
        $('#startDate').blur(function () {
            if ($('#finishDate').val().length === 0) {
                $('#finishDate').val($(this).val());
            }
        });
    });
</script>

@if(!empty(Session::get('error')) || $errors->any())
    <script>
    $(function() {
        $('#createEventModal').modal('show');
    });
    </script>
@endif

@append