@extends ('layouts.app') 

@section('title',  ' | Events') 

@section ('content')


	
	<div class="container mb-40 mt-40">
	    	<div class="row">

            	<div class="col-md-3 ">

               <div class="article">

                <div class="article-featured-image-box">

                    <img class="featured-image" src="{{ $event->image }}" alt="{{ $event->title }}">

                </div>

                <div class="article-content">

                    <div class="article-header">

                        <div class="article-title">
                            {{ $event->title }}
                        </div>
                   </div>
             
               </div>

              </div>
                    <!-- Article buttons -->

                           <div class="event-sidebar-details-box">

                <h2 class="event-sidebar-title">Event Details</h2>

                <h3 class="event-sidebar-subtitle">When:</h3>

                <p>{{ $event->startDate->format('d M Y') }} | {{ $event->startTime }}-{{ $event->finishTime }}</p>

                <h3 class="event-sidebar-subtitle">Where:</h3>

                <p>{{ $event->location }}</p>

                <h3 class="event-sidebar-subtitle">Enquiries:</h3>

                <p>t: 020 7403 0300</p>

                <p>e:
                    <a href="mailto:example@newable.com">example@newable.com</a>
                </p>

                <a class="btn btn-filled-navy btn-book-your-place" href="{{ $outlookEvent }}">Add to Calendar</a>

              

            </div>

                </div>

     

	    		<div class="col-md-9 ">
	    		
					<h3>Company/attendee Information</h3>

	
	<form method="post" enctype="multipart/form-data" action="" data-toggle="validator">	
	

	
	</form>
			
	<form method="post" enctype="multipart/form-data" action="" data-toggle="validator">					
															
	<!--Create new account-->								
	<input type="hidden" name="Name"  value="">
	<input type="hidden" name="Name2"  value="">
	<input type="hidden" name="CONF_AccountId" id="CONF_AccountId" value="">
	<input type="hidden" name="RecordTypeId" id="RecordTypeId" value="0120Y000000ym54QAA">
	<input type="hidden" name="AccountId" id="AccountId" value="">		
    <input type="hidden" name="ContactId" id="ContactId" value="">
	<input type="hidden" name="Campaign_Member_Id" id="Campaign_Member_Id" value="">
	<input type="hidden" name="Status" id="Status" value="">				
    <!--Create new account ends-->									
								
	<!--Create new contact-->						
	<div class="row">
	    
		<div class="col-md-2">
			<div class="form-group">
				<label for="Salutation">Salutation:</label>
				<input type="text"  class="form-control" id="Salutation" value=""  name="Salutation" >
			</div>
		</div> 
		
		<div class="col-md-4">
			<div class="form-group">
				<label for="Forename">First name(s): <span style="color:red">*</span></label></label>
				<input type="text" class="form-control" name="Forename"  id="Forename" placeholder="Required" required value="" >
			</div>
		</div>		
								
		<div class="col-md-4">
			<div class="form-group">
				<label for="Surname">Last name: <span style="color:red">*</span></label>
				<input type="text" class="form-control" name="Surname"  id="Surname" placeholder="Required" required value="">
			</div>
		</div>
		
		<div class="col-md-2">
			<div class="form-group">
				<label for="Suffix">Suffix:</label>
				<input type="text" class="form-control" name="Suffix" id="Suffix" value="">
			</div>
		</div>
		
	</div>
	
	<div class="row">
     
     <div class="col-md-6">
				<div class="row">	
			
		<div class="col-md-12">
	
		  <div class="form-group">
			<label for="Email">Email address: <span style="color:red">*</span></label>
			<input name="Email" type="email" readonly class="form-control" id="Email" value="" >
		  </div>
		</div>
		
		 <div class="col-md-12">	
		<div class="form-group">
		 <label for="Title">Job title: <span style="color:red">*</span></label>
			<input type="text" class="form-control" name="Title"  id="Title" required value="">
		</div>
		</div>										
							
								
		<div class="col-md-6">
		  <div class="form-group">
			<label for="Phone">Telephone:</label>
			<input type="tel" class="form-control" name="Phone" id="Phone" value="">
		  </div>
		</div>
						

  	   	<div class="col-md-6">
		  <div class="form-group">
			<label for="MobilePhone">Mobile: <span style="color:red">*</span></label>
			<input type="tel" class="form-control" name="MobilePhone" id="MobilePhone" required value="">
		  </div>
		</div>
	 
						
				
						
							
	</div>
		</div>	
     			
     <div class="col-md-6">
	
	<div class="row">	
	    
	   <div class="col-md-12">
		  	
		<div class="form-group">
		 <label for="ShippingStreet">Street: <span style="color:red">*</span></label>
			<input type="text" class="form-control" name="ShippingStreet" id="ShippingStreet" required
			value="">
		</div>
		
		 </div>
			  
	   <div class="col-md-6">
	 
	  <div class="form-group">
	   <label for="Shipping_City">City: <span style="color:red">*</span></label>
	   <input type="text" class="form-control" name="Shipping_City" id="Shipping_City" required
	   value="">
	 </div>
	 
	 </div>
	 
	   <div class="col-md-6">
			<div class="form-group">
		 <label for="Shipping_State">State:</label>
			<input type="text" class="form-control" name="Shipping_State" id="Shipping_State" value="">
		</div>
		</div>
	 		  
	   <div class="col-md-5">
	 <div class="form-group">
	   <label for="ShippingPostalCode">Postal Code: <span style="color:red">*</span></label>
	   <input type="text" class="form-control" name="ShippingPostalCode"">
	 </div>
		</div>
			 
	   <div class="col-md-7">  
		<div class="form-group">
		 <label for="Website">Website: <span style="color:red">*</span></label>
			<input type="text" class="form-control" name="Website" id="Website" required value="">
		</div>
		</div>
	
	</div>
	
		</div>
			
	</div>
	<!--Create new contact ends-->
	
	<!--Event specific include-->
		
 @include ('partials.registration')
	
     <div class="row">	
	    

	
	<!--Edit contact Button-->
						
	<div class="col-md-12">																						
	  <button type="submit" name="Edit_Campaign" id="Edit_Campaign" class="btn btn-newable-rectangle btn-block">Submit application</button>   
	
	</div>	 
				   

					   

		
     <div class="col-md-12">														
      <!--Create new contact Button-->									
	   <button type="submit" name="Add_New_Contact" id="Add_New_Contact" class="btn btn-newable-rectangle btn-block">Submit application</button> 
	</div>
		
	

	
			
	 <div class="col-md-12">						
	 <!--Edit contact Button-->										
	  <button type="submit" name="Join_Campaign" id="Join_Campaign" class="btn btn-newable-rectangle btn-block">Submit application</button>  
	</div>
	
  	

		
	</div>			
							
	</form>
						
	</div>
   </div>
  </div>


    @endsection
