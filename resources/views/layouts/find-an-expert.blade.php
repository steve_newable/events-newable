<div class="form-box dark_grey">
        <div class="row">
            <div class="col-xs-12">
                <div class="header">
                    <div class="heading">
                        <h2>Find an Expert</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-container">

            <form method="get" action="{{ action('SearchController@expertSearch') }}">
                <div class="row">

                    <div class="form-group col-xs-12 col-md-12">
                        <label for="search-box">I need to talk to someone about</label>
                        <input type="text" class="form-control filled" id="search-box" name="q" placeholder="eg: event organisation">
                    </div>

                    <div class="form-group col-xs-12 col-md-12">
                        <button type="submit" id="find-contact" class="btn btn-filled-blue btn-block no-margin">Search</button>
                    </div>
                </div>
            </form>

        </div>

    </div> 