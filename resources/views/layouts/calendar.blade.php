<section id="calendar">
    <div id="events-widget">
        <div class="widget-heading">

            <div class="row header">

                <div class="col-xs-12">
                    <h2 class="events-title">Newable Events</h2>
                </div>

            </div>


            <div id="calendar-widget">

                {!! $calendar_events->calendar() !!} 
                
                @section('inline-script')

                @parent

                <script>
                    function getEventDate(event) {
                        var dateobj = event.start.format('YYYY-MM-DD');
                        return dateobj;
                    }

                    function getEventUrl(event) {
                        var eventUrl = event.url;

                        return eventUrl;
                    }
                    $(document).ready(function () {
                        $('#event-slider').slick({
                            adaptiveHeight: true,
                            dots: false,
                            infinite: true,
                            speed: 300,
                            fade: true,
                            cssEase: 'linear',
                            appendArrows: $(".event-slider-nav")
                        });
                    });
                </script>

                {!! $calendar_events->script() !!} 
                
                
                @stop

            </div>

            @if(!Request::is('events') && !Request::is('editable/events'))

            <div id="event-slider">

                @foreach($all_events as $event)

                <div class="event-slide">

                    <div class="event-date">
                        <p class="event-day">{{ $event->startDate->format('d') }}</p>
                        <p class="event-month">{{ $event->startDate->format('M') }}</p>
                    </div>

                    <div class="event-details">

                        <h3 class="event-title">
                            <a href="{{action('EventController@show',[$event->id, str_slug($event->title)])}}">{{ $event->title }}</a>
                        </h3>

                        <div class="event-details-group">
                            <p class="event-subtitle">When:</p>
                            <p class="event-detail">{{ $event->startDate->format('d M Y') }}</p>
                            <p class="event-detail">{{ $event->startTime }}-{{ $event->finishTime }}</p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="event-details-group">
                            <p class="event-subtitle">Where:</p>
                            <p class="event-detail event-location">{{ $event->location }}</p>
                        </div>

                    </div>

                </div>

                @endforeach

            </div>

            @endif

            <div class="event-slider-nav"></div>

        </div>
</section>