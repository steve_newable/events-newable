<div class="grid-item grid-item-full">
    <section class="widget" id="poll-results">

            <div class="row header">

                    <div class="col-xs-11">
                        <div class="heading">
                            <h2>Poll Results</h2>
                        </div>
                    </div>
        
                    <div class="col-xs-2 AddWidget">
                            @include('partials.widget-button')
                    </div>
                
                </div>
        
                <div class="row">
                    <div class="col-md-12">
                        <div class="poll-chart-container" style="width: 100%; margin: 0 auto;">
                            <div class="poll-chart-slider">
                                @for ($i = 0; $i < sizeof($polls); $i++)
                                <div class="poll-chart">
                                    <h3 class="poll-title">{{$polls[$i][0]['title']}}</h3>
                                    <div id="chart_div{{$i}}"></div>
                                </div>
                                @endfor
                                @if (sizeof($polls) == 0)
                                <p class="no-polls-text">Start voting on some polls to see the results.</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>


    </section>
</div>

@section('inline-script')

    <script>
            $(document).ready(function(){
                $('.poll-chart-slider').slick({
                    dots: true,
                    nextArrow: '<i class="fas fa-arrow-right"></i>',
                    prevArrow: '<i class="fas fa-arrow-left"></i>',
                    infinite: false,
                    
                });
            });
    </script>


<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function(event) {

        // Load the Visualization API and the corechart package.
        google.charts.load('current', {'packages':['corechart']});

        // Set a callback to run when the Google Visualization API is loaded.
        @for ($i = 0; $i < sizeof($polls); $i++)
        google.charts.setOnLoadCallback(drawChart{{$i}});
        @endfor

        // Callback that creates and populates a data table,
        // instantiates the pie chart, passes in the data and
        // draws it.
        @for ($i = 0; $i < sizeof($polls); $i++)

        function drawChart{{$i}}() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', '');
        data.addColumn('number', 'Votes');
        data.addRows([
            @foreach($pollsOptions[$i] as $pollOption)
            ['{{$pollOption["answer"]}}', {{$pollOption["voteAmount"]}}],
            @endforeach
        ]);

        // Set chart options
        var options = {
            
            'title':'',
            'width':900,
            'height':350,
            'pieHole': 0.4,
            'is3D': true,
            'sliceVisibilityThreshold': 0,
            'chartArea': {'width': '80%', 'height': '80%', 'bottom' : '70'},
            'legend': {'position': 'bottom'},
            'colors': ['#0af', '#0081c1', '#005d8c', '#083875', '#49c2ff', '#0077b3', '#90daff']
                    
        };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div{{$i}}'));
        chart.draw(data, options);

        }
        @endfor
    });
</script>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js" defer></script>

@append