<div class="padded-content-box">

        <div class="header">

            <div class="row">

                <div class="col-xs-12 col-md-12">

                    <h1 class="heading">
                      Tag Cloud
                    </h1>

                </div>

            </div>

        </div>

        <div class="content">

     
                 @foreach($usedTags as $tag)
    
                       @php $tag_size = ($tag->articles_count + $tag->events_count) ;
                       if ($tag_size >1){
                          $tag_size = $tag_size*0.50;
                       }
                    
                       @endphp
                            <a style="font-size:{{$tag_size}}em;" class="tag-cloud" title="{{ $tag->name }}" href="{{ URL::action('TagController@show', $tag->slug) }}">{{ $tag->name }}</a> 
                        
                      
                        @endforeach

        </div>

    </div>