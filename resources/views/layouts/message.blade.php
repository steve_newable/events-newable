@if($flash = session('flash_message'))

    <div class="alert alert-info alert-dismissible fade in" role="alert">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        
        {{ $flash }}
    
    </div>

@endif