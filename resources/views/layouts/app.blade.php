@include ('layouts.head')

@yield('inline-style')



<body @if(!empty($bodyClass)) class="{{$bodyClass}}" @endif>
    <!-- Navigation -->
    @include ('layouts.nav')

    @yield('content')

  

    <!-- Scripts -->
    @include('layouts.scripts')
    
    @yield('inline-script')
    
</body>
</html>