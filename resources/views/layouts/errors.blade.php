@if(count($errors) > 0)
    <div id="error-messages" class="alert alert-warning alert-dismissible fade in" role="alert">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        
        @foreach($errors->all() as $error)

            {{ $error }}<br />

        @endforeach
    </div>
@endif