<!-- Core JavaScript -->
<script src="{{ mix('js/app.js') }}"></script>

<script src="https://unpkg.com/packery@2/dist/packery.pkgd.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/draggabilly/2.1.1/draggabilly.pkgd.js"></script>

<!-- TinyMCE WYSIWYG Editor -->
<script src="{{asset('vendor/tinymce/jquery.tinymce.min.js')}}"></script>
<script src="{{asset('vendor/tinymce/tinymce.min.js')}}"></script>
<script src="{{asset('vendor/tinymce/tinymce-settings.js')}}"></script>

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<!-- Laravel Filemanager -->
<script src="{{asset('vendor/laravel-filemanager/js/lfm.js')}}"></script>

<script>               
if($('#lfm').length > 0){
    $('#lfm').filemanager('image');
}


if($('#lfm-file').length > 0){
    $('#lfm-file').filemanager('file');

}
</script>











