	<nav class="navbar navbar-default navbar-custom" role="navigation">
		<div class="container">
			<div class="row">
				
				<div class="col-md-12">

					<div class="navbar-header page-scroll">

						<button id="" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">

							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>

						</button>

						<a class="navbar-brand" href="/"><img src="/assets/img/logos/newable-logo-blue.svg" width="auto" height="20" alt="" class="invisible-desktop"/></a>

					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse">
			

						<ul class="nav navbar-nav pull-right">

                      
							<li><a href="/events">Events</a></li>
						<li> <a href="/editable/events">Manage Events</a></li>
						</ul>

					</div><!-- /.navbar-collapse -->
				</div>   
			</div>
		</div><!-- /.container -->
	</nav>