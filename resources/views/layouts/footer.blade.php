<div id="footer" class="footer center-text pb-31">
    <div class="container">

        <div class="row">

            <div class="col-sm-3 col-md-2 col-lg-2 pt-40">
                <img class="logo" src="/assets/img/logos/newable-logo-grey.svg" width="130" height="auto" alt="Grey Newable logo" />
            </div>

            <div class="col-sm-2 col-md-2 col-lg-1 pt-40">
                <b>Pages</b>
                <br/>
                <a href="{{ action('CultureController@index') }}">Culture</a>
                <br/>
                <a href="{{ action('ArticleController@index') }}">News</a>
                <br/>
                <a href="{{ action('TeamController@index') }}">People</a>
                <br/>
                <a href="{{ action('EventController@index') }}">Events</a>
                <br />
                <a href="{{ action('TemplateController@index') }}">Library</a>
                <br />
                <a href="{{ action('WidgetController@index') }}">Widgets</a>
                <br />
                <a href="{{ action('TagController@index') }}">Tags</a>
            </div>

            <div class="col-sm-2 col-md-2 col-lg-2 pt-40">
                <b>Your Pages</b>
                <br/>
                <a href="{{ action('LikeController@index') }}">Things you've liked</a>
                <br/>
                <a href="{{ action('UserController@show', [auth()->user()->department, auth()->user()->username]) }}">Your profile</a>
                <br/>
                <a href="{{ action('MessageController@index') }}">Messages</a>
                <br/>
                <a href="{{ action('TaskController@index') }}">Tasks</a>
                <br/>
            </div>

            <div class="col-sm-3 col-md-2 col-lg-2 pt-40">
                <b>Your Privileges</b>
                <br/> 

                <a href="{{ action('ProfileController@index', auth()->user()->username) }}">Edit your profile</a><br />

                @hasanyrole('Admin|Help Desk')
                    <a href="/admin">Manage Users</a><br />
                @endrole 

                @hasanyrole('News Contributor|News Approver')
                    <a href="/editable/news-and-updates">Manage Articles</a><br />
                @endrole
                
                @role('Spotlight Manager')
                    <a href="/editable/spotlight">Manage Spotlights</a><br />
                @endrole 
                
                @hasanyrole('Events Contributor|Events Approver')
                    <a href="/editable/events">Manage Events</a><br />
                @endrole 
                
                @role('Team Manager')
                    <a href="/editable/teams/{{ auth()->user()->department }}">Manage your Team profile</a><br />
                @endrole 
                
                @role('Admin')
                    <a href="/editable/tags">Manage Tags</a><br />
                    <a href="/editable/polls">View active Polls</a><br />
                @endrole

            </div>

            <div class="col-sm-2 col-md-2 col-lg-2 pt-40">
                <b>Registered office:</b>
                <br/>
                <span class="reg-office">
                    140 Aldersgate Street
                    <br/> London
                    <br/> EC1A 4HY</span>
            </div>

            <div class="social col-xs-12 col-sm-12 col-md-12 col-lg-2 pt-40">

                <ul>
                    <li>
                        <a title="Facebook" href="https://www.facebook.com/Newable-1603765719919998/" onClick="ga('send', 'event', 'Social Profile view', 'Click', 'Facebook');"
                            target="_blank" rel="noopener">
                            <img id="facebook" src="/assets/img/social-icons/facebookft-box-rounded.svg" width="auto" height="26" alt="Newable Facebook page link"
                            />
                        </a>

                    </li>

                    <li>
                        <a title="Twitter" href="https://twitter.com/Newable" onClick="ga('send', 'event', 'Social Profile view', 'Click', 'Twitter');"
                            target="_blank" rel="noopener">
                            <img id="twitter" src="/assets/img/social-icons/twitterft-box-rounded.svg" width="auto" height="26" alt="Newable Twitter page link"
                            />
                        </a>

                    </li>

                    <li>
                        <a title="LinkedIn" href="https://www.linkedin.com/company/15222947?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A15222947%2Cidx%3A2-3-4%2CtarId%3A1477919143019%2Ctas%3Anewab"
                            onClick="ga('send', 'event', 'Social Profile view', 'Click', 'LinkedIn');" target="_blank" rel="noopener">
                            <img id="linked" src="/assets/img/social-icons/linkedinft-box-rounded.svg" width="auto" height="26" alt="Newable LinkedIn page link"
                            />
                        </a>

                    </li>

                    <li>
                        <a title="Vimeo" href="https://vimeo.com/newable" onClick="ga('send', 'event', 'Social Profile view', 'Click', 'Vimeo');"
                            target="_blank" rel="noopener">
                            <img id="vimeo" src="/assets/img/social-icons/vimeoft-box-rounded.svg" width="auto" height="26" alt="Newable Vimeo page link"
                            />
                        </a>

                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div id="lowerfooter">
    <div class="container">
        <div class="row grey">
            <div class="col-md-12">

                <h6>Registered in England and Wales | Registered number: 1653116 | VAT number: 237 9198 23 | Copyright &copy;
                    2017 Newable Limited</h6>

            </div>
        </div>
    </div>
</div>