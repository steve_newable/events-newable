<!-- Search form -->
<section class="widget" id="search">

    <div class="form-box dark_grey">



        <div class="row header">

            <div class="col-xs-12">
                <div class="heading">
                    <h2>Quick search a contact</h2>
                </div>
            </div>




        </div>

        <div class="form-container">

            <form method="get" action="{{ action('SearchController@quickSearch') }}">

                <div class="row">

                    <div class="form-group col-xs-12 col-md-12">
                        <label for="department">Department</label>

                        <select class="form-control transparant" id="department" name="department">
                            <option value="">Select</option>

                            @foreach($departments as $department)
                            <option value="{{ $department }}">{{ $department }}</option>
                            @endforeach

                        </select>
                    </div>

                    <div class="form-group col-xs-12 col-md-12">
                        <label for="role">Role</label>

                        <select class="form-control transparant" id="role" name="role">
                            <option value="">Select</option>

                            @foreach($roles as $role)
                            <option value="{{ $role }}">{{ $role }}</option>
                            @endforeach

                        </select>
                    </div>

                    <div class="form-group col-xs-12 col-md-12">
                        <label for="location">Location</label>

                        <select class="form-control transparant" id="location" name="location">
                            <option value="">Select</option>

                            @foreach($locations as $location)
                            <option value="{{ $location }}">{{ $location }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-xs-12 col-md-12">
                        <button class="btn btn-filled-blue btn-block reset" title="Reset search filter">
                            <i class="fas fa-sync-alt"></i> Reset filter
                        </button>
                    </div>

                    <div class="form-group col-xs-12 col-md-12">
                        <button type="submit" id="find-contact" class="btn btn-filled-blue btn-block">Find contact</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

</section>