<div class="form-box dark_grey">
        <div class="row">
            <div class="col-xs-12">
                <div class="header">
                    <div class="heading">
                        <h2>Find a colleague</h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-container">

            <form method="get" action="{{ action('SearchController@colleagueSearch') }}">
                <div class="row">

                    <div class="form-group col-xs-12 col-md-12">
                        <label for="search-box">Enter a name, or part of a name</label>
                        <input type="text" class="form-control filled" id="search-box" name="q" placeholder="eg: Lesley">
                    </div>

                    <div class="form-group col-xs-12 col-md-12">
                        <button type="submit" id="find-contact" class="btn btn-filled-blue btn-block no-margin">Search</button>
                    </div>
                </div>
            </form>

        </div>

    </div> 