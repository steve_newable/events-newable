 @if(Route::current()->uri() == 'welcome')

<form action="{{ action('WidgetController@destroy', $widgetNo ) }}" method="post">
    @csrf @method('delete')
    <button type="submit" class="widget-btn remove-widget-btn">
        <i class="fa fa-bars "></i>
    </button>
</form>

@elseif(Route::current()->uri() == 'widget-library') 

    @if($active)
        <i class="fa fa-bars "></i>
    @else
        <form action="{{action('WidgetController@store')}}" method="POST">
            @csrf
            <input type="hidden" name="user_id" value="{{ $user->id }}">
            <input type="hidden" name="widget_id" value="{{ $widgetNo }}">

            <button type="submit" class="widget-btn add-widget-btn">
                <i class="fa fa-plus "></i>
            </button>
        </form>
    @endif

@endif