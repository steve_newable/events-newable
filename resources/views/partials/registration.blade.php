<div  id="formNav">

      <div class="row ">
 	  
       <div class="col-md-6">

       <div class="row">
  	
	    <div class="col-md-12">
		  
	       <div class="form-group">
	        <label for="CRN">Company registration number (CRN): <span style="color:red">*</span></label>
	        <input name="CRN" type="text" class="form-control"  id="CRN" value="" required>
	      </div>
			  
		 </div>
			  
	    <div class="col-md-12">
			
		   <div class="form-group">
		    <label for="Started_Trading">Date of incorporation: <span style="color:red">*</span></label>
			<input type="date" class="form-control" name="Started_Trading" id="Started_Trading" required value="">
		   </div>
		   
		 </div>
			  
		<div class="col-md-12">
		  		 
	      <div class="form-group">
		<label for="Industry">Industry sector: <span style="color:red">*</span></label>
		 <select class="form-control" required name="Industry" id="Industry" >
  
			
		
		 <option value="Aerospace">Aerospace</option>
	     <option value="Business and Financial Services">Business and Financial Services</option>
	     <option value="Communications, Publishing">Communications, Publishing</option>
	     <option value="Consumer Goods, Retail">Consumer Goods, Retail</option>
         <option value="Creative, Gaming, Apps, Experience Economy">Creative, Gaming, Apps, Experience Economy</option>
	     <option value="Education">Education</option>
	     <option value="Energy, Oil & Gas">Energy, Oil & Gas</option>
	     <option value="Environment, Utilities">Environment, Utilities</option>
         <option value="Fashion (including accessories)">Fashion (including accessories)</option>
	     <option value="Food & Drink">Food & Drink</option>
	     <option value="Healthcare, Life Sciences, Pharmaceuticals">Healthcare, Life Sciences, Pharmaceuticals</option>
	     <option value="Infrastructure, Construction, Engineering, Global Sports">Infrastructure, Construction, Engineering, Global Sports</option>
         <option value="Manufacturing, Chemicals">Manufacturing, Chemicals</option>
	     <option value="Marine">Marine</option>
	     <option value="Mass Transport, Ports & Logistics">Mass Transport, Ports & Logistics</option>
	     <option value="Security, Defence, Cyber Security">Security, Defence, Cyber Security</option>
         <option value="Technology, Digital, Software">Technology, Digital, Software</option>
	     <option value="Other">Other</option>
 
	    </select>
		</div>
		 
			 
		</div>
			  	  
	   </div>
  
	   </div>

	   <div class="col-md-6">
	   
	    <div class="row">
		  
		 <div class="col-md-12">
			 
			 	  <div class="form-group">
		   <label for="NumberOfEmployees">How many people does your company employee?: <span style="color:red">*</span></label>
		   <input type="number" class="form-control" name="NumberOfEmployees" id="NumberOfEmployees" required value="">
		  </div>
	 
	  <div class="form-group">
   	   <label for="Turnover">Annual turnover: <span style="color:red">*</span></label>
	   <select class="form-control" name="Turnover" id="Turnover" required>
      

		<option value="£0 - £49,999">£0 - £49,999</option>
	     <option value="£50,000 - £99,999">£50,000 - £99,999</option>
	     <option value="£100,000 - £499,999">£100,000 - £499,999</option>
	     <option value="£500,000 - £999,999">£500,000 - £999,999</option>
	     <option value="£1,000,000 - £2,999,999">£1,000,000 - £2,999,999</option>
	     <option value="£3,000,000 - 4,999,999">£3,000,000 - 4,999,999</option>
	     <option value="£5,000,000 - £9,999,999">£5,000,000 - £9,999,999</option>
	     <option value="£10,000,000 - £39,999,999">£10,000,000 - £39,999,999</option>
	     <option value="£40,000,000+">£40,000,000+</option>
	
	     </select>
	  </div>
			 
	<div class="form-group">
   	    <label for="Net_Profit">Net Profit Before Tax £: <span style="color:red">*</span></label>
	    <select class="form-control"  name="Net_Profit" id="Net_Profit" required>
       
	    
	     <option value="£0 - £49,999">£0 - £49,999</option>
	     <option value="£50,000 - £99,999">£50,000 - £99,999</option>
	     <option value="£100,000 - £499,999">£100,000 - £499,999</option>
	     <option value="£500,000 - £999,999">£500,000 - £999,999</option>
	     <option value="£1,000,000 - £2,999,999">£1,000,000 - £2,999,999</option>
	     <option value="£3,000,000 - 4,999,999">£3,000,000 - 4,999,999</option>
	     <option value="£5,000,000 - £9,999,999">£5,000,000 - £9,999,999</option>
	     <option value="£10,000,000 - £39,999,999">£10,000,000 - £39,999,999</option>
	     <option value="£40,000,000+">£40,000,000+</option>
	  
	     </select>
	  </div>	 
			 
			  
	
		
	   </div>	
	      
		</div>
		
		</div>
	
      </div>
	   
	  <div class="row">
		
	  
		  
	   <div class="col-md-12">
	
		 <ul>
			 <li>Is your company’s balance sheet below €43m? <span style="color:red">*</span>
		   
		     <div class="pull-right">
		       <label>
			    <input type="radio" name="Balance_Sheet"  value="1" id="Balance_Sheet_0" required="required" >
			       Yes</label>
		        <label>
			    <input type="radio" name="Balance_Sheet" value="0" id="Balance_Sheet_1"  required="required">
			       No</label>
			 </div>
		   
		   </li>
			 
			
	
	 	 
		   
	 	   
		    <li>Does another business hold 25% or more capital or voting rights in your business? <span style="color:red">*</span>
		    
		  <div class="pull-right">
		       <label>
			    <input type="radio" name="Subsidary" value="1" id="Subsidary_0" required >
			       Yes</label>
		        <label>
			    <input type="radio" name="Subsidary" value="0" id="Subsidary_1" required >
			       No</label>
			 </div>
		    
		    </li>
			 
			 	    <li>Does your business hold 25% or more capital or voting rights in another business? <span style="color:red">*</span>
		    
		  <div class="pull-right">
		       <label>
			    <input type="radio" name="Parent_Rule" value="1" id="Parent_Rule_0" required >
			       Yes</label>
		        <label>
			    <input type="radio" name="Parent_Rule" value="0" id="Parent_Rule_1" required >
			       No</label>
			 </div>
		    
		    </li>
			 
			 
			    <li>Is your business 'undertaking in difficulty'?  <a data-target="#myModal1" data-toggle="modal" class="MainNavText" id="MainNavHelp" 
       href="#myModal1">(see full definition here)</a> <span style="color:red">*</span>
					
					
		  <div class="pull-right">
		       <label>
			    <input type="radio" name="Difficulty" required value="1" id="Difficulty_0"  >
			       Yes</label>
		        <label>
			    <input type="radio" name="Difficulty" required value="0" id="Difficulty_1">
			       No</label>
		   </div>
		    
		  </li>
			 
			 
			 	    <li>Have you received any type of support from any public funds in the last<br/> three fiscal years? <a data-target="#myModal2" data-toggle="modal" class="MainNavText" id="MainNavHelp" 
       href="#myModal1">(see more information on state aid here)</a> <span style="color:red">*</span>
					
					
		 <label class="push" for="DM0">No</label>
				<input class="push" name="DM" type="radio" id="DM" required value="None"  >
		      
				<label class="push" for="DM1">Yes&nbsp;</label>
				<input class="push" type="radio" name="DM" value="State aid" required id="option-2"  >
			  
		  <fieldset class="conditional">
			
			
	   <div class="col-md-4 mt-30">
		 <p>Funding body</p> 
		<input name="X1_Funding" type="text" class="form-control" id="X1_Funding" tabindex="1" value=""> 
		<input name="X2_Funding" type="text" class="form-control" id="X2_Funding" tabindex="4" value="">
		<input name="X3_Funding" type="text" class="form-control" id="X3_Funding" tabindex="7" value="">   
			 
	   </div>
			 
	   <div class="col-md-4 mt-30">
	    <p>Purpose</p>
	 <input name="X1_Purpose" type="text" class="form-control" id="X1_Purpose" tabindex="2" value=""> 
		<input name="X2_Purpose" type="text" class="form-control" id="X2_Purpose" tabindex="5" value="">
		<input name="X3_Purpose" type="text" class="form-control" id="X3_Purpose" tabindex="8" value="">   
		 	 
	   </div>
			 
	   <div class="col-md-4 mt-30 mb-30">
	    <p>Amount</p>
         <input name="X1_Amount" type="number" class="form-control" id="X1_Amount" tabindex="3" value=""> 
		<input name="X2_Amount" type="number" class="form-control" id="X2_Amount" tabindex="6" value="">
		<input name="X3_Amount" type="number" class="form-control" id="X3_Amount" tabindex="9" value=""> 
				  </div>
				   <div class="col-md-12 ">

		  

	 
		   <input name="Total_State_Aid" type="hidden" class="form-control" 	id="Total_State_Aid" value="" readonly>
		  </div>
													
												
			</fieldset>

		   </div>
		    
		  </li>
	
		</ul>
	  
	  
	    </div>
		  
		

	 		 		 
				 
     </div>
        
      
      

	
	  <div class="row">
		   
		   
	  <div class="col-md-12">
			
		<ul>
		
		 <li>Is your business High growth? <a data-target="#myModal4" data-toggle="modal" class="MainNavText" id="MainNavHelp" 
       href="#myModal4">(see full definition here)</a> <span style="color:red">*</span>

		 	  <div class="pull-right">
		       <label>
			    <input type="radio" name="High_Growth" required value="1" id="High_Growth_0" >
			       Yes</label>
		        <label>
			    <input name="High_Growth" type="radio" required id="High_Growth_1" value="0" >
			       No</label>
			 </div>
		  
		 </li>
			 <li>Is your business Innovation Active? <a data-target="#myModal3" data-toggle="modal" class="MainNavText" id="MainNavHelp" 
       href="#myModal3">(see full definition here)</a> <span style="color:red">*</span>
		 	  <div class="pull-right">
		       <label>
			    <input type="radio" name="Innovation_Active" required value="1" id="Innovation_Active_0" >
			       Yes</label>
		        <label>
			    <input name="Innovation_Active" type="radio" required id="Innovation_Active_1" value="0"   >
			       No</label>
			 </div>
		  
		 </li>
			 <li>Do you agree to be contacted by an independent evaluator for project<br/> evaluation purposes? <span style="color:red">*</span> 	
  <div class="pull-right">
		       <label>
			    <input type="radio" name="Email_Third_Parties" required value="1" id="Email_Third_Parties_0" >
			       Yes</label>
		        <label>
			    <input name="Email_Third_Parties" type="radio" required id="Email_Third_Parties_1" value="0" >
			       No</label>
			 </div>
		  
		 </li>
		</ul>
		
	   </div>
		   
	  </div> 


	  <div class="row">
		   
	  <div class="col-md-12 mb-30">
		  <h4>General Data Protection Regulation (GDPR) notice:</h4>

		  <p>This event is planned and delivered by Newable Ltd.</p>
		  <p>The event is part funded by the European Regional Development Fund. As a result a range of data points must be collected to assess your eligibility to attend this funded event.<br/>
		  To comply with ERDF funding requirements Newable and ERDF project delivery partners collect and store your registration data for European Commission auditing and project evaluation purposes until 2033 before all records are destroyed.<br/>
			  Newable will not be share your data with any other third parties that are not involved with the delivery of the event  or funding of the project. You can see our full privacy policy here.</p>
		  
		  <p> At Newable, we offer a variety of events, products and services to help businesses start, grow and internationalise that we hope you’d like to hear about.</p> 
 

          	  <ul>
		    <li>Please tick if you would like to hear more about Newable events, products and services. You can contact us to opt out at any moment;
		
               
	         <div class="pull-right">
		       <label>
			    <input type="checkbox" name="Email_opt_in" value="1" id="Email_opt_in"  >
			       email</label>
		        <label>
			    <input type="checkbox" name="Phone_opt_in" value="1" id="Phone_opt_in" >
			       Phone</label>
				 
				  <label>
			    <input type="checkbox" name="Post_opt_in" value="1" id="Post_opt_in" >
			       Post</label>
			 </div>
		    
		    </li>
	
		</ul>
           

	  </div>
     </div>
			



<div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Definition of an Undertaking in difficulty</h4>
        </div>
        <div class="modal-body">
          <p>Quoted from the 651/2014 EU commission regulation (GBER):<br/>
“‘undertaking in difficulty’ means an undertaking in respect of which at least one of the following circumstances occurs:</p>
   <ol type="a">
	   <li>In the case of a limited liability company (other than an SME that has been in existence for less than three years or, for the purposes of eligibility for risk finance aid, an SME within 7 years from its first commercial sale that qualifies for risk finance investments following due diligence by the selected financial intermediary), where more than half of its subscribed share capital has disappeared as a result of accumulated losses. This is the case when deduction of accumulated losses from reserves (and all other elements generally considered as part of the own funds of the company) leads to a negative cumulative amount that exceeds half of the subscribed share capital. For the purposes of this provision, ‘limited liability company’ refers in particular to the types of company mentioned in Annex I of Directive 2013/34/EU (1) and ‘share capital’ includes, where relevant, any share premium.</li>

	   <li>In the case of a company where at least some members have unlimited liability for the debt of the company – (other than an SME that has been in existence for less than three years or, for the purposes of eligibility for risk finance aid, an SME within 7 years from its first commercial sale that qualifies for risk finance investments following due diligence by the selected financial intermediary), where more than half of its capital as shown in the company accounts has disappeared as a result of accumulated losses. For the purposes of this provision, ‘a company where at least some members have unlimited liability for the debt of the company’ refers in particular to the types of company mentioned in Annex II of Directive 2013/34/EU</li>
	   <li>	Where the undertaking is subject to collective insolvency proceedings or fulfils the criteria under its domestic law for being placed in collective insolvency proceedings at the request of its creditors.</li>
	   <li>Where the undertaking has received rescue aid and has not yet reimbursed the loan or terminated the guarantee, or has received restructuring aid and is still subject to a restructuring plan.</li>
	   <li>In the case of an undertaking that is not an SME, where, for the past two years:
		   <ol><li>the undertaking's book debt to equity ratio has been greater than 7,5 and</li>
			   <li>the undertaking's EBITDA interest coverage ratio has been below 1,0.”</li>
		   </ol>
	   </li>
			</ol>
</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-newable" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>


  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">State Aid Thresholds</h4>
        </div>
        <div class="modal-body">
			<ol>
				<li><p>De Minimis” State Aid Threshold</br>
De Minimis” State Aid thresholds apply to all export related ERDF projects; Global Growth, Capital Accelarate & Scale Tech Superstar (CASTS), Get Exporting 2, ASEAN and South East International Business Growth (SEIBG)
The European Commission recognises any type of support given from any public funds as State Aid and it is therefore subject to State Aid restrictions. However, we are allowed to exempt this grant under the De Minimis exemption regulation, which allows a business to receive up to a cumulative €200,000 over any rolling three-year period.
This section of the form is necessary because any grant giver (in this case DIT), which employs the De Minimis exemption regulation, is required to ensure that the €200,000 limit will not be breached when they agree an application for grant.<br/>
				Therefore, you will need to record here any public aid you have received over the past three years that may need to be included in your De Minimis threshold. You need not list any aid that has already been specifically exempted in any way other than by the De Minimis regulation or any aid that has been previously “notified” to the Commission.<br/>Please write N/A if necessary. Please add additional rows if needed.</p></li>


<li><p>European Commission’s ‘General Block Exemption Regulation State Aid Scheme’
GBER State Aid thresholds apply to Innovation related ERDF projects; Innovate 2 Succeed.
The European Commission recognises any type of support given from any public funds as State Aid and it is therefore subject to State Aid restrictions. However, this project employs the European Commission’s ‘General Block Exemption Regulation State Aid Scheme: Innovation Aid for SME’s which allows a business to receive up to a cumulative €200,000 in the last three fiscal years. We are required to ensure that the three-year €200,000 limit will not be breached with this application for support.</p></li> 
		  </ol>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-newable" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<div class="modal fade" id="myModal3" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Definition of Innovation Active Enterprise?</h4>
        </div>
        <div class="modal-body">
				<p>Your business is defined as an “Innovation Active” Enterprise if during the last 3 year period it engaged in any of the following:</p>
			<ul>
				<li>Introduction of a new or significantly improved product (good or service) or process  </li>
				<li>Engagement in innovation projects not yet complete or abandoned</li>
                  <li>New and significantly improved forms of organisation, business structures or practices and marketing concepts or strategies</li> 
		  </ul>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-newable" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

<div class="modal fade" id="myModal4" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Definition of High Growth Enterprise?</h4>
        </div>
        <div class="modal-body">
			
			<p>Your business is defined as a “High Growth” Enterprise if:</p>
			<ul>
				<li>you have at least 10 employees and </li>
                 <li>have experienced growth at an annual rate of 20% over a three year period</li> 
		  </ul>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-newable" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  

  
