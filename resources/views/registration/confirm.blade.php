@extends ('layouts.app') 

@section('title',  ' | Events') 

@section ('content')


<div class="container">
    <div class="row">

        <div class="col-xs-12">

           @if ($TknValid < 1)
       
              
             <div class="thumbnail">
		
				<h2>Thank you </h2>
									<p>Your email has been successfully confirmed,you will shortly be forwarded to our registration page<br/>If you are not forwarded within 5 seconds please click the below button to proceed.</p>

											<a class='btn btn-newable-rectangle form-control' href='/campaign/{{$eventId}}'>Proceed to registration</a>
								</div>
							</div>



                
        @else
          

 
             <div class="thumbnail">
		
				<h2>The link you have used has expired</h2>
					<p>Hello, it seems that the link you just tried to use has already been used previously, and is no longer valid.</p>
									
					<p>If you believe this to be an error please email <a class="cta" href="mailto:webmaster@newable.co.uk">webmaster@newable.co.uk</a></p>
									
									<a class='btn btn-lg btn-newable-rectangle' href='/events'>Go back</a>
									
								</div>
							</div>

        
        @endif











    </div>

</div>


    @endsection
