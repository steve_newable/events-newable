// *** Filter search type function arguments ***
// data.filter = filter input value for a column;
// data.iFilter = same as filter, except lowercase (if wo.filter_ignoreCase is true)
// data.exact = table cell text (or parsed data, if column parser enabled)
// data.iExact = same as exact, except lowercase (if wo.filter_ignoreCase is true)

// search for a match from the beginning of a string
// "^l" matches "lion" but not "koala"
$.tablesorter.filter.types.start = function( config, data ) {
    if ( /^\^/.test( data.iFilter ) ) {
      return data.iExact.indexOf( data.iFilter.substring(1) ) === 0;
    }
    return null;
  };
  
  // search for a match at the end of a string
  // "a$" matches "Llama" but not "aardvark"
  $.tablesorter.filter.types.end = function( config, data ) {
    if ( /\$$/.test( data.iFilter ) ) {
      var filter = data.iFilter,
        filterLength = filter.length - 1,
        removedSymbol = filter.substring(0, filterLength),
        exactLength = data.iExact.length;
      return data.iExact.lastIndexOf(removedSymbol) + filterLength === exactLength;
    }
    return null;
  };
  

$(function() {
    $(".table").tablesorter({
        widthFixed : true,
        widgets: ['filter'],
        widgetOptions: {
        filter_reset: '.reset'
        }
    });
});