var editor_config = {
    path_absolute : "/",
    selector: "textarea.editable",
    height : "250",
    plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
    ],
    formats: {
        alignleft: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'left'},
        aligncenter: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'center'},
        alignright: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'right'},
        alignjustify: {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'full'},
        bold: {inline : 'span', 'classes' : 'bold'},
        italic: {inline : 'span', 'classes' : 'italic'},
        underline: {inline : 'span', 'classes' : 'underline', exact : true},
        strikethrough: {inline : 'del'},
        forecolor: {inline : 'span', classes : 'forecolor', styles : {color : '%value'}},
        hilitecolor: {inline : 'span', classes : 'hilitecolor', styles : {backgroundColor : '%value'}},
        custom_format: {block : 'h1', attributes : {title : 'Header'}, styles : {color : 'red'}}
    },
    
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    image_advtab: true ,
  
	image_class_list: [
        {title: 'None', value: ''},
        {title: 'Float left', value: 'pull-left'},
        {title: 'Float right', value: 'pull-right'},
        {title: 'Centre image', value: 'center-block'},
        {title: 'Image width', value: 'imgWidth'}
      ],

    relative_urls: false,
    
    file_browser_callback : function(field_name, url, type, win) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
        
        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;

    if (type == 'image') 
    {
        cmsURL = cmsURL + "&type=Images";
    } 
    else 
    {
        cmsURL = cmsURL + "&type=Files";
    }

    tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
    });
}};

tinymce.init(editor_config);