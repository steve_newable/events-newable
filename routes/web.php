<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/authenticate', function()
{
    return Forrest::authenticate();
});

Route::get('/callback', function()
{
    Forrest::callback();

    return Redirect::to('/');
});


Route::post('/confirm', 'ConfirmationController@storeToken');
Route::get('/confirm/{token}', 'ConfirmationController@checkToken');
Route::get('/campaign/{id}', 'CampaignController@campaign');

    // Authorized routs (logged in users only)
    Route::get('/', 'EventController@index')->name('welcome-page');

 

    // Events
    Route::get('/events/date/{date}', 'EventController@displayByDate');
    Route::get('/events', 'EventController@index');
    Route::get('/events/{id}/{slug?}', 'EventController@show');

    // Comments
    Route::post('/events/{event}/{slug?}/comments', 'CommentController@store');

    // Templates and tools
    Route::get('/templates-and-tools', 'TemplateController@index');
    Route::post('/templates-and-tools/download', 'TemplateController@download');

    // Polls
    Route::get('/poll', 'PollController@index');
    Route::get('/poll/view/{poll_id}', 'PollController@show');
    Route::post('/poll', 'PollController@store');
    Route::post('/poll/view/{poll_id}', 'PollController@vote');

    //Messages
    Route::get('/messages', 'MessageController@index');
    Route::get('/messages/show/{thread_id}', 'MessageController@show')->name('messages.show');
    Route::post('/messages/show/{thread_id}', 'MessageController@reply');
    Route::delete('/messages/show/{thread_id}', 'MessageController@leaveConversation');
    Route::post('/messages/change-title/{thread_id}', 'MessageController@changeTitle');
    Route::post('/messages/add-participant/{thread_id}', 'MessageController@addParticipant');
    Route::post('/messages/new/', 'MessageController@sendMessageByWidget');
    Route::post('/messages/new/{thread_id?}', 'MessageController@store');
    Route::post('/messages/suggestion', 'MessageController@storeSuggestion');
    Route::post('/messages/shareArticle{current_thread_id?}/{recipient_id?}', 'MessageController@storeShare');
 
    //#report page
    Route::get('/reports', 'ReportController@index');

    //Get Users List
    Route::get('/api/users-list', 'MessageController@getUsersIndex');

    //Get Events List
    Route::get('api/events', 'EventController@getEventsIndex');


    
    // Search routes

    Route::get('/search/event', 'SearchController@eventSearch');

    // Search box filtering
    Route::get('/search/department/', 'SearchController@filterByDepartment');
    Route::get('/search/role/', 'SearchController@filterByRole');
    Route::get('/search/location/', 'SearchController@filterByLocation');
    Route::get('/search/all/', 'SearchController@resetFilter');

    // Tags
    Route::get('/tags', 'TagController@index');
    Route::get('/tags/{tag}', 'TagController@show');
    Route::get('/api/tag-list', 'TagController@getTagsIndex');

    // Likes
    Route::get('/likes', 'LikeController@index');
    Route::post('/like/article/{id}', 'LikeController@likeArticle');
    Route::post('/like/event/{id}', 'LikeController@likeEvent');


    // Watches
 
    Route::post('/watch/event/{id}', 'WatchController@watchEvent');
    
    // Editable
    Route::prefix('editable')->group(function () {


        Route::resource('events', 'Editable\EventController');

    
    });

  


