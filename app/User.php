<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Scout\Searchable;

use App\Message;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use Searchable;

    protected $connection = 'mysql2';

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return[
            'id' => $this->id,
            'username' => $this->username,
            'displayName' => $this->displayName,
            'email' => $this->email,
            'role' => $this->role,
            'department' => $this->department,
            'location' => $this->location,
            'directDialIn' => $this->directDialIn,
            'mobileNumber' => $this->mobileNumber,
        ];
    }

    /**
     * Get the index name for this model
     */
    public function searchableAs()
    {
        return 'users_index';
    }

    /**
     * Set route key name to tell Laravel which database field to do lookups on
     * The default is ID
     */
    public function getRouteKeyName()
    {
        return 'username';
    }

    /**
     * Table to use
     */
    protected $table = 'users';

    /**
     * By default Laravel sets id as the Primary Key for all tables.
     * You can override this by specifying the Primary Key to use.
     * Also, tell Laravel that it doesn't auto-increment
     */
    protected $primaryKey = 'username';
    public $incrementing = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'displayName', 'email',
        'role', 'department', 'location',
        'directDialIn', 'mobileNumber', 'managedByUsername',
        'managedByDisplayName'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
     * A user belongs to a team
     *
     * @return void
     */
    public function team()
    {
        return $this->belongsTo(Team::class, 'department', 'name');
    }

    /**
     * Get the profile associated with this user
     */
    public function profile()
    {
        return $this->hasOne(Profile::class, 'user_username', 'username');
    }

    /**
     * Get all of the spotlights associated with this user
     */
    public function spotlights()
    {
        return $this->hasMany(Spotlight::class, 'user_username', 'username');
    }

    /**
     * Get all of the articles associated with this user
     */
    public function articles()
    {
        return $this->hasMany(Article::class, 'author', 'displayName');
    }

    /**
     * A user may have many Widgets
     *
     * @return void
     */
    public function widgets()
    {
        return $this->hasMany(Widget::class, 'user_id', 'id')->orderBy('position', 'asc');
    }

    /**
     * A user can have many sent messages
     */
    public function message()
    {
        return $this->hasOne(Message::class, 'sender_id');
    }

    /**
     * A user's last message within a thread
     *
     */
    public function thread_last_message()
    {
        return $this->hasMany(Thread::class, 'thread_last_message_user_id');
    }

    /**
     * A user has many threads
     *
     * @return void
     */
    public function threads()
    {
        return $this->belongsToMany(Thread::class, 'thread_participants');
    }

















    /**
     * Define that a User can have many comments
     */
    public function comments()
    {
        return $this->hasMany(Comment::class, 'user_username', 'username');
    }

    /**
     * Get the liked Articles for this user
     */
    public function likedArticles()
    {
        return $this->morphedByMany(Article::class, 'likeable')->whereDeletedAt(null);
    }

    /**
     * Get the liked Articles for this user
     */
    public function likedEvents()
    {
        return $this->morphedByMany(Event::class, 'likeable')->whereDeletedAt(null);
    }

    /**
     * Get the liked files for this user
     */
    public function likedFiles()
    {
        return $this->morphedByMany(FileMetaData::class, 'likeable')->whereDeletedAt(null);
    }

    /**
     * Get the watched Articles for this user
     */
    public function watchedArticles()
    {
        return $this->morphedByMany(Article::class, 'watchable')->whereDeletedAt(null);
    }

    /**
     * Get the watched Articles for this user
     */
    public function watchedEvents()
    {
        return $this->morphedByMany(Event::class, 'watchable')->whereDeletedAt(null);
    }

    /**
     * Scope a query by department
     */
    public function scopeByDepartment($query, $department)
    {
        return $query->where('department', $department);
    }

    /**
     * Scope a query by role
     */
    public function scopeByRole($query, $role)
    {
        return $query->where('role', $role);
    }

    /**
     * Scope a query by location
     */
    public function scopeByLocation($query, $location)
    {
        return $query->where('location', $location);
    }

}
