<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Poll;

class PollOption extends Model
{
    use SoftDeletes;
    
    /**
     * Declaire the fields that should be mass assignable
     *
     * @var array
     */
    protected $fillable = ['poll_id', 'answer', 'voteAmount'];

    /**
     * Declaire the field that should be cast to a Carbon instance
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Declaire that Poll options belong to Poll votes
     *
     * @return void
     */
    public function votes()
    {
        return $this->belongsTo(PollUserVotes::class, 'option_id');
    }
}
