<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ThreadParticipants extends Model
{
    use SoftDeletes;

    protected $fillable = ['thread_id', 'user_username', 'has_read'];

    public function thread_participant_user(){
        return $this->belongsTo(User::class, 'user_username', 'username');
    }

    public function thread(){
        return $this->belongsTo(Thread::class, 'thread_id', 'id');
    }
}
