<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Like extends Model
{
    use SoftDeletes;

    protected $table = 'likeables';

    protected $fillable = [
        'user_username',
        'likeable_id',
        'likeable_type',
    ];

    /**
     * Get all of the articles that are assigned to this like
     */
    public function articles()
    {
        return $this->morphedByMany(Article::class, 'likeable');
    }

    /**
     * Get all of the events that are assigned to this like
     */
    public function events()
    {
        return $this->morphedByMany(Event::class, 'likeable');
    }

    /**
     * Establish that a file can have likes
     *
     * @return void
     */
    public function files()
    {
        return $this->morphedByMany(FileMetaData::class, 'likeable');
    }
}
