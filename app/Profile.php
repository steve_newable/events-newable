<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Profile extends Model
{
    use Searchable;

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->only('id', 'skills', 'background');

        $related = $this->user->only('department', 'role');

        return array_merge($array, $related);
    }

    /**
     * Get the index name for this model
     */
    public function searchableAs()
    {
        return 'profiles_index';
    }

    /**
     * Set route key name to tell Laravel which database field to do lookups on
     * The default is ID
     */
    public function getRouteKeyName()
    {
        return 'id';
    }

    /**
     * Table to use
     */
    protected $table = 'profiles';

    /**
     * By default Laravel sets id as the Primary Key for all tables.
     * You can override this by specifying the Primary Key to use.
     * Also, tell Laravel that it doesn't auto-increment
     */
    protected $primaryKey = 'id';
    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'background', 'skills', 'displayPicture', 'socialProfiles', 'user_username'
    ];

    /**
     * Guarded viariables are not mass assignable
     */
    protected $guarded = [];

    /**
    * Get the user that has this Profile
    */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_username', 'username');
    }

    /**
     * Scope a query to only include specific users
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUser($query, $user)
    {
        return $query->where('user_username', $user);
    }
}
