<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Laravel\Scout\Searchable;

class Event extends Model
{

    protected $connection = 'mysql2';

    
    use Searchable;

    public $asYouType = true;

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = $this->toArray();

        // Customize array...

        return $array;
    }

    /**
     * Get the index name for this model
     */
    public function searchableAs()
    {
        return 'events_index';
    }

    /**
     * Set route key name to tell Laravel which database field to do lookups on
     * The default is ID
     */
    public function getRouteKeyName()
    {
        return 'id';
    }

    /**
     * Table to use
     */
    protected $table = 'events';

    /**
     * By default Laravel sets id as the Primary Key for all tables.
     * You can override this by specifying the Primary Key to use.
     * Also, tell Laravel that it doesn't auto-increment
     */
    protected $primaryKey = 'id';
    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'location', 'startDate', 'startTime', 'finishDate', 'finishTime',
        'filepath', 'description', 'keyDetails', 'externalPageUrl', 'hostedBy', 'type',
        'category', 'sector'
    ];

    protected $dates = [
        'startDate', 'finishDate'
    ];

    /**
     * Define that an Event can have many comments
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * Get all of the tags for the Event by finding the Tags for this event.
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    /**
     * Get all of the likes for this Event
     *
     * @return void
     */
    public function likes()
    {
        return $this->morphToMany(User::class, 'likeable')->whereDeletedAt(null);
    }

    /**
     * Get all of the watches for this Event
     *
     * @return void
     */
    public function watches()
    {
        return $this->morphToMany(User::class, 'watchable')->whereDeletedAt(null);
    }

    /**
     *
     */
    public function getIsLikedAttribute()
    {
        $like = $this->likes()->whereUserUsername(Auth::user()->username)->first();
        
        return (!is_null($like)) ? true : false;
    }

    /**
     *
     */
    public function getIsWatchedAttribute()
    {
        $watch = $this->watches()->whereUserUsername(Auth::user()->username)->first();
        
        return (!is_null($watch)) ? true : false;
    }
}
