<?php

namespace App\Http\Controllers;

use App\Article;
use App\Team;

use App\Event;
use Calendar;

use Request;
use Illuminate\Support\Facades\Auth;

class PagesController extends Controller
{
    public function editable()
    {
        $teams = Team::all();
        $user = auth()->user();

        return view('editable.index', compact('teams', 'user'));
    }

      
    public function events()
    {
        return view('pages.events.index');
    }

    public function templates()
    {
        return view('pages.templates-and-tools.index');
    }

}

 


