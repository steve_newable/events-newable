<?php

namespace App\Http\Controllers;

use Validator;
use App\Poll;
use App\PollOption;
use App\PollUserVotes;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.poll.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // The selectable answers for this poll
        $answers = $request->answer;

        //Validation
        $validator = Validator::make($request->all(), [
            'question' => 'required',
            'answer' => 'required',
        ]);

        //If validation fails, return with error
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
            return redirect()->back();
        }

        //If there are less than 2 answers provided, return with error
        if(count($answers) < 2){
            return back()->with('poll-error', 'You must have at least 2 answers.')->withInput();
            return redirect()->back();
        }

        //If any of the answer fields are empty/null, return with error
        foreach ($answers as $answer){
            if($answer == null){
                return back()->with('poll-error', 'Please make sure all answer fields are filled.')->withInput();
                return redirect()->back();
            }
        }

        //Create new poll
        $poll = new Poll();

        $poll->title = $request->question;

        //Check if the user has marked the poll as anonymous
        //If anonymous, the user's ID will not be recorded in the DB
        if( !$request->has('anonymous') ) {
            $poll->user_username = auth()->user()->username;
        }

        $poll->save();

        //For each answer, add a new poll option into seperate "Poll Option" table 
        //(Answers are dynamic fields which means any number of options could be added.) 
        for($i = 0; $i < count($answers); $i++){

            $option = new PollOption();

            $option->poll_id = $poll->id;
            $option->answer = $answers[$i];

            $option->save();

        }

        //Add mandatory poll option where the user has skipped the poll
        $option = new PollOption();

        $option->poll_id = $poll->id;
        $option->answer = "Skipped";

        $option->save();

        return back()->with('poll-success', 'Poll has been created.');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Poll  $poll
     * @return \Illuminate\Http\Response
     */
    public function show($poll_id)
    {
        $user = auth()->user()->username;

        //Find out if the user has already voted on this poll
        $userHasVotedCount = PollUserVotes::where('user_username', $user)->where('poll_id', $poll_id)->get();
        $userHasVoted = false;

        //If the user has voted, set userHasVoted to true
        if(count($userHasVotedCount) > 0)
        {
            $userHasVoted = true;
        }

        $poll = Poll::find($poll_id);
        $pollOptions = PollOption::where('poll_id', $poll_id)->get();

        return view('pages.poll.show', compact('poll', 'pollOptions', 'userHasVoted'));
    }

    public function vote(Request $request, $poll_id)
    {

        $user = auth()->user()->username;

        //Only validate if the user votes
        if($request->submit == "Vote")
        {
            //Validation
            $validator = Validator::make($request->all(), [
                'options' => 'required',
            ]);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
                return redirect()->back();
            }
        }

        //Find out if the user has already voted on this poll
        $userHasVotedCount = PollUserVotes::where('user_username', $user)->where('poll_id', $poll_id)->get();

        //If the user HAS already voted on the poll, stop and redirect back. Else, continue voting.
        if(count($userHasVotedCount) > 0)
        {
            return back()->with('vote-error', 'You have already voted on this poll.');
            return redirect()->back();
        }
        else
        {
            //If the user votes
            if($request->submit == "Vote")
            {
                //Get the option which the user picked from the view
                $option_id = $request->get('options');

            }

            //If the user skips the poll
            else if($request->submit == "Skip")
            {
                //Get the ID of the skipped option
                $option_id = $request->get('skipped_option');
            }

            //Otherwise redirect back with error
            else{
                //Redirect back
                return back()->with('vote-error', 'An error occured.');
                return redirect()->back();
            }

            //Find the option and increment the vote amount
            $pollOption = PollOption::find($option_id);

            //If the user tries to vote for an answer for a different poll (e.g. the user inspected element and changed the poll_id)
            if($pollOption->poll_id != $poll_id){
                return back()->with('vote-error', 'The option picked does not match the poll you are voting on.');
                return redirect()->back();
            }

            $pollOption->voteAmount = $pollOption->voteAmount + 1;
            $pollOption->save();

            //Record the vote in the DB
            $vote = new PollUserVotes();

            $vote->user_username = $user;
            $vote->poll_id = $poll_id;

            //Check if the user has marked the poll as anonymous
            //If anonymous, the user's ID will not be recorded in the DB
            if( !$request->has('anonymous') ) {
                $vote->option_id = $option_id;
            }

            $vote->save();

            if($request->submit == "Vote")
            {
                return back()->with('vote-success', 'Thank you for voting');
            }

            else if($request->submit == "Skip"){
                return back()->with('vote-success', 'Poll Skipped.');
            }

            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Poll  $poll
     * @return \Illuminate\Http\Response
     */
    public function edit(Poll $poll)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Poll  $poll
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Poll $poll)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Poll  $poll
     * @return \Illuminate\Http\Response
     */
    public function destroy(Poll $poll)
    {
        //
    }
}
