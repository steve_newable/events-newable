<?php

namespace App\Http\Controllers;

use App\Widget;
use Illuminate\Http\Request;
use App\Article;
use App\Team;
use App\Event;
use Calendar;

use Illuminate\Support\Facades\Auth;

class WidgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::all();

        $user = auth()->user();

        $widgets = $user->widgets;

        $article_pending = Article::orderBy('id', 'desc')->where("published", "=", "pending")->take(10)->get();

        $likedArticles = $user->likedArticles()->get();

        $likedEvents = $user->likedEvents()->get();

        $likedFiles = $user->likedFiles()->get();

        return view('pages.widget-library.index', compact('teams', 'user', 'article_pending', 'widgets', 'likedFiles'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect('/widgets');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = auth()->user()->id;
       
        $widgetCount = Widget::where('user_id', '=', $id)->count();
     
        $widget = new Widget();

        $widget->user_id = $request->get('user_id');
        $widget->widget_id = $request->get('widget_id');
        $widget->position = ++$widgetCount;
        
        $widget->save();
     
        return redirect('/widget-library')->with('success', 'Widget added to homepage');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Widget  $widget
     * @return \Illuminate\Http\Response
     */
    public function show(Widget $widget)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Widget  $widget
     * @return \Illuminate\Http\Response
     */
    public function edit(Widget $widget)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Widget  $widget
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Widget $widget)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Widget  $widget
     * @return \Illuminate\Http\Response
     */
  
    public function destroy($id)
    {
        $widget = Widget::where('widget_id', $id)->first();

        $widget->delete();

        return redirect('/welcome')->with('success', 'Widget removed');
    }
}
