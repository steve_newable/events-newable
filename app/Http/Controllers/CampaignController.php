<?php

namespace App\Http\Controllers;
use App\Event;
use Spatie\CalendarLinks\Link;

use Illuminate\Http\Request;

class CampaignController extends Controller
{
    public function campaign($id)
    {
        $event = Event::find($id);

        $title = $event->title;
        $from = $event->startDate;
        $to = $event->finishDate;

        $calendarLink = Link::create($title, $from, $to)->description($event->description)->address($event->location);

        // Generate a data uri for an ics file (for iCal & Outlook)
        $outlookEvent = $calendarLink->ics();

        return view('campaigns.index' , compact ('event', 'outlookEvent'));
    }

}
