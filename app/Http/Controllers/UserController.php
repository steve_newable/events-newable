<?php

namespace App\Http\Controllers;

use App\User;
use App\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserController extends Controller
{
    /**
    * Instantiate a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware(['role:Admin|Help Desk'])->except('show');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @param Team $team
     * @return \Illuminate\Http\Response
     */
    public function show(Team $team, User $user)
    {
        $user_auth = auth()->user()->id;

        $colleagues = User::where('department', $team->name)->where('username', '!=', $user->username)->get();

        if (!$user->profile) {
            $socialProfiles = null;
        } else {
            $socialProfiles = json_decode($user->profile->socialProfiles, true);
        }

        return view('pages.people-and-teams.user', compact('user', 'team', 'colleagues', 'socialProfiles', 'user_auth'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Get all users and pass it to the view
        $users = User::all();
        $teams = Team::all();

        

        return view('users.index', compact('users', 'teams'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $users
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Role::get(); //Get all roles

        return view('users.edit', compact('user', 'roles')); //pass user and roles data to view
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User $users
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //Validate name, email and password fields
        $this->validate($request, [

        ]);

        $roles = $request['roles']; //Retreive all roles

        if (isset($roles)) {
            $user->roles()->sync($roles);  //If one or more role is selected associate user to roles
        } else {
            $user->roles()->detach(); //If no role is selected remove exisiting role associated to a user
        }
        
        return redirect()->route('users.index')->with('flash_message', 'User successfully edited.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $users
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->profile()->delete();
        $user->delete();
        
        return redirect()->route('users.index')->with('flash_message', 'User successfully deleted.');
    }
}
