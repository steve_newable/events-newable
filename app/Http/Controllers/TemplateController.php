<?php

namespace App\Http\Controllers;

use App\FileMetaData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TemplateController extends Controller
{
    public function index()
    {
        $files = FileMetaData::get();

        return view('pages.templates-and-tools.index', compact('files'));
    }
}
