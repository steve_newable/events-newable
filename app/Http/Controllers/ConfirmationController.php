<?php

namespace App\Http\Controllers;
use App\Token;
use App\Message;
use App\Mail\confirmation;
use Carbon\Carbon;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;


class ConfirmationController extends Controller
{
    public function storeToken(Request $request)
    {
        $tokTokenString = bin2hex(openssl_random_pseudo_bytes(30));

        //Check message
        $validatedData = $request->validate([
            'SLF_email' => 'required',
            'event' => 'required'
        ]);
        $recipient_email = $request->get('SLF_email');
       
        $token = new Token();
        $token->tokResetEmail = $request->get('SLF_email');
        $token->eventID = $request->get('event');
        $token->tokTokenString =  $tokTokenString;
        $token->save();

        $data = [
            'person' => $recipient_email,
            'token' => $tokTokenString,
         
        ];
       
        Mail::to($recipient_email)->send(new confirmation($data));
         
        return back()->with('success', 'You will shortly receive an email.');
        return redirect()->back();

    }

    public function checkToken(string $token)
    {

      
        $confirm = Token::find($token);
        $email = $confirm->tokResetEmail;
        $eventId = $confirm->eventID;
        $TknDate = $confirm->created_at;
     

        $now = Carbon::now('Europe/London');
        $TknDate = Carbon::parse($TknDate , 'Europe/London');
        $TknValid = $TknDate->diffInDays($now);

       if ($TknValid < 1){
       
        return view('registration.confirm', compact( 'TknValid', 'eventId'));
                }
        else{
          

            return view('registration.confirm', compact(  'TknValid', 'eventId'));

        }        

    }





}
