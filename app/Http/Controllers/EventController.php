<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;
use Calendar;
use GMaps;
use Carbon\Carbon;
use DateTime;
use Spatie\CalendarLinks\Link;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //List all events in ascending order
        $events = Event::orderBy('startDate', 'asc')->paginate(6);

        return view('pages.events.index', compact('events'));
    }


    public function getEventsIndex()
    {
        //Return all events as JSON
        $events = Event::all();

        //Return all events after today
        //$events = Event::whereDate('startDate', '>=', date('Y-m-d'))->get();
        return response()->json($events);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::find($id);

        $tags = $event->tags;

        $comments = $event->comments;

        $commentCount = count($comments);

        /**
         * Related events are events that are similar in some way (includes tags)
         */
        $relatedEvents = Event::where('startDate', '>=', Carbon::now())
        ->where('id', '!=', $event->id)
        ->where(function ($query) use ($event, $tags) {
            $query->where('hostedBy', $event->hostedBy);
            $query->orWhere('type', $event->type);
            $query->orWhere('category', $event->category);
            $query->orWhere('sector', $event->sector);
            $query->orWhereHas('tags', function ($query) use ($tags) {
                foreach ($tags as $tag) {
                    $query->where('slug', $tag->slug);
                }
            });
        })
        ->orderBy('startDate', 'dsc')
        ->get();
        
        /**
        * Load a map using GMaps but only if a location has been provided
        */
        if ($event->location != null) {
            $config['center'] = $event->location;
            $config['zoom'] = '15';

            GMaps::initialize($config);

            $marker['position'] = $event->location;

            GMaps::add_marker($marker);

            $location = GMaps::create_map();
        }

        /**
         * Create an ICS file that the user can download in order to add this event to their outlook calendar
         * Use the given event details to fill the necessary fields
         */
        $title = $event->title;
        $from = $event->startDate;
        $to = $event->finishDate;

        $calendarLink = Link::create($title, $from, $to)->description($event->description)->address($event->location);

        // Generate a data uri for an ics file (for iCal & Outlook)
        $outlookEvent = $calendarLink->ics();
        
        return view('pages.events.show', compact('event', 'comments', 'commentCount', 'location', 'relatedEvents', 'outlookEvent'));
    }

    /**
     * Display Events on a given date
     *
     * @param [type] $date
     * @return void
     */
    public function displayByDate($date)
    {
        $events = Event::whereDate('startDate', $date)->paginate(6);

        return view('pages.events.index', compact('events', 'date'));
    }
}
