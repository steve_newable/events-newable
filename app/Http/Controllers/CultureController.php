<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Culture;

class CultureController extends Controller
{
    public function index()
    {
        $cultureText = Culture::where('title', 'Culture-text')->get()->first();
        
        $AmbassadorText = Culture::where('title', 'Ambassador-text')->get()->first();
     
        return view('pages.culture.index', compact('cultureText', 'AmbassadorText'));
    }


    public function update(Request $request, $id)
    {
        $culture = Culture::find($id);

        $culture->content = $request->get('content');
   
        $culture->save();

        return redirect()->back();
    }
}
