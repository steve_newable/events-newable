<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Watch;
use App\Article;
use App\Event;
use Illuminate\Support\Facades\Auth;

class WatchController extends Controller
{
    /**
     * Display all watched content for this user
     */
    public function index()
    {
        $user = Auth::user();

        $articles = $user->watchedArticles()->get();
        $articleCount = count($articles);

        $events = $user->watchedEvents()->get();
        $eventCount = count($events);

        return view('pages.watches.index', compact('articles', 'articleCount', 'events', 'eventCount'));
    }

    /**
     * Handle the liking of an Article
     *
     * @param int $id
     * @return void
     */
    public function watchArticle($id)
    {
        // here you can check if product exists or is valid or whatever
        $this->handleWatch(Article::class, $id);
        
        return redirect()->back();
    }

    /**
     * Handle the liking of an Event
     *
     * @param int $id
     * @return void
     */
    public function watchEvent($id)
    {
        // here you can check if product exists or is valid or whatever
        $this->handleWatch(Event::class, $id);

        return redirect()->back();
    }

    /**
     * Handle a Like
     * First we check the existing Likes as well as the currently soft deleted likes.
     * If this Like doesn't exist, we create it using the given fields
     *
     *
     * @param [type] $type
     * @param [type] $id
     * @return void
     */
    public function handleWatch($type, $id)
    {
        $existingWatch = Watch::withTrashed()
        ->whereWatchableType($type)
        ->whereWatchableId($id)
        ->whereUserUsername(Auth::user()->username)
        ->first();

        if (is_null($existingWatch)) {
            // This user hasn't liked this thing so we add it
            Watch::create([
                'user_username' => Auth::user()->username,
                'watchable_id'   => $id,
                'watchable_type' => $type,
            ]);
        } else {
            // As existingLike was not null we need to effectively un-like this thing
            if (is_null($existingWatch->deleted_at)) {
                $existingWatch->delete();
            } else {
                $existingWatch->restore();
            }
        }
    }
}
