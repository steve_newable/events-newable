<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Like;
use App\Article;
use App\Event;
use Illuminate\Support\Facades\Auth;
use App\FileMetaData;

class LikeController extends Controller
{
    /**
     * Display all liked content for this user
     */
    public function index()
    {
        $user = Auth::user();

        $articles = $user->likedArticles()->orderBy('created_at', 'desc')->get();

        $events = $user->likedEvents()->orderBy('created_at', 'desc')->get();

        $files = $user->likedFiles()->orderBy('name', 'desc')->get();

        /**
         * Below are the filters in place for each model search
         * 1. News articles must be open
         * 2. Events are split into open and closed
         * 3. Seperating Help and How To and templates
         */
        $articles = $articles->filter(function ($articles) {
            return $articles->published === 'published';
        });

        $templates = $files->filter(function ($files) {
            return $files->category !== 'Help and How To';
        });

        $helpDocs = $files->filter(function ($files) {
            return $files->category === 'Help and How To';
        });

        $templateCount = count($templates);
        $helpDocCount = count($helpDocs);
        $articleCount = count($articles);
        $eventCount = count($events);


        return view('pages.likes.index', compact('templates', 'templateCount', 'helpDocs', 'helpDocCount', 'articles', 'articleCount', 'events', 'eventCount'));
    }

    /**
     * Handle the liking of an Article by passing in the class and ID of the model instance
     *
     * @param int $id
     * @return void
     */
    public function likeArticle($id)
    {
        // here you can check if product exists or is valid or whatever
        $this->handleLike(Article::class, $id);
        
        return redirect()->back();
    }

    /**
     * Handle the liking of an Event by passing in the class and ID of the model instance
     *
     * @param int $id
     * @return void
     */
    public function likeEvent($id)
    {
        // here you can check if product exists or is valid or whatever
        $this->handleLike(Event::class, $id);

        return redirect()->back();
    }

    /**
    * Handle the liking of a File by passing in the class and ID of the model instance
    *
    * @param int $id
    * @return void
    */
    public function likeFile($id)
    {
        // here you can check if product exists or is valid or whatever
        $this->handleLike(FileMetaData::class, $id);

        return redirect()->back();
    }

    /**
     * Handle a Like
     * First we check the existing Likes as well as the currently soft deleted likes.
     * If this Like doesn't exist, we create it using the given fields
     *
     *
     * @param [type] $type
     * @param [type] $id
     * @return void
     */
    public function handleLike($type, $id)
    {
        $existingLike = Like::withTrashed()->whereLikeableType($type)->whereLikeableId($id)->whereUserUsername(Auth::user()->username)->first();

        if (is_null($existingLike)) {
            // This user hasn't liked this thing so we add it
            Like::create([
                'user_username' => Auth::user()->username,
                'likeable_id'   => $id,
                'likeable_type' => $type,
            ]);
        } else {
            // As existingLike was not null we need to effectively un-like this thing
            if (is_null($existingLike->deleted_at)) {
                $existingLike->delete();
            } else {
                $existingLike->restore();
            }
        }
    }
}
