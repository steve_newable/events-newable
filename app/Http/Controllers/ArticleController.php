<?php

namespace App\Http\Controllers;

use App\Article;
use App\TwitterPost;
use App\FacebookPost;
use Twitter;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::orderBy('featuredArticle', 'DESC')->orderBy('created_at', 'DESC')->where('published', '=', 'published')->get();

        $twitterPosts = TwitterPost::all();

        $facebookPosts = FacebookPost::all();

        $allPosts = collect(); //Create empty collection which we know has the merge() method

        $allPosts = $allPosts->merge($articles);
        $allPosts = $allPosts->merge($twitterPosts);
        $allPosts = $allPosts->merge($facebookPosts);

        $allPosts = $allPosts->sortByDesc('created_at');

        return view('pages.news-and-updates.index', compact('allPosts'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('pages.news-and-updates.show')->withArticle(Article::findOrFail($id));
    }

    public function getSlugAttribute(): string
    {
        return str_slug($this->title);
    }
}
