<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Profile;
use App\Article;
use App\Event;
use Carbon\Carbon;
use App\FileMetaData;
use TeamTNT\TNTSearch\TNTSearch;

class SearchController extends Controller
{
    /**
     * Perform a search given what the user entered into the search box.
     * Uses Laravel Scout to do initial search but because the use of WHERE is limited,
     * we use a filter function instead, on each collection.
     *
     * @param Request $request
     * @return void
     */
    public function search(Request $request)
    {
        // The search string entered
        $search = $request->get('q');

        // A new instance of TNT Search
        $tnt = new TNTSearch;

        // Laravel Scout search() method
        $users = User::search($search)->orderBy('displayName', 'desc')->get();

        $articles = Article::search($search)->orderBy('created_at', 'desc')->get();

        $events = Event::search($search)->orderBy('created_at', 'desc')->get();

        $files = FileMetaData::search($search)->orderBy('name', 'desc')->get();

        // The date and time as of right now
        $today = Carbon::now();

        /**
         * Below are the filters in place for each model search
         * 1. News articles must be open
         * 2. Events are split into open and closed
         * 3. Seperating Help and How To and templates
         */
        $articles = $articles->filter(function ($articles) {
            return $articles->published === 'published';
        });

        $upcomingEvents = $events->filter(function ($events) use ($today) {
            return $events->startDate->gt($today);
        });

        $pastEvents = $events->filter(function ($events) use ($today) {
            return $events->startDate->lt($today);
        });

        $templates = $files->filter(function ($files) {
            return $files->category !== 'Help and How To';
        });

        $helpDocs = $files->filter(function ($files) {
            return $files->category === 'Help and How To';
        });

        $userCount = count($users);
        $articleCount = count($articles);
        $eventCount = count($events);
        $upcomingEventCount = count($upcomingEvents);
        $pastEventCount = count($pastEvents);
        $templateCount = count($templates);
        $helpDocCount = count($helpDocs);

        return view('pages.search.index', compact('search', 'users', 'articles', 'upcomingEvents', 'pastEvents', 'userCount', 'articleCount', 'upcomingEventCount', 'pastEventCount', 'templates', 'templateCount', 'helpDocs', 'helpDocCount'));
    }

    /**
     * Search for a user by department, role and location
     * If any are empty, they will be ignored.
     * Uses Conditional Clause: when()
     * See: https://laravel.com/docs/5.6/queries#conditional-clauses
     *
     * @param Request $request
     * @return void
     */
    public function quickSearch(Request $request)
    {
        $name = $request->get('name');
        $department = $request->get('department');
        $role = $request->get('role');
        $location = $request->get('location');

        if ($name != null) {
            $users = User::search($request->get('name'))->orderBy('displayName')->paginate(12);
        } else {
            $users = User::when($department, function ($query) use ($department) {
                return $query->ByDepartment($department);
            })
            ->when($role, function ($query) use ($role) {
                return $query->byRole($role);
            })
            ->when($location, function ($query) use ($location) {
                return $query->byLocation($location);
            })
            ->orderBy('displayName')->get();
        }

        $userCount = count($users);

        return view('pages.search.user', compact('users', 'userCount'));
    }

    /**
     * Find a colleague by searching the user model with the given search parameter
     * Return the users returned and a count of these users
     *
     * @param Request $request
     * @return void
     */
    public function colleagueSearch(Request $request)
    {
        $search = $request->get('q');

        $users = User::search($search)->orderBy('displayName')->get();

        $userCount = count($users);

        return view('pages.search.colleague', compact('search', 'users', 'userCount'));
    }

    /**
     * Find an expert by searching through useer's profiles for specific phrases or words
     * Only searches users that have Profiles
     *
     * @param Request $request
     * @return void
     */
    public function expertSearch(Request $request)
    {
        $search = $request->get('q');

        $profiles = Profile::search($search)->orderBy('user_username')->get();

        $profileCount = count($profiles);

        return view('pages.search.expert', compact('search', 'profiles', 'profileCount'));
    }

    /**
     * Search for a specific event given text entered usingh scout
     */
    public function eventSearch(Request $request)
    {
        // The search string entered
        $search = $request->get('q');

        $events = Event::search($search)->get();

        return $events;
    }

    /**
     * Search for related roles and locations
     */
    public function filterByDepartment(Request $request)
    {
        $department = $request->get('department');

        $roles = User::distinct('role')
        ->where('department', $department)
        ->orderBy('role')
        ->pluck('role');

        $locations = User::distinct('location')
        ->where('department', $department)
        ->orderBy('location')
        ->pluck('location');

        $results = array(
            'roles' => $roles,
            'locations' => $locations,
        );

        return $results;
    }

    /**
     * Search for related roles and locations
     */
    public function filterByRole(Request $request)
    {
        $role = $request->get('role');

        $departments = User::distinct('department')
        ->where('role', $role)
        ->orderBy('department')
        ->pluck('department');

        $locations = User::distinct('location')
        ->where('role', $role)
        ->orderBy('location')
        ->pluck('location');

        $results = array(
            'departments' => $departments,
            'locations' => $locations,
        );

        return $results;
    }

    /**
     * Search for related roles and locations
     */
    public function filterByLocation(Request $request)
    {
        $location = $request->get('location');

        $departments = User::distinct('department')
        ->where('location', $location)
        ->orderBy('department')
        ->pluck('department');

        $roles = User::distinct('role')
        ->where('location', $location)
        ->orderBy('role')
        ->pluck('role');

        $results = array(
            'departments' => $departments,
            'roles' => $roles,
        );

        return $results;
    }

    /**
     * Search for related roles and locations
     */
    public function resetFilter()
    {
        $departments = User::distinct('department')
        ->orderBy('department')
        ->pluck('department');

        $roles = User::distinct('role')
        ->orderBy('role')
        ->pluck('role');

        $locations = User::distinct('location')
        ->orderBy('location')
        ->pluck('location');

        $results = array(
            'departments' => $departments,
            'roles' => $roles,
            'locations' => $locations
        );

        return $results;
    }
}
