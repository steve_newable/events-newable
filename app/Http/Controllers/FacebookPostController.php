<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FacebookPost;

class FacebookPostController extends Controller
{
    public function updateFacebook()
    {
        //Access token
        $access_token = 'EAABmN3ehT2QBAPwtX8y3Dgrctwb3ZCylx7Eyq7KiXE8hYVtPSt3PrdHeJ0YsNrWQ9knh5PUIx982lHPsZCvajMMdZB5MsZAm4vI84d6PzPtF6jS9fB0DqiomB4ZCjbpMGoj17Ig7g7F3BZCDJ6LMqhkD2ZAJTmjnbk3EI7KjV8UQgZDZD';
        //Request the public posts.
        $json_str = file_get_contents('https://graph.facebook.com/v3.0/NewableGroup/feed?access_token='.$access_token);
        //Decode json string into array
        $facebookData = json_decode($json_str);

        foreach($facebookData->data as $data){

            //Convert provided date to appropriate date format
            $fbDate = date("Y-m-d H:i:s", strtotime($data->created_time));
            $fbDateToStr = strtotime($fbDate);

            //If a post contains any text
            if(isset($data->message)){

                FacebookPost::firstOrCreate(
                    ['post_id' => $data->id], ['created_at' => $fbDateToStr, 'content' => $data->message, 'featuredImage' => null]
                );

            }
            
        }

        return response()->json($facebookData->data);

    }
}
