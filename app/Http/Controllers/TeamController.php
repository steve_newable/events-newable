<?php

namespace App\Http\Controllers;

use App\User;
use App\Team;
use App\Profile;
use App\Spotlight;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get all teams from the database
        $teams = Team::orderBy('name')->get();

        // Get any spotlight elements
        $spotlights = Spotlight::inRandomOrder()->where('published', 'published')->orderBy('created_at')->take(2)->get();

        return view('pages.people-and-teams.index', compact('teams', 'spotlights'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function show(Team $team)
    {
        // Model $var is same as Model::find(wildcard)
        return view('pages.people-and-teams.team', compact('team'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        $users = User::with('profile')->where('department', $team->name)->get();

        return view('editable.teams.index', compact('team', 'users'));
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Team  $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Team $team)
    {
        $team->picto = $request->get('filepath');
        $team->summary = $request->get('summary');
        $team->skills = $request->get('skills');
        $team->tagline = $request->get('tagline');
        $team->introduction = $request->get('introduction');
  
        $team->save();

        return redirect()->back()->withSuccess('You have successfully updated the information for this team.');
    }
}
