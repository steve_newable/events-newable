<?php 

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App;
use App\Team;

class AuthController extends Controller
{

    /**
     * Check where this user is based on their IP address
     * Sends the user through to the necessary authentification method
     *
     * @return void
     */
    public function checkUserLocation(Request $request)
    {
        $userIpAddress = $request->ip();

        switch ($userIpAddress) {
            // Newable Internal IP
            case "81.171.198.91":
           
           
            if (App::environment('production')) {
                return redirect()->away('http://tholia/intranet/production.php');
            }
            if (App::environment('development')) {
                return redirect()->away('http://tholia/intranet/development.php');
            }
        
                break;
            // Localhost server
            case "127.0.0.1":

           
                return redirect()->away('http://tholia/intranet/local.php');
             
                break;
            // Other, external IP address
            default:

                
            if (App::environment('production')) {
                return redirect()->away('https://oauth.mynewable.co.uk');
            }
            if (App::environment('development')) {
                return redirect()->away('https://oauth.glesystems.co.uk');
            }
                
               
        }
    }

    /**
     * Authenticate this user via the incoming POST request.
     * This request will also tell this method how to handle incoming data.
     *
     * @return redirect
     */
    public function authenticate(Request $request)
    {
        $oAuth365 = $request->get('Oauth365');
        $tholia = $request->get('Tholia');

        // If this user got here via Office 365...
        if (isset($oAuth365) && $oAuth365 == "TRUE") {
            $email = $request->get('email');

            $confirmation = $request->get('confirmation');
            
            $user = User::where('email', $email)->get()->first();
            
            // Login this user
            Auth::login($user);

            if ($user = Auth::check()) {
                return redirect('/welcome');
            }
        }
        // If this user came through tholia...
        elseif (isset($tholia)) {

            // Create a user or update a currently existing one
            $user = User::updateOrCreate([
                'email' => $request->get('EM'),
            ], 
            [
                'username' => $request->get('UN'),
                'displayName' => $request->get('DN'),
                'role' => $request->get('JT'),
                'department' => $request->get('DP'),
                'location' => $request->get('OF'),
                'directDialIn' => $request->get('DD'),
                'mobileNumber' => $request->get('MO'),
                'managedByUsername' => $request->get('MUN'),
                'managedByDisplayName' => $request->get('MCN')
            ]);

            // Check the team which this user belongs to
            $this->checkTeam($user->department);

            // Login this user
            Auth::login($user);

            if ($user = Auth::check()) {
                return redirect('/welcome');
            }
        }
    }

    /**
     * Check whether the given department exists given this user's department
     * If it doesn't, create it in the teams table
     */
    private function checkTeam($department)
    {
        $team = Team::where('name', $department)->get()->first();

        // If no team found create one
        if ($team == null) {
            Team::create([
                'name' => $department,
                'picto' => '/assets/uploads/images/shares/Company Pictos/Newable_Pictogram_Blue_CLOUDS.png'
            ]);
        }
    }
}
