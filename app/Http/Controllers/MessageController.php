<?php

namespace App\Http\Controllers;

use App\Message;
use App\Thread;
use App\ThreadParticipants;
use App\User;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Mail\messages;
use Illuminate\Support\Facades\Mail;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //Authenticate the user
        $user = auth()->user()->username;

        $userIsParticipant = ThreadParticipants::where('user_username', $user)->pluck('thread_id');
        $userThreads = Thread::where('type', '!=', 'Task')->orderBy('updated_at', 'desc')->findMany($userIsParticipant);

        $unreadMessages = ThreadParticipants::where('user_username', $user)->where('has_read', false)->get();
        

        return view('pages.messages.index', compact('user', 'userThreads', 'unreadMessages'));
    }

    public function alertParticipants($thread_id)
    {
        $threadParticipants = ThreadParticipants::where('thread_id', $thread_id)->update(['has_read' => false]);
    }

    //Ajax request list of all users for the user's auto suggest on the message widget
    public function getUsersIndex()
    {
        $usersList = User::pluck('displayName');
        return response()->json($usersList);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.messages.show');
    }

    public function sendMessageByWidget(Request $request)
    {
        //Authenticate the user
        $user = auth()->user();

        //Check message
        $validatedData = $request->validate([
            'message_body' => 'required',
            'recipient_name' => 'required'
        ]);

        $recipients = $request->recipient_name;

        //Checks before sending the message
        foreach($recipients as $recipient){
            try
            {
                //Grab recipient's full name from textbox and try to find ID from user table
                $recipient_username = User::select('username')->where('displayName', $recipient)->firstOrFail()->username;

                //If the user is trying to send a message to themself, display error
                if($recipient_username == auth()->user()->username){
                    return back()->with('message-error', 'You cannot send a message to yourself.')->withInput();
                    return redirect()->back()->withInput();
                    break;
                }

            }
            // catch(Exception $e) catch any exception
            catch(ModelNotFoundException $e)
            {
                return back()->with('message-error', 'User ' . $recipient . ' does not exist.')->withInput();
                return redirect()->back()->withInput();
                break;
            }
        }

        foreach($recipients as $recipient)
        {

            //Grab recipient's full name from textbox and try to find ID from user table
            $recipient_username = User::select('username')->where('displayName', $recipient)->firstOrFail()->username;

            //Get the ID of threads where the user is a participant
            $userParticipant = ThreadParticipants::where('user_username', $user->username)->pluck('thread_id');

            //For each thread where the user is a participant, only retrieve "single" type threads
            $threadBetweenTwoUsers = Thread::whereIn('id', $userParticipant)->where('type', 'single')
            ->whereHas( 'participants', function($query) use ($recipient_username){ 
                $query->where('user_username', $recipient_username); // filter by other participant
            })
            ->first();

            if($threadBetweenTwoUsers != null)
            {
                //Create New Message
                $newMessage = Message::create([
                    'thread_id' => $threadBetweenTwoUsers->id,
                    'sender_username' => $user->username,
                    'message_type' => "Message",
                    'body' => $request->get('message_body')
                ]);

                //Update the current thread last message user ID
                $thread = Thread::find($threadBetweenTwoUsers->id);
                $thread->thread_last_message_username = $user->username;
                $thread->save();
                $thread->touch();
            }

            else
            {
                //If thread does not exist, create one
                $this->store($request, null, null, $recipient_username);
            }
        }

        return back()->with('message-success', 'Message has been sent.');
        return redirect()->back();

    }

    public function storeSuggestion(Request $request, $recipient_names = ['Graeme Massey', 'Jesse Orange', 'Steve Williams'])
    {
        //Authenticate the user
        $user = auth()->user();

        //Check message
        $validatedData = $request->validate([
            'suggestion-text' => 'required',
        ]);

        //For each recipient
        foreach($recipient_names as $recipient_name)
        {
            //Grab recipient's full name from textbox and try to find ID from user table
            $recipient_username = User::select('username')->where('displayName', $recipient_name)->first()->username;

            //Stop message from being sent to self
            if($recipient_username == $user->username){
                continue;
            }

            //Get the ID of threads where the user is a participant
            $userParticipant = ThreadParticipants::where('user_username', $user->username)->pluck('thread_id');

            //For each thread where the user is a participant, only retrieve "single" type threads
            $threadBetweenTwoUsers = Thread::whereIn('id', $userParticipant)->where('type', 'single')
            ->whereHas( 'participants', function($query) use ($recipient_username){ 
                $query->where('user_username', $recipient_username); // filter by other participant
            })
            ->first();

            if($threadBetweenTwoUsers != null)
            {
                //Create New Message
                $newMessage = Message::create([
                    'sender_username' => $user->username,
                    'recipient_username' => $recipient_username,
                    'body' => "Suggestion: " . $request->get('suggestion-type') . ". " . $request->get('suggestion-text'),
                    'thread_id' => $threadBetweenTwoUsers->id,
                    'message_type' => "Message"
                ]);

                //Update the current thread last message user ID
                $thread = Thread::find($threadBetweenTwoUsers->id);
                $thread->thread_last_message_username = $user->username;
                $thread->save();
                $thread->touch();

            }

            else
            {
                //If thread does not exist, create one.
                $this->store($request, null, "Suggestion: " . $request->get('suggestion-type') . ": " . $request->get('suggestion-text'), $recipient_username);
            }
        }

        return back()->with('suggestion-success', 'Suggestion has been sent.');
        return redirect()->back();
    }

    public function storeShare(Request $request, $recipient_username = null)
    {
        //Authenticate the user
        $user = auth()->user();

        //Check message
        $validatedData = $request->validate([
            'share_recipient_name' => 'required',
            'title' => 'required',
            'url' => 'required'
        ]);

        //Grab recipient's full name from textbox and try to find ID from user table
        $recipient_username = User::select('username')->where('displayName', $request->get('share_recipient_name'))->first()->username;

        //Get the ID of threads where the user is a participant
        $userParticipant = ThreadParticipants::where('user_username', $user->username)->pluck('thread_id');

        //For each thread where the user is a participant, only retrieve "single" type threads
        $threadBetweenTwoUsers = Thread::whereIn('id', $userParticipant)->where('type', 'single')
        ->whereHas( 'participants', function($query) use ($recipient_username){ 
            $query->where('user_username', $recipient_username); // filter by other participant
        })
        ->first();

        if($threadBetweenTwoUsers != null)
        {

            $message = Message::create([
                'sender_username' => $user->username,
                'recipient_username' => $recipient_username,
                'body' => 'Hi, I thought you may be interested in this ' . $request->get('type') .': <a href="'. $request->get('url') .'">' . $request->get('title') . '</a>',
                'thread_id' => $threadBetweenTwoUsers->id,
                'message_type' => "Message"
            ]);

            //Update the current thread last message user ID
            $thread = Thread::find($threadBetweenTwoUsers->id);
            $thread->thread_last_message_username = $user->username;
            $thread->save();
            $thread->touch();
        }

        else
        {
            //If thread does not exist, create one
            $this->store($request, null, 'Hi, I thought you may be interested in this ' . $request->get('type') .': <a href="'. $request->get('url') .'">' . $request->get('title') . '</a>', $recipient_username);
        }

        return back()->with('success', 'Message has been sent.');
        return redirect()->back();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $thread_id = null, $additionalMessage = null, $recipient_username = null)
    {

        //Authenticate the user
        $user = auth()->user()->username;

        //If additional message exists (was sent from suggestion box)
        if($additionalMessage == null){
            //Check message
            $validatedData = $request->validate([
                'message_body' => 'required',
            ]);
        }

        if($recipient_username == null){
            //Grab recipient's full name from textbox and try to find username from user table
            $recipient_username = User::select('username')->where('displayName', $request->get('recipient_name'))->first()->username;
        }

        //Create New Thread
        $newThread = Thread::create([
            'title' => null,
            'type' => "Single",
            'thread_starter_username' => $user,
            'thread_recipient_username' => $recipient_username,
            'thread_last_message_username' => $user
        ]);

        //If new thread has been made, set current thread ID to the new thread ID
        if($thread_id == null){
            $thread_id = $newThread->id;
        }

        //If additional message exists (was sent from suggestion box)
        if($additionalMessage == null){
            //Create New Message
            $newMessage = Message::create([
                'thread_id' => $thread_id,
                'sender_username' => $user,
                'message_type' => "Message",
                'body' => $request->get('message_body')
            ]);
        }
        else{
            //Create New Message
            $newMessage = Message::create([
                'thread_id' => $thread_id,
                'sender_username' => $user,
                'message_type' => "Message",
                'body' => $additionalMessage
            ]);
        }

        //Get Thread Participants table
        $threadParticipants = ThreadParticipants::all();

        //Check current thread to see if user is already a participant in this thread
        if($threadParticipants->where('user_username', $user)->where('thread_id', $thread_id)->first() && $threadParticipants->where('user_username', $recipient_username)->where('thread_id', $thread_id)->first()){
            //Do nothing
        }
        else{
            //Create current user as participant
            ThreadParticipants::create([
                'thread_id' => $newThread->id,
                'user_username' => $user,
                'has_read' => true
            ]);
            //Create the recipient as participant
            ThreadParticipants::create([
                'thread_id' => $newThread->id,
                'user_username' => $recipient_username,
                'has_read' => false
            ]);
        } 
          
        //If additional message exists (was sent from suggestion box)
        if($additionalMessage == null){
            return back()->with('message-success', 'Message has been sent.');
            return redirect()->back();
        }

        //Alert participants (mark as unread message)
        if($additionalMessage == null){
            $this->alertParticipants($thread_id);
        }

    }

    public function reply(Request $request, $thread_id = null)
    {

        //Authenticate the user
        $user = auth()->user()->username;

        //Check message
        $validatedData = $request->validate([
            'message_body' => 'required',
        ]);

        try {
            //Check if the user is the participant of this thread. If not then fail
            $threadParticipant = ThreadParticipants::where('thread_id', $thread_id)->where('user_username', $user)->firstOrFail();
        }
        // catch(Exception $e) catch any exception
        catch(ModelNotFoundException $e){
            return back()->with('error', 'Message failed to send.');
            return redirect()->back();
        }

        //Create New Message
        $newMessage = Message::create([
            'thread_id' => $thread_id,
            'sender_username' => $user,
            'message_type' => "Message",
            'body' => $request->get('message_body')
        ]);

        //Update thread time
        $thread = Thread::find($thread_id);
        //Update last message user ID
        $thread->thread_last_message_username = $user;
        $thread->save();
        $thread->touch();

        //Alert participants (mark as unread message)
        $this->alertParticipants($thread_id);
          
        return back()->with('success', 'Message has been sent.');
        return redirect()->back();

    }

    public function addParticipant(Request $request, $thread_id)
    {
        //Authenticate the user
        $user = auth()->user()->username;

        //Check user field is filled
        $validatedData = $request->validate([
            'new_participant_name' => 'required',
        ]);
        
        //Grab new participants's full name from textbox and try to find ID from user table
        $new_participant_username = User::select('username')->where('displayName', $request->get('new_participant_name'))->first();

        //If the entered participant does not exist, redirect back.
        if($new_participant_username == null){
            return back()->with('error', $request->get('new_participant_name') . ' does not exist. Please check spelling.');
            return redirect()->back();
        }

        //Check to see if the entered participant is already a participant in this thread.
        $isAlreadyParticipant = ThreadParticipants::where('thread_id', $thread_id)->where('user_username', $new_participant_username->username)->first();

        //If the entered participant is already a participant in this thread, redirect back.
        if($isAlreadyParticipant != null){
            return back()->with('error', $request->get('new_participant_name') . ' is already a participant.');
            return redirect()->back();
        }

        //Create a new group thread if the current thread is single (1to1).
        $thread = Thread::find($thread_id);
        if($thread->type == "Single")
        {
            //Create a new thread
            $newThread = Thread::create([
                'title' => 'Group by ' . auth()->user()->displayName,
                'type' => 'Group',
                'thread_starter_username' => $user,
                'thread_last_message_username' => $user
            ]);
            //Create a new message alerting of a new participant
            $newMessage = Message::create([
                'thread_id' => $newThread->id,
                'sender_username' => $user,
                'message_type' => "Message",
                'body' => $request->get('new_participant_name') . " has been added to the conversation."
            ]);

            //Get the old thread participants
            $oldThreadParticipants = ThreadParticipants::where('thread_id', $thread_id)->get();

            //For each old thread participant, add them to the newly created thread.
            foreach ($oldThreadParticipants as $oldThreadParticipant){
                ThreadParticipants::create([
                    'thread_id' => $newThread->id,
                    'user_username' => $oldThreadParticipant->user_username,
                    'has_read' => false
                ]);
            }
            //Add the original intended to be added participant to the new group thread
            ThreadParticipants::create([
                'thread_id' => $newThread->id,
                'user_username' => $new_participant_username->username,
                'has_read' => false
            ]);
            //Redirect to the new thread
            return redirect()->action(
                'MessageController@show', ['thread_id' => $newThread->id]
            )->with('success', 'New thread created. ' . $request->get('new_participant_name') . ' has been added successfully.');
        }

        //If thread is already a group thread, then just add a new participant
        else
        {
            //Add the new participant
            ThreadParticipants::create([
                'thread_id' => $thread_id,
                'user_username' => $new_participant_username->username,
                'has_read' => false
            ]);

            //Create New Message
            $newMessage = Message::create([
                'thread_id' => $thread_id,
                'sender_username' => $user,
                'message_type' => "Message",
                'body' => $request->get('new_participant_name') . " has been added to the " . ($thread->type == "Task" ? "task." : "conversation.")
            ]);

            //Update the user ID of the last message in the thread
            $thread->thread_last_message_username = $user;
            $thread->save();
            $thread->touch();

            //If the thread type is a task, also send an email that they have been assigned the task.
            if($thread->type == "Task"){
                //Get the recipient's email address
                $recipient_email = User::select('email')->where('displayName', $request->get('new_participant_name'))->first()->email;
    
                $data = [
                    'person' => $request->get('task-recipient'),
                    'title' => $thread->tasks->title,
                    'message' => $thread->tasks->description,
                    'sender' => auth()->user()->displayName,
                    'id' => $thread->id,
                ];
            
                Mail::to($recipient_email)->send(new messages($data));
            }

            return back()->with('message-success', $request->get('new_participant_name') . ' has been added successfully.');
            return redirect()->back();
        }

        //Alert participants (mark as unread message)
        $this->alertParticipants($thread_id);

    }

    public function changeTitle(Request $request, $thread_id)
    {
        //Authenticate the user
        $user = auth()->user()->username;

        //Check user field is filled
        $validatedData = $request->validate([
            'new_group_chat_title' => 'required',
        ]);

        //Find thread
        $thread = Thread::find($thread_id);

        //Get old thread title
        $oldThreadTitle = $thread->title;

        //Change thread title
        $thread->title = $request->get('new_group_chat_title');
        $thread->save();
        $thread->touch();

        //Create New Message
        $newMessage = Message::create([
            'thread_id' => $thread_id,
            'sender_username' => $user,
            'message_type' => "Message",
            'body' => "Conversation title changed from " . $oldThreadTitle . " to " . $request->get('new_group_chat_title') . "."
        ]);

        //Alert participants (mark as unread message)
        $this->alertParticipants($thread_id);

        return back()->with('message-success', 'Title has been changed to ' . $request->get('new_group_chat_title'));
        return redirect()->back();

    }

    public function leaveConversation($thread_id)
    {
        //Authenticate the user
        $user = auth()->user()->username;

        //Create New Message
        $newMessage = Message::create([
            'thread_id' => $thread_id,
            'sender_username' => $user,
            'message_type' => "Message",
            'body' => auth()->user()->displayName . " has left the group."
        ]);

        //Get the user in the thread
        $userInThread = ThreadParticipants::where('user_username', $user)->where('thread_id', $thread_id)->first();
        $userInThread->delete();

        if(Thread::find($thread_id)->type == "Task")
        {
            return redirect()->action('TaskController@index')->with('task-success', 'You were removed from the task.');
        }
        else{
            return redirect()->action('MessageController@index')->with('message-success', 'You were removed from the conversation.');
        }
    }

    

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($thread_id)
    {

        //Get the user ID
        $user = auth()->user()->username;

        $threadParticipants = ThreadParticipants::where('thread_id', $thread_id)->get();

        //Check if the user is the participant of this thread
        $threadParticipant = ThreadParticipants::where('thread_id', $thread_id)->where('user_username', $user)->firstOrFail();

        //If the user is not a participant, throw 404 error, otherwise continue...

        //Mark that the user has read the latest message/s in this thread
        $threadParticipant->has_read = true;
        $threadParticipant->save();

        //Grab all messages by the thread_id and order in descending order
        $messages = Message::where('thread_id', $thread_id)->orderBy('created_at', 'asc')->get();

        //Retrieve thread. Makes sure it is a message thread. Used to display the name of the sender & recipient in view
        $thread = Thread::where('id', $thread_id)->where('type', '!=', 'Task')->firstOrFail();

        return view('pages.messages.show', compact('user', 'thread', 'messages', 'threadParticipants'));
    }
}
