<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Event;
use Auth;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Show all of the comments for an Event
     *
     * @param Event $event
     * @return void
     */
    public function index(Event $event)
    {
        return $event->comments()->with('user')->latest()->get();
    }

    /**
     * Store a new comment
     */
    public function store(Request $request, Event $event)
    {
        $this->validate(request(), [
            'comment' => 'required|min:2'
        ]);

        if ($request->has('rating')) {
            $rating = $request->get('rating');
        } else {
            $rating = null;
        }

        Comment::create([
            'user_username' => Auth::user()->username,
            'event_id' => $event->id,
            'parent_id' => null,
            'body' => $request->get('comment'),
            'rating' => $rating,
        ]);

        return back();
    }
}
