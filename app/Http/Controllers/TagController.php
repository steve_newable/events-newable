<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Article;
use App\Event;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TagController extends Controller
{
    /**
     * Display a listing of all tags as well as tags currently in use
     * Order tags in use by the sum count of their usage by counting relations
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::orderBy('name')->get();

        $tagCount = count($tags);

        $usedTags = Tag::has('articles')->orHas('events')->withCount('articles', 'events')->orderByRaw('articles_count + events_count DESC')->orderBy('name')->get();

        $usedTagsCount = count($usedTags);

        return view('pages.tags.index', compact('tags', 'usedTags', 'tagCount', 'usedTagsCount'));
    }

    /**
     * Display everything related to a given tag by looking for Models that have given tags through the taggables table
     * Uses whereHas to 
     */
    public function show(Tag $tag)
    {
        $tags = Tag::get();

        $relatedArticles = Article::whereHas('tags', function ($query) use ($tag) {
            $query->where('slug', $tag->slug);
        })
        ->orderBy('created_at')->get();

        // A count of articles
        $articleCount = count($relatedArticles);

        $relatedEvents = Event::whereHas('tags', function ($query) use ($tag) {
            $query->where('slug', $tag->slug);
        })
        ->orderBy('created_at')->get();

        // Seperate events into old and upcoming
        $today = Carbon::now();

        // Events that are upcoming
        $upcomingEvents = $relatedEvents->filter(function ($relatedEvents) use ($today) {
            return $relatedEvents->startDate->gt($today);
        });

        // Past events
        $pastEvents = $relatedEvents->filter(function ($relatedEvents) use ($today) {
            return $relatedEvents->startDate->lt($today);
        });

        // A count of events
        $upcomingEventCount = count($upcomingEvents);

        $pastEventCount = count($pastEvents);

        $usedTags = Tag::has('articles')->orHas('events')->withCount('articles', 'events')->orderByRaw('articles_count + events_count DESC')->orderBy('name')->get();

        $usedTagsCount = count($usedTags);

        return view('pages.tags.show', compact('tags', 'tag', 'relatedArticles', 'upcomingEvents', 'pastEvents', 'articleCount', 'upcomingEventCount', 'pastEventCount', 'usedTags', 'usedTagsCount'));
    }
}
