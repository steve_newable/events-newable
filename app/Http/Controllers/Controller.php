<?php

namespace App\Http\Controllers;


use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Support\Facades\Auth;

use App\Event;
use Calendar;
use View;

use Validator;
use App\Poll;
use App\PollOption;
use App\PollUserVotes;
use App\User;

use App\Report;

use Twitter;
use LinkedIn;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function calendar(){
        $all_events = Event::whereDate('startDate', '>=', date('y-m-d'))->orderBy('startDate', 'asc')->get(); // Used for the event slider to display all events on any page.
        $calendar_events = "No events";

        //Get all events for the calendar
        $calendar_events = Event::get();
        $event_list = [];

        //For each event, input each event into FullCalendar
        foreach ($calendar_events as $key => $event) {
            $event_list[] = Calendar::event(
                $event->title, // Add Event Title
                true,
                new \DateTime($event->startDate), //Add Event Start Date
                new \DateTime($event->finishDate), //Add Event End Date
                $event->id,
                [
                    'url' => '/events/date/'.$event->startDate->format('y-m-d'), //Set Event URL to link to page listing events only on that specific day
                ]
            );
        }

        //Calendar events
        $calendar_events = Calendar::addEvents($event_list)
        ->setOptions([
            'header' =>
                [
                    'left' => 'prev,today',
                    'center' => 'title',
                    'right' => 'next,today',
                ]
        ])
        ->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
            'eventRender' => 'function(event, element, view) {
                $(\'.fc-day-top[data-date="\' + getEventDate(event) + \'"]\').addClass("event-day"); 
                $(\'.fc-day-top[data-date="\' + getEventDate(event) + \'"] .fc-day-number\').wrap(\'<a href="\' + getEventUrl(event) + \'" /> \' );
        }']);

        // ^^ For each event, get the day where an event is prevent and add the "event-day" class + url to the day of the event.

        // Sharing is caring
        View::share('calendar_events', $calendar_events);
        View::share('all_events', $all_events);
    }

    function displayPollResults($user)
    {

        $allPolls = Poll::select('id')->get();

        $pollsToDisplay = [];
        $pollOptionsToDisplay = [];

        foreach ($allPolls as $poll_id)
        {
            //Find out if the user has already voted on this poll
            $userHasVotedCount = PollUserVotes::where('user_username', $user)->where('poll_id', $poll_id->id)->get();

            //If the user has voted, set userHasVoted to true
            if(count($userHasVotedCount) > 0)
            {
                array_push($pollsToDisplay, Poll::where('id', $poll_id->id)->get()->toArray());
                array_push($pollOptionsToDisplay, PollOption::where('poll_id', $poll_id->id)->get()->toArray());
            }
        }


        View::share('polls', $pollsToDisplay);
        View::share('pollsOptions', $pollOptionsToDisplay);
    }

    function displayLatestReports()
    {
        $reports = Report::orderBy('created_at', 'desc')->take(3)->get();
        View::share('latestReports', $reports);
    }

    function twitter()
    {
        $twitterData = Twitter::getUserTimeline(['count' => 20, 'tweet_mode'=>'extended', 'format' => 'array']);
        View::share('twitterData', $twitterData);
    }

    function facebook()
    {

    }

    public function __construct()
    {

        //If the page is widget library, welcome page or cultures, display poll results
        if (\Route::current()->getName() == 'widget-library' || \Route::current()->getName() == 'welcome-page' || \Route::current()->getName() == 'culture') {

        //Get the user ID
        $this->middleware(function ($request, $next) {
            $this->user_username = Auth::user()->username;

            //Display poll results
            self::displayPollResults($this->user_username);
            return $next($request);
        });

        
        }

        //If the page is widget library, welcome page or cultures, display poll results
        if (\Route::current()->getName() == 'widget-library' || \Route::current()->getName() == 'welcome-page'){
            self::twitter();
            self::facebook();
            self::displayLatestReports();
        }

        self::calendar();

    }

}
