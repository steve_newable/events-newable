<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use App\Mail\messages;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Task;
use App\Thread;
use App\ThreadParticipants;
use Carbon;

class TaskController extends Controller
{

    public function index()
    {

        //Authenticate the user
        $user = auth()->user()->username;

        $userIsParticipant = ThreadParticipants::where('user_username', $user)
        ->pluck('thread_id');

        $userTaskThreads = Thread::where('type', 'Task')->orderBy('updated_at', 'desc')
        ->findMany($userIsParticipant)
        ->where('type', 'Task');

        $allTaskThreads = Thread::where('type' ,'Task')->pluck('id')->toArray();

        $unreadTaskThreads = ThreadParticipants::whereIn('thread_id', $allTaskThreads)->where('user_username', $user)
        ->where('has_read', false)
        ->get();

        $publicTasks = Task::where('public', true)->where('status', '!=', 'Closed')->get();

        $currentTaskThreads = null;

        return view('pages.tasks.index', compact('user', 'userTaskThreads', 'currentTaskThreads', 'unreadTaskThreads', 'publicTasks'));

    }

    public function store(Request $request, $recipient_name = null)
    {

        //Authenticate the user
        $user = auth()->user()->username;

        //Check message
        $validatedData = $request->validate([
            'task_recipient' => 'required',
            'task_title' => 'required',
            'task_text' => 'required'
        ]);

        //List of all recipient names
        $recipients = $request->task_recipient;

        //Create array to store recipient usernames
        $recipientsUsernames = [];

        foreach($recipients as $recipient){
            //Grab recipient's full name from textbox and try to find ID from user table
            $recipient_user = User::select('username')->where('displayName', $recipient)->first()->username;
            //Add recipient username to array based on entered display name
            array_push($recipientsUsernames, $recipient_user);
        }

        //Create New Thread
        $newThread = Thread::create([
            'title' => null,
            'type' => "Task",
            'thread_starter_username' => $user,
            'thread_recipient_username' => $recipientsUsernames[0],
            'thread_last_message_username' => $user
        ]);

        //Assign new thread id to thread_id variable
        $thread_id = $newThread->id;

        $newTask = Task::create([
            'thread_id' => $thread_id,
            'title' => $request->get('task_title'),
            'description' => $request->get('task_text'),
            'status' => 'Open'
        ]);

        //Create New Message
        $newMessage = Message::create([
            'thread_id' => $thread_id,
            'sender_username' => $user,
            'message_type' => "Message",
            'body' => $request->get('task_text')
        ]);

        //Get Thread Participants table
        $threadParticipants = ThreadParticipants::all();

        //Check current thread to see if user is already a participant in this thread
        if($threadParticipants->where('user_username', $user)->where('thread_id', $thread_id)->first() && $threadParticipants->where('user_username', $recipient_user)->where('thread_id', $thread_id)->first()){
            //Do nothing
        }
        else{
            //Create current user as participant
            ThreadParticipants::create([
                'thread_id' => $thread_id,
                'user_username' => $user,
                'has_read' => true
            ]);

            foreach($recipientsUsernames as $recipientUsername){
                //If the recipient username does not match the current logged in user username (prevent duplicate user in task)
                if($recipientUsername != auth()->user()->username){
                    //Create the recipient(s) as participant(s)
                    ThreadParticipants::create([
                        'thread_id' => $thread_id,
                        'user_username' => $recipientUsername,
                        'has_read' => false
                    ]);
                }
            }
            foreach($recipients as $recipient){
                //Get the recipient's email address
                $recipient_email = User::select('email')->where('displayName', $recipient)->first()->email;
                //Send task email to recipient
                $this->sendTaskEmail($request, $recipient, $recipient_email, $thread_id);
            }
        } 

        return redirect()->action('TaskController@show', [$thread_id])->with('success', 'Task has been assigned.');
    }


    //Send task recipient an email alerting them that they have been added to a task. 
    function sendTaskEmail(Request $request, $recipient_name, $recipient_email, $thread_id)
    {
        $data = [
            'person' => $recipient_name,
            'title' => $request->get('task_title'),
            'message' => $request->get('task_text'),
            'sender' => auth()->user()->displayName,
            'id' => $thread_id,
        ];
       
        Mail::to($recipient_email)->send(new messages($data));
    }

    public function updateTaskDetails(Request $request, $thread_id)
    {
        //Authenticate user id
        $user = auth()->user()->username;

        //Check user field is filled
        $validatedData = $request->validate([
            'new_task_title' => 'required',
            'new-task-description' => 'required',
        ]);

        //Find task from thread id
        $task = Task::where('thread_id', $thread_id)->first();

        //Change task description
        $task->title = $request->get('new_task_title');
        $task->description = $request->get('new-task-description');
        $task->action = $request->get('new-task-action');
        $task->resolution = $request->get('new-task-resolution');
        $task->save();

        //Create New Message
        $newMessage = Message::create([
            'thread_id' => $thread_id,
            'sender_username' => $user,
            'message_type' => "Message",
            'body' => "Task description has been updated."
        ]);

        return back()->with('success', 'Task description has been updated.');
        return redirect()->back();
    }

    public function setDueDate(Request $request, $thread_id)
    {
        //Authenticate user id
        $user = auth()->user()->username;

        $currentTask = Task::where('thread_id', $thread_id)->get()->first();
        $currentTask->due_date = $request->get('task_due_date');
        $currentTask->save();

        //Create new message to informing task participants that it has been rescheduled
        $message = Message::create([
            'body' => "Task due date has been scheduled to: " . date("d/m/Y", strtotime($request->get('task_due_date'))),
            'thread_id' => $thread_id,
            'message_type' => "Task",
            'sender_username' => $user
        ]);

        return back()->with('success', 'Task scheduled to: ' . date("d/m/Y", strtotime($request->get('task_due_date'))));
        return redirect()->back();
                    
    }

    public function changeStatus(Request $request, $thread_id)
    {
        //Authenticate user id
        $user = auth()->user()->username;

        $currentTask = Task::where('thread_id', $thread_id)->get()->first();
        $currentTask->status = $request->get('task-status');

        //If the task is being closed, assign the closed date
        if($request->get('task-status') == "Closed"){
            $currentTask->closed_date = Carbon\Carbon::now();
        }

        $currentTask->save();

        //Create new message to informing task participants that the status has been updated
        $message = Message::create([
            'body' => "Setting task status to: " . $request->get('task-status'),
            'thread_id' => $thread_id,
            'message_type' => "Task",
            'sender_username' => $user
        ]);
            
        return back()->with('success', 'Task status updated.');
        return redirect()->back();
    }

    public function changeVisibility(Request $request, $thread_id)
    {
        //Authenticate user id
        $user = auth()->user()->username;

        $currentTask = Task::where('thread_id', $thread_id)->get()->first();
        

        if($request->get('task-visibility') == "Public"){
            $currentTask->public = true;
        }
        else{
            $currentTask->public = false;
        }

        $currentTask->save();

        //Create new message to informing task participants that the status has been updated
        $message = Message::create([
            'body' => "Setting task visibility to: " . $request->get('task-visibility'),
            'thread_id' => $thread_id,
            'message_type' => "Task",
            'sender_username' => $user
        ]);
            
        return back()->with('success', 'Task visibility updated.');
        return redirect()->back();
    }

    public function show($thread_id)
    {
        //Get the user ID
        $user = auth()->user()->username;

        $threadParticipants = ThreadParticipants::where('thread_id', $thread_id)->get();

        //Check if the user is the participant of this thread
        $threadParticipant = ThreadParticipants::where('thread_id', $thread_id)->where('user_username', $user)->firstOrFail();

        //If the user is not a participant, throw 404 error, otherwise continue...

        //Mark that the user has read the latest message/s in this thread
        $threadParticipant->has_read = true;
        $threadParticipant->save();

        //Grab all messages by the thread_id and order in descending order
        $messages = Message::where('thread_id', $thread_id)->orderBy('created_at', 'asc')->get();

        //Retrieve thread. Used to display the name of the sender & recipient in view. Throw an error if the thread is not a task
        $thread = Thread::where('id', $thread_id)->where('type', 'Task')->firstOrFail();

        //Retrieve task information and also throw error if the thread is not a task (eg. trying to access messages thread from task view)
        $task_information = Task::where('thread_id', $thread_id)->firstOrFail();

        return view('pages.tasks.show', compact('user', 'messages', 'thread', 'task_information', 'threadParticipants'));
    }

    public function deleteTask($thread_id)
    {
        //Get the user ID
        $user = auth()->user()->username;

        //Check if the user is the participant of this thread
        $threadParticipant = ThreadParticipants::where('thread_id', $thread_id)->where('user_username', $user)->first();

        //If the user is a participant of the thread, destroy it. 
        if(!empty($threadParticipant)){
            Thread::destroy($thread_id);
            ThreadParticipants::where('thread_id', $thread_id)->delete();
            Task::where('thread_id', $thread_id)->delete();
            return redirect()->action('TaskController@index')->with('success', 'Task has been deleted.');
        }
        else{
            return redirect()->action('TaskController@index')->with('error', 'You are not a participant of this task.');
        }
    }

}
