<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twitter;
use App\TwitterPost;

class TwitterPostController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */

    public function updateTwitter($amount = 400)
    {
        $twitterData = Twitter::getUserTimeline(['count' => $amount, 'tweet_mode'=>'extended', 'format' => 'array']);

        foreach($twitterData as $data){

            //dd($data);

            $tweetDate = date("Y-m-d H:i:s", strtotime($data['created_at']));
            $tweetDateToStr = strtotime($tweetDate);
            $tweetImg = null;

            if(!empty($data['extended_entities']['media']))
            {
                foreach($data['extended_entities']['media'] as $v){
                    $tweetImg = $v['media_url'];
                }
            }
            
            TwitterPost::firstOrCreate(
                ['post_id' => $data['id']], ['created_at' => $tweetDateToStr, 'content' => $data['full_text'], 'featuredImage' => $tweetImg]
            );
        }

        return response()->json($twitterData);
    }
}