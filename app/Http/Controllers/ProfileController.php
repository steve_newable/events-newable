<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UpdateProfile;

class ProfileController extends Controller
{
    /**
     * Display the currently logged in users profile as well as the form to edit the profile.
     * This acts as both a view and an edit
     * If the profile doesn't exist, create it and assign values
     */
    public function index(User $user)
    {
        if ($user) {
            // Get this user's profile
            $profile = $user->profile;

            // If there is no profile for this user, create one
            if (!$profile) {
                $profile = new Profile();

                // Define the social profiles array (this can be appended to)
                $socialProfiles = [
                    'LinkedIn' => null,
                    'Twitter' => null
                ];

                $profile->mangedByUsername = $user->managedByUsername;
                $profile->displayPicture = '/assets/img/staff_photos/default.jpg';
                $profile->socialProfiles = json_encode($socialProfiles);

                $user->profile()->save($profile);
            }

            $socialProfiles = json_decode($profile->socialProfiles, true);

            if ((Auth::user()->hasAnyRole('Admin|Team Manager')) || (Auth::user()->username == $user->username)) {
                return view('editable.people.profile', compact('user', 'socialProfiles'));
            } else {
                abort(403, 'Unauthorized action.');
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfile $request, User $user)
    {
        if ($user) {
            // Only proceed if there is a logged in user
            $profile = $user->profile;

            // If there is no profile, create one for this user as they'll need one.
            if (!empty(request()->get('background'))) {
                $profile->background = $request->get('background');
            }

            if (!empty(request()->get('skills'))) {
                $profile->skills = $request->get('skills');
            }

            if (!empty(request()->get('filepath'))) {
                $profile->displayPicture = $request->get('filepath');
            }

            if (!empty(request()->get('linkedInUrl'))) {
                $socialProfilesDecoded = json_decode($user->profile->socialProfiles, true);
                $socialProfilesDecoded["LinkedIn"] = $request->get('linkedInUrl');
                $profile->socialProfiles = json_encode($socialProfilesDecoded);
            }

            if (!empty(request()->get('twitterUrl'))) {
                $socialProfilesDecoded = json_decode($user->profile->socialProfiles, true);
                $socialProfilesDecoded["Twitter"] = $request->get('twitterUrl');
                $profile->socialProfiles = json_encode($socialProfilesDecoded);
            }

            $user->profile()->save($profile);

            return redirect()->back()->withSuccess('Your profile has been successfully updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(String $user)
    {
        $profile = Profile::where('user_username', '=', $user);

        $profile->delete();

        return redirect('/welcome')->withSuccess('Your profile has been cleared');
    }
}
