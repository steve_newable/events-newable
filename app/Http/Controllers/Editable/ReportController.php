<?php

namespace App\Http\Controllers\Editable;

use App\Http\Controllers\Controller;

use App\Report;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ReportController extends Controller
{
    /**
    * Instantiate a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware(['role:Report Manager']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = Report::get();

        return view('editable.reports.index', compact('reports'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|min:2',
            'pdf-file' => 'required|mimes:pdf|max:30000'
        ]);

        $title = $request->get('title');
        $image_path = $request->get('image_filepath');
        $pdf = $request->file('pdf-file');

        $extension = strtolower($pdf->getClientOriginalExtension());
        
        $path = $pdf->storeAs('reports', $pdf->getClientOriginalName());

        if (Storage::exists($path)) {
            // If the file was successfully uploaded, save the new model instance

            $report = new Report();

            $report->title = $title;
            $report->image_path = $image_path;
            $report->pdf_path = $path;

            $report->save();

            return redirect('editable/reports')->with('success', $title . ' has been added to the Reports section');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Report  $Report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Report  $Report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $Report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        $validatedData = $request->validate([
            'title' => 'required|min:2',
            'pdf-file' => 'mimes:pdf|max:30000'
        ]);

        $title = $request->get('title');
        $image_path = $request->get('image_filepath');

        $report->title = $title;
        $report->image_path = $image_path;

        if ($request->hasFile('pdf-file')) {
            $pdf = $request->file('pdf-file');

            $extension = strtolower($pdf->getClientOriginalExtension());
            $path = $pdf->storeAs('reports', $pdf->getClientOriginalName());

            $report->pdf_path = $path;
        } else {
            $report->pdf_path = $report->pdf_path;
        }

        $report->save();

        return redirect('editable/reports')->with('success', 'Report has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Report  $Report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        Report::destroy($report->id);
        return redirect('editable/reports')->with('success', 'Report has been deleted');
    }
}
