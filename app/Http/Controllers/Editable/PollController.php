<?php

namespace App\Http\Controllers\Editable;

use App\Http\Controllers\Controller;

use Validator;
use App\Poll;
use App\PollOption;
use App\PollUserVotes;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PollController extends Controller
{
    /**
    * Instantiate a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware(['role:Content Manager']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $polls = Poll::withTrashed()->get();
        $pollsOptions = PollOption::withTrashed()->get();

        return view('editable.poll.index', compact('polls', 'pollsOptions'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Poll  $poll
     * @return \Illuminate\Http\Response
     */
    public function show($poll_id)
    {
        $user = auth()->user()->username;

        //Find out if the user has already voted on this poll
        $userHasVotedCount = PollUserVotes::where('user_username', $user)->where('poll_id', $poll_id)->get();
        $userHasVoted = false;

        //If the user has voted, set userHasVoted to true
        if(count($userHasVotedCount) > 0)
        {
            $userHasVoted = true;
        }

        $poll = Poll::find($poll_id);
        $pollOptions = PollOption::where('poll_id', $poll_id)->get();

        return view('editable.poll.show', compact('poll', 'pollOptions', 'userHasVoted'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Poll  $poll
     * @return \Illuminate\Http\Response
     */
    public function edit(Poll $poll)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Poll  $poll
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Poll $poll)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Poll  $poll
     * @return \Illuminate\Http\Response
     */
    public function destroy(Poll $poll)
    {
        $pollOptions = PollOption::where('poll_id', $poll->id);
        $poll->delete();
        $pollOptions->delete();

        return redirect('editable/polls')->with('success', 'Poll has been deleted');
    }

    public function restore(Poll $poll)
    {
        dd($poll->id);
        $poll = Poll::withTrashed()->find($poll);
        $pollOptions = PollOption::withTrashed()->where('poll_id', $poll->id);

        dd($poll);
        $poll->restore();
        $pollOptions->restore();

        return redirect('editable/polls')->with('success', 'Poll has been restored');
    }

}
