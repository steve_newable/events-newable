<?php

namespace App\Http\Controllers\Editable;

use Validator;
use App\Article;
use App\Tag;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    /**
    * Instantiate a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware(['role:News Contributor|News Approver']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all()->sortByDesc("created_at");
        $categories = Article::distinct('category')->pluck('category');
        $user = auth::user();

        return view('editable.news.index', compact('articles', 'categories', 'user'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $article = Article::find($id);

        return view('pages.news-and-updates.article', compact('article'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth::user();

        return view('editable.news.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'excerpt' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('editable/news-and-updates')
                    ->withErrors($validator)
                    ->withInput();
        }

        $article = new Article();

        $isFeatured = false;

        //Check if article is marked as featured
        if ($request->get('featuredArticle') == "true") {
            $isFeatured = true;
        }

        $article->title = $request->get('title');
        $article->author = $request->get('author');
        $article->category = $request->get('category');
        $article->excerpt = $request->get('excerpt');
        $article->content = $request->get('content');
        $article->featuredImage = $request->get('featuredImage');
        $article->featuredVideo = $request->get('featuredVideo');
        $article->readingTime = $request->get('readingTime');
        $article->published = "pending";
        $article->featuredArticle = $isFeatured;

        $article->save();

        // Get the ID of the article that was just inserted
        $id = $article->id;

        // Handle tags
        $this->handleTags($request, $id);
            
        return redirect('editable/news-and-updates')->with('success', 'Article has been added');
    }

    /**
     * Show the form to edit this resource
     */
    public function edit($id)
    {
        $user = auth::user();
        $article = Article::find($id);

        // Get the tags associated with this article and convert to a comma seperated string
        if ($article->has('tags')) {
            $tags = $article->tags->pluck('name')->toArray();
            $tags = implode(", ", $tags);
        } else {
            $tags = "";
        }
       
        return view('editable.news.edit', compact('article', 'user', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'title' => 'required',
            'excerpt' => 'required',
        ]);

        $article = Article::find($id);

        //Check if article is marked as featured
        if ($request->get('featuredArticle') == "true") {
            $isFeatured = true;
        } else {
            $isFeatured = false;
        }

        $article->title = $request->get('title');
        $article->author = $request->get('author');
        $article->category = $request->get('category');
        $article->excerpt = $request->get('excerpt');
        $article->content = $request->get('content');
        $article->featuredImage = $request->get('featuredImage');
        $article->featuredVideo = $request->get('featuredVideo');
        $article->readingTime = $request->get('readingTime');
        $article->published = $request->get('published');
        $article->featuredArticle = $isFeatured;

        $article->save();

        // Handle tags
        $this->handleTags($request, $id);
        
        return back()->with('success', 'Article edited successfully.');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $articles = Article::find($id);

        $articles->delete();

        return redirect('editable/news-and-updates')->with('success', 'Article has been deleted');
    }

    /**
    * Handle tagging of this resource
    *
    * First, get the tags field and make sure it isn't empty
    * Convert the string into an array and loop through the array
    * Create new tags if they don't exist
    */
    public function handleTags(Request $request, $id)
    {
        $article = Article::find($id);

        if ($request->has('tags')) {
            $tags = $request->get('tags');

            if (!empty($tags)) {
                // Turn a String into an array E.g. one, two
                $tagArray = array_filter(explode(", ", $tags));

                // Loop through the tag array that we just created
                foreach ($tagArray as $tag) {
                    Tag::firstOrCreate(
                        [
                            'name' => ucfirst(trim($tag)),
                            'slug' => str_slug($tag)
                        ]
                    );
                }

                // Grab the IDs for the tags in the array
                $tags = Tag::whereIn('name', $tagArray)->get()->pluck('id');
                $article->tags()->sync($tags);
            } else {
                // If there were no tags, remove them from this model instance
                $article->tags()->sync(array());
            }
        }
    }
}
