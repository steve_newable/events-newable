<?php

namespace App\Http\Controllers\Editable;

use App\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagController extends Controller
{
    /**
    * Instantiate a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware(['role:Admin']);
    }

    /**
     * Display a listing of all the available Tags from the tags table
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::orderBy('name')->get();

        return view('editable.tags.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('editable.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|unique',
        ]);

        if ($request->has('tags')) {
            $tags = $request->get('tags');
    
            if (!empty($tags)) {
                // Turn a String into an array E.g. one, two
                $tagArray = array_filter(explode(", ", $tags));
    
                // Loop through the tag array that we just created
                foreach ($tagArray as $tag) {
                    Tag::firstOrCreate(
                        [
                            'name' => ucfirst(trim($tag)),
                            'slug' => str_slug($tag)
                        ]
                    );
                }
            }
        }
        
        return redirect('editable/tags')->with('success', 'Tag(s) added successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        return view('editable.tags.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        $tag->name = $request->get('name');
        $tag->slug = str_slug($request->get('name'));

        $tag->save();

        return redirect('editable/tags')->with('success', 'Tag updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        $tag->delete();

        return redirect('editable/tags')->with('success', 'Tag: ' . $tag->name . ' has been deleted');
    }
}
