<?php

namespace App\Http\Controllers\Editable;

use Validator;
use App\Event;
use App\Tag;
use GMaps;
use Calendar;
use Carbon\Carbon;
use DateTime;
use Spatie\CalendarLinks\Link;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    /**
    * Instantiate a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware(['role:Events Contributor|Events Approver']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::orderBy('startDate', 'asc')->get();

        return view('editable.events.index', compact('events'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::find($id);

        return view('pages.events.show', compact('event'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth::user();

        return view('editable.events.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'location' => 'required',
            'startDate' => 'required|date|after_or_equal:today',
            'startTime' => 'required',
            'finishDate' => 'required|date',
            'finishTime' => 'required',  
        ]);

        if ($validator->fails()) {
            return redirect('editable/events')->withErrors($validator)->withInput();
        }

        $event = new Event();

        $event->title = $request->get('title');
        $event->location = $request->get('location');
        $event->startDate = $request->get('startDate');
        $event->startTime = $request->get('startTime');
        $event->finishDate = $request->get('finishDate');
        $event->finishTime = $request->get('finishTime');
        $event->image = $request->get('filepath');
        $event->description = $request->get('description');
        $event->details = $request->get('details');
        $event->externalPageUrl = $request->get('externalPageUrl');
        $event->hostedBy = $request->get('hostedBy');
        $event->type = $request->get('type');
        $event->category = $request->get('category');
        $event->sector = $request->get('sector');
        $event->published = "pending";

        //If no featured image set, automatically create featured image placeholder
        if ($request->get('filepath') == null) {
            $event->image = "http://via.placeholder.com/350x150";
        }

        $event->save();

        // Get the ID of the article that was just inserted
        $id = $event->id;

        // Handle tags
        $this->handleTags($request, $id);

        return redirect('editable/events')->with('success', 'Event has been added');
    }

    /**
     * Show the form to edit this resource
     */
    public function edit($id)
    {
        $user = auth::user();
        $event = Event::find($id);

        /**
        * Load a map using GMaps but only if a location has been provided
        */
        if ($event->location != null) {
            $config['center'] = $event->location;
            $config['zoom'] = '15';

            GMaps::initialize($config);

            $marker['position'] = $event->location;

            GMaps::add_marker($marker);

            $location = GMaps::create_map();
        }
       
        return view('editable.events.edit', compact('event', 'user', 'location'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'title' => 'required',
            'location' => 'required',
            'startDate' => 'required|date|after_or_equal:today',
            'startTime' => 'required',
            'finishDate' => 'required|date|after_or_equal:startDate',
            'finishTime' => 'required',
            
        ]);

        $event = Event::find($id);

        $event->title = $request->get('title');
        $event->location = $request->get('location');
        $event->startDate = $request->get('startDate');
        $event->startTime = $request->get('startTime');
        $event->finishDate = $request->get('finishDate');
        $event->finishTime = $request->get('finishTime');
        $event->image = $request->get('filepath');
        $event->description = $request->get('description');
        $event->details = $request->get('details');
        $event->externalPageUrl = $request->get('externalPageUrl');
        $event->type = $request->get('type');
        $event->category = $request->get('category');
        $event->hostedBy = $request->get('hostedBy');
        $event->sector = $request->get('sector');
        $event->published = $request->get('published');

        $event->save();

        // Get the ID of the article that was just inserted
        $id = $event->id;

        // Handle tags
        $this->handleTags($request, $id);

        return back()->with('success', 'Event edited successfully.');;
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);

        $event->delete();

        return redirect('editable/events')->with('success', 'Event has been deleted');
    }

    /**
    * Handle tagging of this resource
    *
    * First, get the tags field and make sure it isn't empty
    * Convert the string into an array and loop through the array
    * Create new tags if they don't exist
    */
    public function handleTags(Request $request, $id)
    {
        $event = Event::find($id);

        if ($request->has('tags')) {
            $tags = $request->get('tags');

            if (!empty($tags)) {
                // Turn a String into an array E.g. one, two
                $tagArray = array_filter(explode(", ", $tags));

                // Loop through the tag array that we just created
                foreach ($tagArray as $tag) {
                    Tag::firstOrCreate(
                        [
                            'name' => ucfirst(trim($tag)),
                            'slug' => str_slug($tag)
                        ]
                    );
                }

                // Grab the IDs for the tags in the array
                $tags = Tag::whereIn('name', $tagArray)->get()->pluck('id');
                $event->tags()->sync($tags);
            } else {
                // If there were no tags, remove them from this model instance
                $event->tags()->sync(array());
            }
        }
    }
}
