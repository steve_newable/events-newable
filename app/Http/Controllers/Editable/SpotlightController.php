<?php

namespace App\Http\Controllers\Editable;

use App\Spotlight;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;

class SpotlightController extends Controller
{
    /**
    * Instantiate a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware(['role:Spotlight Manager']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $spotlights = Spotlight::orderBy('created_at', 'asc')->get();

        return view('editable.spotlight.index', compact('spotlights'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $displayName = auth()->user()->displayName;

        return view('editable.spotlight.create', compact('displayName'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user' => 'required',
            'body' => 'required',
            'category' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $spotlight = new Spotlight();

        $user = $request->get('user');

        $username = User::where('displayName', $user)->pluck('username')->first();

        if ($request->has('filepath')) {
            $spotlight->featuredImage = $request->get('filepath');
        }

        $spotlight->body = $request->get('body');

        if ($request->has('quote')) {
            $spotlight->quote = $request->get('quote');
        }

        $spotlight->category = $request->get('category');

        $spotlight->user_username = $username;

        $spotlight->save();

        return redirect('editable/spotlight');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Spotlight  $spotlight
     * @return \Illuminate\Http\Response
     */
    public function show(Spotlight $spotlight)
    {
        if ($spotlight->category == 'sixty-second-spotlight') {
            $spotlight->category = 'Sixty second spotlight';
        } else {
            $spotlight->category = 'Newbies at Newable';
        }

        return view('editable.spotlight.show', compact('spotlight'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Spotlight  $spotlight
     * @return \Illuminate\Http\Response
     */
    public function edit(Spotlight $spotlight)
    {
        // We need to get the User's display name
        $user = $spotlight->user->displayName;

        return view('editable.spotlight.edit', compact('spotlight', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Spotlight  $spotlight
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Spotlight $spotlight)
    {
        $validator = Validator::make($request->all(), [
            'user' => 'required',
            'body' => 'required',
            'category' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $user = $request->get('user');

        $username = User::where('displayName', $user)->pluck('username')->first();

        if ($request->has('filepath')) {
            $spotlight->featuredImage = $request->get('filepath');
        }

        $spotlight->body = $request->get('body');

        if ($request->has('quote')) {
            $spotlight->quote = $request->get('quote');
        }

        $spotlight->category = $request->get('category');
        $spotlight->published = $request->get('published');
        $spotlight->user_username = $username;

        $spotlight->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Spotlight  $spotlight
     * @return \Illuminate\Http\Response
     */
    public function destroy(Spotlight $spotlight)
    {
        $spotlight->destroy();
    }
}
