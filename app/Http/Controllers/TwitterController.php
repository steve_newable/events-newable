<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Twitter;
use File;


class TwitterController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function twitter()
    {
        $data = Twitter::getUserTimeline(['count' => 20, 'format' => 'array']);

    	return view('widgets.twitter',compact('data'));
    }
}

