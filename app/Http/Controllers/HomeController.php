<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Article;
use App\Team;
use App\Widget;

use Validator;
use App\Poll;
use App\PollOption;
use App\PollUserVotes;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();

        $teams = Team::all();

        $article = Article::orderBy('created_at', 'desc')->where("published", "=", "published")->where('featuredArticle', '=', true)->take(3)->get();

        $article_pending = Article::orderBy('id', 'desc')->where("published", "=", "pending")->take(10)->get();

        $likedArticles = $user->likedArticles()->get();

        $likedEvents = $user->likedEvents()->get();

        $likedFiles = $user->likedFiles()->get();


        /**
         * Poll display
        */
        $allPolls = Poll::select('id')->get();

        $userHasVoted = false;

        foreach ($allPolls as $poll_id) {
            $userHasVotedCount = PollUserVotes::where('user_username', $user->username)->where('poll_id', $poll_id->id)->get();
            
            //If the user has voted, set userHasVoted to true
            if (count($userHasVotedCount) == 1) {
            } else {
                $selected_poll_id = $poll_id->id;
                break;
            }
        }

        if (isset($selected_poll_id)) {
            $pollsRemaining = true;
            $poll = Poll::find($selected_poll_id);
            $pollOptions = PollOption::where('poll_id', $selected_poll_id)->get();
        } else {
            $poll = null;
            $pollOptions = null;
            $pollsRemaining = false;
        }
       
        return view('pages.index', compact('user', 'article', 'article_pending', 'teams', 'poll', 'pollOptions', 'userHasVoted', 'pollsRemaining', 'likedFiles'));
    }
}
