<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'background' => array(
                'sometimes',
                'required',
                'string',
                'min:10'
            ),
            'skills' => array(
                'sometimes',
                'required',
                'string',
                'min:10'
            ),
            'linkedInUrl' => array(
                'nullable',
                'url',
            ),
            'twitterUrl' => array(
                'nullable',
                'url',
            ),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'background.required' => 'Please enter something in the background section.',
            'background.min' => 'Please tell us a little bit more about your background.',
            'skills.required' => 'Please write something in the skills section.',
            'skills.min' => 'Please tell us a little bit more about your skills.',
            'linkedInUrl.url' => 'The URL you entered for your LinkedIn profile isn\'t a valid URL.',
            'twitterUrl.url' => 'The URL you entered for your Twitter profile isn\'t a valid URL.',
            'filepath.required' => 'Please select an image to use as your profile picture'
        ];
    }
}
