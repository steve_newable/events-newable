<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

    /**
     * Table to use
     */
    protected $table = 'tags';

    /**
     * Explicitly set the Primary Key
     */
    protected $primaryKey = 'id';
    public $incrementing = true;

    /**
     * Set route key name to tell Laravel which database field to do lookups on
     * The default is ID
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'slug'
    ];

    /**
     * Guarded viariables are not mass assignable
     */
    protected $guarded = ['created_at', 'updated_at'];

    /**
     * Get all of the posts that are assigned to a tag.
     */
    public function articles()
    {
        return $this->morphedByMany(Article::class, 'taggable');
    }

    /**
     * Get all of the posts that are assigned to a tag.
     */
    public function events()
    {
        return $this->morphedByMany(Event::class, 'taggable');
    }

    public function getTagCountAttribute()
    {
        return $this->has('articles')->orHas('events')->withCount('articles', 'events')->orderByRaw('articles_count + events_count DESC')->pluck('articles_count');
    }
}
