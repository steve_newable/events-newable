<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Message extends Model
{
    protected $fillable = ['sender_username', 'thread_id', 'message_type', 'body', 'title', 'url'];

    public function sender(){
        return $this->belongsTo(User::class, 'sender_username', 'username');
    }

    public function recipient(){
        return $this->belongsTo(User::class, 'recipient_username', 'username');
    }

    public function user(){
        return $this->belongsTo(User::class, 'sender_username', 'username');
    }

    public function tasks(){
        return $this->hasMany(Task::class, 'thread_id', 'thread_id');
    }

    public function messages(){
        return $this->belongsTo(Thread::class, 'thread_id', 'id');
    }
    
}
