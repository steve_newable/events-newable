<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // View composer for user departments, roles and locations
        view()->composer(['layouts.quick-search',
            'layouts.quick-search-full-width',
            'widgets.quick-search-full-width',
            'widgets.quick-search',
            'editable.templates-and-tools.create',
            'editable.templates-and-tools.edit',
            'editable.templates-and-tools.bulk'], function ($view) {
                $view->with('departments', \App\User::distinct('department')
                    ->orderBy('department')
                    ->pluck('department'));
            
                $view->with('roles', \App\User::distinct('role')
                    ->orderBy('role')
                    ->pluck('role'));

                $view->with('locations', \App\User::distinct('location')
                    ->orderBy('location')
                    ->pluck('location'));
            });

        // Tag view composer
        view()->composer(['layouts.tags-area'], function ($view) {
            $view->with('tags', \App\Tag::has('articles')
            ->orHas('events')
            ->orderBy('name')
            ->pluck('name'));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
