<?php

use App\Message;
use App\Thread;
use App\ThreadParticipants;
use App\User;
use App\Task;

if (!function_exists('getFirstName')) {

    /**
     * This function uses array_slice() to retrieve the first name of a user.
     * Works by looking for the first space and grabbing the string behind it.
     * 
     * Usage: getFirstName('John Smith)
     *
     * @param string $name
     * @return string $name
     */
    function getFirstName($name) {
        return implode(' ', array_slice(explode(' ', $name), 0, -1));
    }
}

//Get Unread Messages Count
if(!function_exists('getUnreadMessagesCount')){

    function getUnreadMessagesCount(){

        //Authenticate user id
        $user = auth()->user()->username;

        $messageThreads = Thread::where('type', '!=', 'Task')->pluck('id')->toArray();

        //Grab list of unread messages
        $unreadMessages = ThreadParticipants::where('user_username', $user)
        ->where('has_read', false)->whereIn('thread_id', $messageThreads)
        ->get(); 

        return count($unreadMessages);
    }

}

//Get Incomplete Tasks Count
if(!function_exists('getOutstandingTasksCount')){

    function getOutstandingTasksCount(){

        //Authenticate user id
        $user = auth()->user()->username;

        //Grab list of threads where the user is in a task
        $userinTask = ThreadParticipants::where('user_username', $user)
        ->pluck('thread_id')->toArray(); 

        //Get list of threads where the user is in a task
        $taskThreads = Thread::findMany($userinTask)->where('type', 'Task')->pluck('id')->toArray();

        //Check which tasks are not completed
        $tasks = Task::whereIn('thread_id', $taskThreads)->where('status', '!=', 'Closed')->get();

        //Get count of incomplete tasks
        $incompleteTasks = count($tasks);

        return $incompleteTasks;

    }

}