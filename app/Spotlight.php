<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Spotlight extends Model
{
    /**
    * Get the user that's associated with this spotlight
    */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_username', 'username');
    }
}
