<?php
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

use App\Article;
use App\TwitterPost;
use App\FacebookPost;
use Twitter;

class UpdateSocial extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:social';
 
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates facebook and twitter feeds to DB';
 
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function updateFacebook()
    {

        //Access token
        $access_token = 'EAABmN3ehT2QBAPwtX8y3Dgrctwb3ZCylx7Eyq7KiXE8hYVtPSt3PrdHeJ0YsNrWQ9knh5PUIx982lHPsZCvajMMdZB5MsZAm4vI84d6PzPtF6jS9fB0DqiomB4ZCjbpMGoj17Ig7g7F3BZCDJ6LMqhkD2ZAJTmjnbk3EI7KjV8UQgZDZD';
        //Request the public posts.
        $json_str = file_get_contents('https://graph.facebook.com/v3.0/NewableGroup/feed?access_token='.$access_token);
        //Decode json string into array
        $facebookData = json_decode($json_str);

        //For each facebook post
        foreach ($facebookData->data as $data) {

            //Convert provided date to appropriate date format
            $fbDate = date("Y-m-d H:i:s", strtotime($data->created_time));
            $fbDateToStr = strtotime($fbDate);

            //If a post contains any text
            if (isset($data->message)) {

                //Create new facebook post if it does not already exist in the DB
                $facebookPost = FacebookPost::firstOrCreate(
                    [
                        'post_id' => $data->id
                    ],
                    [
                        'created_at' => $fbDateToStr,
                        'content' => $data->message,
                        'featuredImage' => null
                    ]
                );

                //Output any new facebook posts to the console.
                if ($facebookPost->wasRecentlyCreated) {
                    $this->info("New Facebook Post Added --- " . $facebookPost->content);
                }
            }
        }
    }

    public function updateTwitter($amount)
    {
        $twitterData = Twitter::getUserTimeline(['count' => $amount, 'tweet_mode'=>'extended', 'format' => 'array']);

        foreach ($twitterData as $data) {

            //Convert provided date to appropriate date format
            $tweetDate = date("Y-m-d H:i:s", strtotime($data['created_at']));
            $tweetDateToStr = strtotime($tweetDate);
            $tweetImg = null;

            //Get the twitter image if any
            if (!empty($data['extended_entities']['media'])) {
                foreach ($data['extended_entities']['media'] as $v) {
                    $tweetImg = $v['media_url_https'];
                }
            }
            
            //Create new tweet if it does not already exist in the DB
            $twitterPost = TwitterPost::firstOrCreate(
                ['post_id' => $data['id']],
                [
                    'created_at' => $tweetDateToStr,
                    'content' => $data['full_text'],
                    'featuredImage' => $tweetImg
                ]
            );

            //Output any new twitter posts to the console.
            if ($twitterPost->wasRecentlyCreated) {
                $this->info("New Tweet Added --- " . $twitterPost->content);
            }
        }
    }
 
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->updateFacebook();
        $this->updateTwitter(20);
    }
}
