<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use SoftDeletes;

    protected $fillable = ['thread_id', 'title', 'description', 'status', 'message_type', 'action', 'resolution'];
    protected $dates = ['due_date', 'closed_date'];

    //Messages related to task
    public function message(){
        return $this->belongsTo(TaskMessage::class, 'thread_id', 'thread_id');
    }

    //Display the name of the user which the task was forwarded to
    public function forwarded_user(){
        return $this->belongsTo(User::class, 'forwarded_to', 'username');
    }

    //Thread ID related to task
    public function threads(){
        return $this->belongsTo(Thread::class, 'thread_id', 'id');
    }
}
