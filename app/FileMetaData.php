<?php

namespace App;

use Laravel\Scout\Searchable;

use Illuminate\Database\Eloquent\Model;

class FileMetaData extends Model
{
    use Searchable;

    public function toSearchableArray()
    {
        return[
            'id' => $this->id,
            'name' => $this->name,
            'department' => $this->department,
            'category' => $this->category,
            'type' => $this->type,
            'extension' => $this->extension,
        ];
    }

    /**
     * Get the index name for the model.
     *
     * @return string
     */
    public function searchableAs()
    {
        return 'files_index';
    }

    /**
     * Define the table to be used
     *
     */
    protected $table = 'file_meta_data';

    /**
     * Define mass assignable attributes
     */
    protected $fillable = [
        'name', 'department', 'category', 'type', 'extension', 'size', 'filepath', 'created_at', 'updated_at'
    ];

    /**
     * Get all of the likes for this File
     *
     * @return void
     */
    public function likes()
    {
        return $this->morphToMany(User::class, 'likeable')->whereDeletedAt(null);
    }

    /**
    * Create an attribute - isLiked - for this model
    * Gets the likes where the user username is that of the currently logged in user
    */
    public function getIsLikedAttribute()
    {
        $like = $this->likes()->whereUserUsername(auth()->user()->username)->first();
        
        return (!is_null($like)) ? true : false;
    }

    /**
     * Get the user's full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return "{$this->name} {$this->extension}";
    }

    /**
     * Determine what icon to use when displaying files by using the stored file extension,
     * Checking whether the extension is within a given array of extensions
     * This is not an expansive list but should do for our purposes
     *
     * @return void
     */
    public function getIconAttribute()
    {
        $extensions = [
            // Images
            'jpg' => '<i class="far fa-file-image"></i>',
            'jpeg' => '<i class="far fa-file-image"></i>',
            'png' => '<i class="far fa-file-image"></i>',
            'eps' => '<i class="far fa-file-image"></i>',
            'ai' => '<i class="far fa-file-image"></i>',

            // Text files
            'txt' => '<i class="far fa-file-alt"></i>',
            'odt' => '<i class="far fa-file-alt"></i>',
            'doc' => '<i class="far fa-file-word"></i>',
            'docx' => '<i class="far fa-file-word"></i>',
            'dotx' => '<i class="far fa-file-word"></i>',
            'pdf' => '<i class="far fa-file-pdf"></i>',

            // Powerpoint
            'ppt' => '<i class="far fa-file-powerpoint"></i>',
            'pptx' => '<i class="far fa-file-powerpoint"></i>',

            // Spreadsheet files
            'xls' => '<i class="far fa-file-excel"></i>',
            'xlsx' => '<i class="far fa-file-excel"></i>',
            'xlsm' => '<i class="far fa-file-excel"></i>',

            // Audio files
            'mp3' => '<i class="far fa-file-audio"></i>', 
            'ogg' => '<i class="far fa-file-audio"></i>', 
            'mpga' => '<i class="far fa-file-audio"></i>',

            // Video files
            'mp4' => '<i class="far fa-file-video"></i>', 
            'mpeg' => '<i class="far fa-file-video"></i>'

        ];

        return array_get($extensions, $this->extension);
    }

    /**
     * Define an attribute to give a more human readable name to file extensions
     *
     * @return void
     */
    public function getFileTypeAttribute()
    {
        $extensions = [
            // Images
            'jpg' => 'JPEG Image',
            'jpeg' => 'JPEG Image',
            'png' => 'PNG Image',
            'gif' => 'GIF Image',
            'ai' => 'Adobe Illustrator Image',

            // Text files
            'txt' => 'Text File',
            'odt' => 'Open Document File',
            'doc' => 'Microsoft Word',
            'docx' => 'Microsoft Word',
            'dotx' => 'Microsoft Word Template',
            'pdf' => 'Adobe Acrobat',

            // Powerpoint
            'ppt' => 'Microsoft PowerPoint',
            'pptx' => 'Microsoft PowerPoint',

            // Spreadsheet files
            'xls' => 'Microsoft Excel',
            'xlsx' => 'Microsoft Excel',
            'xlsm' => 'Microsoft Excel (Macro enabled)',
        ];

        return array_get($extensions, $this->extension);
    }

    /**
     * Get the file size in kilobytes as a new attribute
     * E.g. $file->sizeKb
     *
     * @return int size in kiobytes
     */
    public function getSizeKbAttribute()
    {
        return round($this->size/1000, 0).'kb';
    }
}
