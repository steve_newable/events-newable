<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * Define the fillable attributes
     *
     * @var array
     */
    protected $fillable = [
        'user_username', 'event_id', 'parent_id', 'body', 'rating'
    ];

    /**
     * Define that a comment may have many replies
     */
    public function replies()
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }

    /**
     * Define that a comment belongs to an Event
     *
     * @return void
     */
    public function event()
    {
        return $this->belongsTo(Event::class);
    }


    /**
     * Define that a comment belongs to a user
     *
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_username', 'username');
    }
}
