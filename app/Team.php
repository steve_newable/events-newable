<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    /**
     * Set route key name to tell Laravel which database field to do lookups on
     * The default is ID
     */
    public function getRouteKeyName()
    {
        return 'name';
    }

    /**
     * Table to use
     */
    protected $table = 'teams';

    /**
     * By default Laravel sets id as the Primary Key for all tables.
     * You can override this by specifying the Primary Key to use.
     * Also, tell Laravel that it doesn't auto-increment
     */
    protected $primaryKey = 'name';
    public $incrementing = false;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'picto',
    ];


    /**
     * Eager load the relationship between the Team and User Models
     */
    protected $with = ['users'];

    /**
     * Get the profile associated with this user
     */
    public function users()
    {
        return $this->hasMany(User::class, 'department');
    }
}
