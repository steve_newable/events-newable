<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Watch extends Model
{
    use SoftDeletes;

    protected $table = 'watchables';

    protected $fillable = [
        'user_username',
        'watchable_id',
        'watchable_type',
    ];

    /**
     * Get all of the articles that are assigned to this watch
     */
    public function articles()
    {
        return $this->morphedByMany(Article::class, 'watchable');
    }

    /**
     * Get all of the events that are assigned to this watch
     */
    public function events()
    {
        return $this->morphedByMany(Event::class, 'watchable');
    }
}
