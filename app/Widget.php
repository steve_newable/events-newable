<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Widget extends Model
{
    /**
    * Get the user that has this Widget
    */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * Check whether a given user has this Widget
     */
    public function getIsSelectedAttribute()
    {
        $selected = $this->user()->whereId(auth()->user()->id)->first();
        
        return (!is_null($selected)) ? true : false;
    }
}
