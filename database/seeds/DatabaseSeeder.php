<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            TeamsTableSeeder::class,
            UsersTableSeeder::class,
            ProfilesTableSeeder::class
        ]);
        
        factory('App\Article', 20)->create();


    }
}
