<?php

use Illuminate\Database\Seeder;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * Adds the given departments from LDAP into the teams table.
     * These departments/teams may change.
     * To check those you can use the below query on the Intranet database:
     * SELECT DISTINCT department FROM users;
     * As the user table is filled directly from AD via LDAP
     * 
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert([
            'user_username' => 'jesseo',
            'skills' => 'Skillz',
            'background' => 'Background',
            'socialProfiles' => 'Facebook, Twitter',
            'displayPicture' => 'link_to_picture',
            'mangedByUsername' => 'stevew',
            'changesPending' => '0'
        ]);
    }
}
