<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * Adds the given departments from LDAP into the teams table.
     * These departments/teams may change.
     * To check those you can use the below query on the Intranet database:
     * SELECT DISTINCT department FROM users;
     * As the user table is filled directly from AD via LDAP
     * 
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'jesseo',
            'displayName' => 'Jesse Orange',
            'email' => 'Jesse.Orange@newable.co.uk',
            'role' => 'Web Developer, Digital',
            'department' => 'Digital',
            'location' => 'Head Office',
            'directDialIn' => '+44 (0)20 7940 1599',
            'mobileNumber' => 'NULL',
            'managedByUsername' => 'stevew',
            'managedByDisplayName' => 'Steve Williams',
        ]);
    }
}
