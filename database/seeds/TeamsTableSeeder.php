<?php

use Illuminate\Database\Seeder;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * Adds the given departments from LDAP into the teams table.
     * These departments/teams may change.
     * To check those you can use the below query on the Intranet database:
     * SELECT DISTINCT department FROM users;
     * As the user table is filled directly from AD via LDAP
     * 
     *
     * @return void
     */
    public function run()
    {
        DB::table('teams')->insert([
            'name' => 'Digital',
            'picto' => '/assets/uploads/images/shares/Company Pictos/Newable_Pictogram_Blue_COCKTAIL.png'
        ]);

        DB::table('teams')->insert([
            'name' => 'Properties',
            'picto' => '/assets/uploads/images/shares/Company Pictos/Newable_Pictogram_Blue_COCKTAIL.png'
        ]);

        DB::table('teams')->insert([
            'name' => 'Trade London',
            'picto' => '/assets/uploads/images/shares/Company Pictos/Newable_Pictogram_Blue_COCKTAIL.png'
        ]);

        DB::table('teams')->insert([
            'name' => 'Events and Live Marketing',
            'picto' => '/assets/uploads/images/shares/Company Pictos/Newable_Pictogram_Blue_COCKTAIL.png'
        ]);

        DB::table('teams')->insert([
            'name' => 'Lending',
            'picto' => '/assets/uploads/images/shares/Company Pictos/Newable_Pictogram_Blue_COCKTAIL.png'
        ]);

        DB::table('teams')->insert([
            'name' => 'Central Services',
            'picto' => '/assets/uploads/images/shares/Company Pictos/Newable_Pictogram_Blue_COCKTAIL.png'
        ]);

        DB::table('teams')->insert([
            'name' => 'Business Advisory Services',
            'picto' => '/assets/uploads/images/shares/Company Pictos/Newable_Pictogram_Blue_COCKTAIL.png'
        ]);

        DB::table('teams')->insert([
            'name' => 'Innovation',
            'picto' => '/assets/uploads/images/shares/Company Pictos/Newable_Pictogram_Blue_COCKTAIL.png'
        ]);

        DB::table('teams')->insert([
            'name' => 'Trade South East',
            'picto' => '/assets/uploads/images/shares/Company Pictos/Newable_Pictogram_Blue_COCKTAIL.png'
        ]);

        DB::table('teams')->insert([
            'name' => 'Social Impact',
            'picto' => '/assets/uploads/images/shares/Company Pictos/Newable_Pictogram_Blue_COCKTAIL.png'
        ]);

        DB::table('teams')->insert([
            'name' => 'Private Investing',
            'picto' => '/assets/uploads/images/shares/Company Pictos/Newable_Pictogram_Blue_COCKTAIL.png'
        ]);

        DB::table('teams')->insert([
            'name' => 'Events Team',
            'picto' => '/assets/uploads/images/shares/Company Pictos/Newable_Pictogram_Blue_COCKTAIL.png'
        ]);
    }
}
