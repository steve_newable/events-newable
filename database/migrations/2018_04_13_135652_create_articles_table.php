<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id', 10);
            $table->string('title');
            $table->string('excerpt')->nullable();
            $table->mediumText('content')->nullable();
            $table->string('featuredImage')->nullable();
            $table->mediumText('featuredVideo')->nullable();
            $table->string('author')->nullable();
            $table->string('readingTime')->nullable();
            $table->string('category')->nullable();
            $table->boolean('featuredArticle')->default(0);
            $table->string('published')->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
