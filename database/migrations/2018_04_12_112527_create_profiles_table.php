<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_username');
            $table->mediumText('skills')->nullable();
            $table->mediumText('background')->nullable();
            $table->string('socialProfiles')->nullable();
            $table->string('displayPicture')->default('/assets/img/staff_photos/default.jpg');
            $table->string('mangedByUsername')->nullable();
            $table->boolean('changesPending')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
