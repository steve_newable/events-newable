<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     * Users table contains data pulled in from AD directly.
     * Some fields are used in other tables
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('displayName');
            $table->string('email')->unique();
            $table->string('role')->nullable();
            $table->string('department')->nullable();
            $table->string('location')->nullable();
            $table->string('directDialIn')->nullable();
            $table->string('mobileNumber')->nullable();
            $table->string('managedByUsername')->nullable();
            $table->string('managedByDisplayName')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
