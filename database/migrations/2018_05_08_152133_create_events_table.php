<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('location')->nullable();
            $table->date('startDate')->nullable();
            $table->string('startTime')->nullable();
            $table->date('finishDate')->nullable();
            $table->string('finishTime')->nullable();
            $table->string('image')->nullable();
            $table->mediumText('description')->nullable();
            $table->mediumText('details')->nullable();
            $table->string('externalPageUrl')->nullable();
            $table->string('hostedBy')->nullable();
            $table->string('type')->nullable();
            $table->string('category')->nullable();
            $table->string('sector')->nullable();
            $table->string('published')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
