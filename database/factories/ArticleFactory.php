<?php

use Faker\Generator as Faker;

$factory->define(App\Article::class, function (Faker $faker) {
    return [
        'title' => "Title of article",
        'excerpt' => "Excerpt from article",
        'content' => "A big body of content will go here at some point but for now: Cras ultricies ligula sed magna dictum porta. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Curabitur aliquet quam id dui posuere blandit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula. Donec sollicitudin molestie malesuada.",
        'featuredImage' => 'http://via.placeholder.com/350x150',
        'author' => 'Jesse Orange',
        'readingTime' => '2 minutes',
        'seoUrl' => 'null',
        'published' => '1',
    ];
});
