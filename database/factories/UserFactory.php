<?php

use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'username' => $faker->userName,
        'displayName' => $faker->name,
        'email' => $faker->email,
        'role' => $faker->jobTitle,
        'department' => $faker->name,
        'location' => $faker->address,
        'directDialIn' => $faker->phoneNumber,
        'mobileNumber' => $faker->phoneNumber,
        'managedByUsername' => $faker->userName,
        'managedByDisplayName' => $faker->name,
    ];
});
